# Système automatisé de gestion des visites Sentritech

Dépôt concernant un système automatisé de gestion des visites Sentritech consistant en une API (ApiPlatform), et une PWA basée sur React.

Le projet se déroule sur une période allant du 01/06/2021 au 17/09/2021 et pourra se poursuivre au delà.

### Liens

Lien vers le serveur de pré-production : https://test-api.sos-termites.com
