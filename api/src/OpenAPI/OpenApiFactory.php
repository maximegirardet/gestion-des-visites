<?php


namespace App\OpenAPI;


use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\Model\RequestBody;
use ApiPlatform\Core\OpenApi\OpenApi;
use ArrayObject;

class OpenApiFactory implements OpenApiFactoryInterface
{
	public function __construct(private OpenApiFactoryInterface $decorated)
	{

	}

	public function __invoke(array $context = []): OpenApi
	{
		$openApi = $this->decorated->__invoke($context);
		/**
		 * @var PathItem $path
		 */
		foreach ($openApi->getPaths()->getPaths() as $key => $path) {
			if ($path->getGet() && $path->getGet()->getSummary() === 'hidden') {
				$openApi->getPaths()->addPath($key, $path->withGet(null));
			}
		}

		$schemas = $openApi->getComponents()->getSecuritySchemes();
		$schemas["bearerAuth"] = new ArrayObject([
			'type' => 'http',
			'scheme' => 'bearer',
			'bearerFormat' => 'JWT'
		]);
		$schemas = $openApi->getComponents()->getSchemas();
		$schemas['Credentials'] = new ArrayObject([
			'type' => 'object',
			'properties' => [
				'username' => [
					'type' => 'string',
					'example' => 'john@doe.fr'
				],
				'password' => [
					'type' => 'string',
					'example' => '0000'
				]
			]
		]);
		$schemas['Refresh'] = new ArrayObject([
			'type' => 'object',
			'properties' => [
				'refresh_token' => [
					'type' => 'string',
				]
			]
		]);


		$pathItemLogin = new PathItem(
			post: new Operation(
				operationId: 'postApiLogin',
				tags: ["Auth"],
				responses: [
				'200' => [
					'description' => 'Utilisateur connecté'
				],
				'401' => [
					'description' => 'Erreur lors de la connexion'
				]
			],
				requestBody: new RequestBody(
					content: new ArrayObject([
						'application/json' => [
							'schema' => [
								'$ref' => '#/components/schemas/Credentials'
							]
						]
					])
				)
			)
		);

		$openApi->getPaths()->addPath('/login', $pathItemLogin);

		$pathItemRefresh = new PathItem(
			get: new Operation(
				operationId: 'postApiRefresh',
				tags: ["Auth"],
				responses: [
					'200' => [
						'description' => 'Token'
					],
					'401' => [
						'description' => 'Erreur lors de la connexion'
					]
				]
			)
		);

		$openApi->getPaths()->addPath('/token/refresh', $pathItemRefresh);


		return $openApi;
	}
}