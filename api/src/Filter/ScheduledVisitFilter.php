<?php


namespace App\Filter;


use ApiPlatform\Core\Api\FilterInterface;

class ScheduledVisitFilter implements FilterInterface
{

	public function getDescription(string $resourceClass): array
	{
		$description = [];
		$description['date'] = [
			'property' => 'scheduled_at',
			'type' => 'datetime',
			'required' => true,
			'strategy' => 'exact',
			'is_collection' => true
		];
		return $description;
	}
}
