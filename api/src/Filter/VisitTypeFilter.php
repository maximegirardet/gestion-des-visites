<?php


namespace App\Filter;


use ApiPlatform\Core\Api\FilterInterface;

class VisitTypeFilter implements FilterInterface
{

	public function getDescription(string $resourceClass): array
	{
		$description = [];
		$description['worksite_id'] = [
			'property' => 'worksite_id',
			'type' => 'int',
			'required' => false,
			'strategy' => 'exact',
			'is_collection' => false
		];
		return $description;
	}
}
