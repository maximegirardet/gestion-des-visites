<?php


namespace App\Filter;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Service\TypesenseService;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Http\Client\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Typesense\Exceptions\TypesenseClientError;

class WorksiteFilter extends AbstractContextAwareFilter
{
	public function __construct(ManagerRegistry $managerRegistry, private TypesenseService $typesenseService, ?RequestStack $requestStack = null, LoggerInterface $logger = null, array $properties = null, NameConverterInterface $nameConverter = null)
	{
		parent::__construct($managerRegistry, $requestStack, $logger, $properties, $nameConverter);
	}

	public function getDescription(string $resourceClass): array
	{
		$description = [];
		$description['q'] = [
			'property' => 'q',
			'type' => 'string',
			'required' => false,
			'strategy' => 'exact',
			'is_collection' => true
		];
		return $description;
	}

	/**
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
	{
		if ($property !== "q" || $value === "") {
			return;
		}

		$worksites = $this->typesenseService->searchWorksite($value);

		$alias = $queryBuilder->getRootAliases()[0];
		$parameterName = $queryNameGenerator->generateParameterName($property); // Generate a unique parameter name to avoid collisions with other filters
		$queryBuilder
			->andWhere(sprintf('%s.%s IN (:%s)', $alias, "id", $parameterName))
			->setParameter($parameterName, $worksites);
	}
}

