<?php


namespace App\Service;


use App\Entity\ScheduledVisit;
use App\Entity\Visit;
use App\Entity\VisitType;
use App\Entity\Worksite;
use DateInterval;
use DateTime;
use Exception;
use Google\Service\Calendar\Event;


class ScheduledVisitService
{
	public function __construct(private GoogleCalendarService $calendarService, private StepDurationCalculationService $durationCalculationService)
	{
	}

	/**
	 * Sets calculated step durations to visitType object
	 * @param VisitType $type Type of the visit
	 * @param $worksite Worksite Concerned worksite
	 * @param $duration DateInterval Total duration of the visit
	 */
	public function getTypeWithCalculatedStepDuration(VisitType $type, Worksite $worksite, DateInterval $duration): VisitType
	{
		return $this->durationCalculationService->calculateStepsDuration($type, $worksite, $duration);
	}

	/**
	 * Constructs a ScheduledVisit object from an event, a visit type and a worksite
	 * @param Event $event Event
	 * @param Worksite $worksite Concerned worksite
	 * @param VisitType $type Type of the visit
	 * @param Visit|null $visit Concerned visit (if it exists)
	 * @return ScheduledVisit
	 * @throws Exception
	 */
	public function getScheduledVisitFromEvent(Event $event, Worksite $worksite, VisitType $type, ?Visit $visit): ScheduledVisit
	{
		$start = new DateTime($event->getStart()->getDateTime());
		$end = new DateTime($event->getEnd()->getDateTime());
		$updatedType = $this->getTypeWithCalculatedStepDuration($type, $worksite, $start->diff($end));
		return (new ScheduledVisit())
			->setEventId($event->getId())
			->setScheduledAt($start)
			->setScheduledDuration($start->diff($end))
			->setWorksite($worksite)
			->setType($updatedType)
			->setVisit($visit);
	}

	/**
	 * Gets next scheduled visits of a specific worksite
	 * @param Worksite $worksite Concerned worksite
	 * @return array
	 * @throws Exception
	 */
	public function getNextScheduledVisits(Worksite $worksite): array
	{
		$events = $this->calendarService->getWorksiteEvents($worksite->getCode(), new DateTime(), true);
		return $events->map(fn($e) => $this->getScheduledVisitFromEvent($e, $worksite, $worksite->getNextVisitType(), null))->toArray();
	}
}

