<?php


namespace App\Service;


use App\Entity\Worksite;
use Doctrine\ORM\EntityManagerInterface;
use Http\Client\Exception;
use JsonException;
use Symfony\Component\HttpClient\HttplugClient;
use Typesense\Client;
use Typesense\Exceptions\ConfigError;
use Typesense\Exceptions\TypesenseClientError;

class TypesenseService
{

	private Client $client;

	/**
	 * @throws ConfigError
	 */
	public function __construct(private string $typesenseKey, private EntityManagerInterface $entityManager)
	{
		$this->setClient();
	}

	/**
	 * @throws ConfigError
	 * Create typesense client
	 */
	private function setClient()
	{
		$this->client = new Client([
			'api_key' => $this->typesenseKey,
			'nodes' => [
				[
					'host' => 'typesense',
					'port' => '8108',
					'protocol' => 'http',
				],
			],
			'client' => new HttplugClient(),
		]);
	}

	/**
	 * Creates worksite collection
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function createWorksiteCollection()
	{
		$this->client->collections->create([
			'name' => 'worksites',
			'fields' => [
				[
					'name' => 'code',
					'type' => 'string',
				],
				[
					'name' => 'name',
					'type' => 'string',
				],
				[
					'name' => 'address',
					'type' => 'string',
				],
			],
		]);
	}

	/**
	 * Normalizes a worksite to index it as a typesense document
	 * @param Worksite $worksite The worksite to normalize
	 * @return array Normalized array
	 * @noinspection PhpArrayShapeAttributeCanBeAddedInspection
	 * @noinspection PhpPureAttributeCanBeAddedInspection
	 */
	private function normalizeWorksite(Worksite $worksite): array
	{
		return [
			"id" => strval($worksite->getId()),
			"name" => $worksite->getName(),
			"code" => strval($worksite->getCode()),
			"address" => $worksite->getAddress()->getStreet() . " " . $worksite->getAddress()->getCity()
		];
	}

	/**
	 * Import all worksites that are stored in database
	 * @throws Exception
	 * @throws TypesenseClientError
	 * @throws JsonException
	 */
	public function importAllWorksites()
	{
		$worksites = $this->entityManager->getRepository(Worksite::class)->findAll();
		$worksites = array_map(fn(Worksite $w) => $this->normalizeWorksite($w), $worksites);
		$this->client->collections["worksites"]->documents->import($worksites, ['action' => 'create']);
	}

	/**
	 * Upsert (insert or update) the specified worksite into Typesense database
	 * @param Worksite $worksite The worksite to upsert
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function upsertWorksite(Worksite $worksite)
	{
		$this->client->collections["worksites"]->documents->upsert($this->normalizeWorksite($worksite));
	}

	/**
	 * Deletes the specified worksite from typesense database
	 * @param string $id Worksite id as a string
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function deleteWorksite(string $id)
	{
		$this->client->collections["worksites"]->documents[$id]->delete();
	}

	/**
	 * Searches for worksites corresponding to the query in Typesense database
	 * @param string $query Search query
	 * @return array Array of corresponding worksites
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function searchWorksite(string $query): array
	{
		$worksites = $this->client->collections["worksites"]->documents->search(
			[
				'q' => $query,
				'query_by' => 'name, code, address',
				'query_by_weights' => "4, 4, 1",
				'num_typos' => '1',
				'per_page' => '5'
			]
		);
		return array_map(fn($w) => $w["document"]["id"], $worksites["hits"]);
	}
}

