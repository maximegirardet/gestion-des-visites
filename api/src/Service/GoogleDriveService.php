<?php


namespace App\Service;


use Google_Service_Drive;

class GoogleDriveService extends GoogleAbstractService
{

	public function __construct(string $env, string $clientID, string $clientSecret, string $tokenPath, string $scope)
	{
		parent::__construct($env, $clientID, $clientSecret, $tokenPath, $scope);
	}

	/**
	 * Downloads the file whose id is specified
	 * @param string $fileID
	 * @noinspection PhpMissingReturnTypeInspection
	 * @noinspection PhpDocSignatureIsNotCompleteInspection
	 */
	public function downloadFile(string $fileID)
	{
		$service = new Google_Service_Drive($this->client);
		return $service->files->get($fileID, ["alt" => "media"]);
	}
}

