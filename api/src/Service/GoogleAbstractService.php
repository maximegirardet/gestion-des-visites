<?php


namespace App\Service;


use Exception;
use Google_Client;

abstract class GoogleAbstractService
{
	/**
	 * @var Google_Client Google Client SDK
	 */
	protected Google_Client $client;

	/**
	 * @throws Exception
	 */
	public function __construct(private string $env, private string $clientID, private string $clientSecret, private string $tokenPath, private string $scope)
	{
		$this->client = $this->getClient();
	}

	/**
	 * @throws Exception
	 * Constructs a configured client with an initialized token
	 */
	private function getClient(): Google_Client
	{
		$client = GoogleAbstractService::configureClient(new Google_Client(), $this->clientID, $this->clientSecret, $this->scope);
		return $this->setToken($client);
	}

	/**
	 * @param Google_Client $client Google Client SDK
	 * @param string $clientID Google ClientID
	 * @param string $clientSecret Google ClientSecret
	 * @return Google_Client The Google Client with all credentials and required parameters
	 */
	public static function configureClient(Google_Client $client, string $clientID, string $clientSecret, string $scope): Google_Client
	{
		$client->setClientId($clientID);
		$client->setClientSecret($clientSecret);
		$client->setAccessType('offline');
		$client->setRedirectUri("urn:ietf:wg:oauth:2.0:oob");
		$client->setScopes([$scope]);
		return $client;
	}

	/**
	 * Gets and sets a new token in Google Client.
	 * If the token is expired, tries to get a new one using refresh token and saves the new token.
	 * If any token can be retrieved, throws an Exception.
	 * @param Google_Client $client Google Client SDK
	 * @return Google_Client Google Client SDK with token
	 * @throws Exception
	 */
	private function setToken(Google_Client $client): Google_Client
	{
		if (file_exists($this->tokenPath)) {
			$accessToken = json_decode(file_get_contents($this->tokenPath), true);
			$client->setAccessToken($accessToken);
			return $client;
		}
		if ($client->isAccessTokenExpired()) {
			if ($client->getRefreshToken()) {
				$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			} else {
				if ($this->env === "prod"){
					throw new Exception("Cannot refresh token, please authenticate yourself.");
				}
			}
			GoogleAbstractService::saveToken($this->tokenPath, $client);
		}
		return $client;
	}

	/**
	 * Saves the token in the specified path
	 * @param string $tokenPath The path of the token file
	 * @param Google_Client $client Google Client SDK
	 */
	public static function saveToken(string $tokenPath, Google_Client $client)
	{
		if (!file_exists(dirname($tokenPath))) {
			mkdir(dirname($tokenPath), 0700, true);
		}
		file_put_contents($tokenPath, json_encode($client->getAccessToken()));
	}

}
