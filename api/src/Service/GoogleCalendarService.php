<?php


namespace App\Service;


use App\Entity\Contact;
use App\Entity\User;
use App\Entity\Visit;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Google\Service\Calendar\Event;
use Google\Service\Calendar\Events;
use Google_Service_Calendar;
use Symfony\Component\Security\Core\User\UserInterface;

class GoogleCalendarService extends GoogleAbstractService
{

	public function __construct(string $env, string $clientID, string $clientSecret, string $tokenPath, string $scope, private string $novaCalendarID, private EntityManagerInterface $entityManager)
	{
		parent::__construct($env, $clientID, $clientSecret, $tokenPath, $scope);
	}

	/**
	 * Gets all the scheduled events in a user's calendar from a specific day with options.
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @return Events|null The corresponding events
	 */
	private function listEventsFromSpecificDay(?UserInterface $user, DateTime $date, array $options): ?Events
	{
		$service = new Google_Service_Calendar($this->client);
		if ($user instanceof User) {
			$minTime = $date->setTime(0, 0);
			return $service->events->listEvents($user->getCalendarId(), array_merge([
				"timeMin" => $minTime->format(DateTimeInterface::RFC3339),
			], $options));
		}
		return null;
	}

	/**
	 * Gets all the scheduled events in a user's calendar during a specific day with options.
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @return Events|null The corresponding events
	 */
	private function listEvents(?UserInterface $user, DateTime $date, array $options): ?Events
	{
		if ($user instanceof User) {
			$maxTime = $date->setTime(23, 59);
			return $this->listEventsFromSpecificDay($user, $date, array_merge([
				"timeMax" => $maxTime->format(DateTimeInterface::RFC3339)
			], $options));
		}
		return null;
	}

	/**
	 * Gets all the scheduled events in a user's calendar during a specific day.
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @return Events|null The corresponding events
	 */
	public function getScheduledEvents(?UserInterface $user, DateTime $date): ?Events
	{
		return $this->listEvents($user, $date, []);
	}

	/**
	 * Gets all events of a worksite during a specific day for a specific user.
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param string $code Worksite's code
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @return array Events of a worksite during a specific day for a specific user.
	 */
	public function getWorksiteUserEvents(?UserInterface $user, string $code, DateTime $date): array
	{
		return array_filter($this->listEvents($user, $date, [])["items"], fn($event) => $event instanceof Event && str_contains($event->getDescription(), $code));
	}

	/**
	 * Gets all events of a worksite during a specific date or gets the next visits of the worksite as from the specified day
	 * @param string $code Worksite's code
	 * @param DateTime $date The day during which we want to know the scheduled events
	 * @param bool $nextVisit Boolean which indicates if we look for the next visit after the specified date
	 * @return ArrayCollection
	 */
	public function getWorksiteEvents(string $code, DateTime $date, bool $nextVisit = false): ArrayCollection
	{
		$events = [];
		$users = $this->entityManager->getRepository(User::class)->findAll();
		foreach ($users as $user) {
			if ($user instanceof UserInterface) {
				if ($nextVisit) {
					$items = $this->listEventsFromSpecificDay($user, $date, [
						"q" => $code
					]);
				} else {
					$items = $this->getWorksiteUserEvents($user, $code, $date);
				}
				$events = [...$events, ...$items];
			}
		}
		return new ArrayCollection($events);
	}

	/**
	 * Gets the specified event from user's calendar
	 * @param UserInterface|null $user The authenticated User (or null)
	 * @param string $eventId Google Calendar Event ID
	 * @return Event|null
	 */
	public function getEvent(?UserInterface $user, string $eventId): ?Event
	{
		$service = new Google_Service_Calendar($this->client);
		if ($user instanceof User) {
			return $service->events->get($user->getCalendarId(), $eventId);
		}
		return null;
	}

	/**
	 * Gets phone numbers of the worksite, separated by a comma
	 * @param $visit Visit The visit
	 * @return string
	 */
	private function getContact(Visit $visit): string
	{
		return join(",", $visit->getWorksite()->getContacts()->map(fn(Contact $c) => $c->getPhoneNumber())->toArray());
	}

	/**
	 * Gets the summary of the new Google Calendar Event
	 * @param Visit $visit The visit
	 * @return string
	 */
	private function getEventSummary(Visit $visit): string
	{
		return $visit->getWorksite()->getName() . " " . $visit->getType()->getCode() . " - " . $this->getContact($visit);
	}

	/**
	 * Creates an event on Google Calendar thanks to a Visit
	 * @throws Exception
	 */
	public function createEvent(Visit $visit): Event
	{
		$timeZone = "Europe/Paris";
		$service = new Google_Service_Calendar($this->client);
		$address = $visit->getWorksite()->getAddress();
		$beginDatetime = DateTime::createFromInterface($visit->getScheduledAt())->setTimezone(new DateTimeZone($timeZone))->setTime(18, 0);
		$event = new Event([
			"summary" => $this->getEventSummary($visit),
			'location' => $address->getStreet() . " - " . $address->getPostalCode() . " " . $address->getCity(),
			'description' => $visit->getWorksite()->getCode(),
			'start' => array(
				'dateTime' => $beginDatetime->format(DateTimeInterface::RFC3339),
				'timeZone' => $timeZone,
			),
			'end' => array(
				'dateTime' => $beginDatetime->add($visit->getEstimatedDuration())->format(DateTimeInterface::RFC3339),
				'timeZone' => $timeZone,
			),
		]);
		return $service->events->insert($this->novaCalendarID, $event);
	}
}

