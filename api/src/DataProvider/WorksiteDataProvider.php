<?php


namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\ResourceClassNotSupportedException;
use App\Entity\Worksite;
use App\Service\ScheduledVisitService;
use DateInterval;
use Exception;

class WorksiteDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{

	public function __construct(private ContextAwareCollectionDataProviderInterface $collectionDataProvider, private ScheduledVisitService $scheduledVisitService)
	{
	}

	/**
	 * @throws ResourceClassNotSupportedException
	 * @throws Exception
	 */
	public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
	{
		$collection = $this->collectionDataProvider->getCollection($resourceClass, $operationName, $context);

		if ($resourceClass !== Worksite::class) {
			return $collection;
		}

		foreach ($collection as $worksite) {
			/**
			 * @var $worksite Worksite
			 */
			$visits = $this->scheduledVisitService->getNextScheduledVisits($worksite);
			$worksite->setNextScheduledVisits($visits);
			if (count($visits) === 0) {
				$visitType = $this->scheduledVisitService->getTypeWithCalculatedStepDuration($worksite->getNextVisitType(), $worksite, DateInterval::createFromDateString("0 min"));
				$worksite->setNextVisitType($visitType);
			}
		}
		return $collection;
	}

	public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
	{
		return true;
	}
}

