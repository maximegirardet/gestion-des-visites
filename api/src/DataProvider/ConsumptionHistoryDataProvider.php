<?php


namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\Worksite;
use App\Repository\WorksiteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class ConsumptionHistoryDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{

	public function __construct(private EntityManagerInterface $entityManager)
	{
	}

	/**
	 * @throws NonUniqueResultException
	 */
	public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?Worksite
	{
		$repository = $this->entityManager->getRepository($resourceClass);
		if ($repository instanceof WorksiteRepository) {
			return $repository->findByIdWithOrderedVisits($id);
		}
		return null;
	}

	public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
	{
		return $resourceClass === Worksite::class && ($operationName === 'get_consumption_history' || $operationName === 'get_elimination_end_info');
	}
}
