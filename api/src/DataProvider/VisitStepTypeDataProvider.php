<?php


namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\VisitStepType;
use App\Entity\VisitStepTypeAdjustment;
use App\Entity\Worksite;
use App\Repository\VisitStepTypeRepository;
use App\Service\StepDurationCalculationService;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;

class VisitStepTypeDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
	public function __construct(private StepDurationCalculationService $durationCalculationService, private EntityManagerInterface $entityManager)
	{

	}

	/**
	 * Finds corresponding visit types and hydrates them with visit step durations when worksite_id exists
	 * @param string $resourceClass
	 * @param string|null $operationName
	 * @param array $context
	 * @return array
	 */
	public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
	{
		$repository = $this->entityManager->getRepository(VisitStepType::class);
		if ($repository instanceof VisitStepTypeRepository) {
			$collection = $repository->findAll();

			if (isset($context["filters"]["worksite_id"])) {
				$worksite = $this->entityManager->getRepository(Worksite::class)->find($context["filters"]["worksite_id"]);
				if ($worksite instanceof Worksite) {
					return array_map(function ($v) use ($worksite) {
						if ($v instanceof VisitStepType) {
							[$originalDuration, $adjustmentDuration] = $this->durationCalculationService->getCharacteristicDurations($v, $worksite, DateInterval::createFromDateString("0 min"));
							return $v->removeAllAdjustments()->setDuration($originalDuration ?? DateInterval::createFromDateString("0 min"))
								->addAdjustment((new VisitStepTypeAdjustment())->setDuration($adjustmentDuration ?? DateInterval::createFromDateString("0 min"))->setId(0));
						}
						return null;
					}, $collection);
				}
			}
			return $collection;
		}
		return [];
	}

	public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
	{
		return $resourceClass === VisitStepType::class;
	}
}
