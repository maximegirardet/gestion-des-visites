<?php


namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\VisitType;
use App\Entity\Worksite;
use App\Repository\VisitTypeRepository;
use App\Service\StepDurationCalculationService;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;

class VisitTypeDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
	public function __construct(private StepDurationCalculationService $durationCalculationService, private EntityManagerInterface $entityManager)
	{

	}

	/**
	 * Finds corresponding visit types and hydrates them with visit step durations when worksite_id exists
	 * @param string $resourceClass
	 * @param string|null $operationName
	 * @param array $context
	 * @return array
	 */
	public function getCollection(string $resourceClass, string $operationName = null, array $context = []): array
	{
		$repository = $this->entityManager->getRepository(VisitType::class);
		if ($repository instanceof VisitTypeRepository) {
			$collection = $repository->findByWorksiteStatusAndInstallationOnly($context["filters"]["worksite_status.short_name"], $context["filters"]["is_allowed_from_installation_only"]);

			if (isset($context["filters"]["worksite_id"])) {
				$worksite = $this->entityManager->getRepository(Worksite::class)->find($context["filters"]["worksite_id"]);
				if ($worksite instanceof Worksite) {
					return array_map(function ($v) use ($worksite) {
						if ($v instanceof VisitType) {
							return $this->durationCalculationService->calculateStepsDuration($v, $worksite, DateInterval::createFromDateString("0 min"));
						}
						return null;
					}, $collection);
				}
			}
			return $collection;
		}
		return [];
	}

	public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
	{
		return $resourceClass === VisitType::class;
	}
}
