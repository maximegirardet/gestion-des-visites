<?php


namespace App\Controller;


use App\Entity\EliminationPeriod;
use App\Entity\StationStatus;
use App\Entity\Visit;
use App\Entity\Worksite;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EliminationEndInfoController extends AbstractController
{
	public function __construct()
	{
	}

	/**
	 * Tells if consumption has increased between two visits
	 * @param Visit $v1 Visit 1
	 * @param Visit $v2 Visit 2
	 * @return bool
	 */
	private function hasConsumptionIncreased(Visit $v1, Visit $v2): bool
	{
		return $v2->getStationStatuses()->forAll(function (int $currentStatusIndex) use ($v1, $v2) {
			/**
			 * @var $currentStatus StationStatus
			 */
			$currentStatus = $v2->getStationStatuses()[$currentStatusIndex];
			/**
			 * @var $oldStatus StationStatus
			 */
			$oldStatus = $v1->getStationStatuses()->filter(fn(StationStatus $st) => $st->getStation() === $currentStatus->getStation())->first();
			if ($oldStatus) {
				if ($oldStatus->getIsReplaced() || $oldStatus->getIsRecharged()){
					return $currentStatus->getConsumption() === 0;
				}
				return $currentStatus->getConsumption() <= $oldStatus->getConsumption();
			}
			return $currentStatus->getConsumption() === 0;
		});
	}

	/**
	 * @throws Exception
	 */
	public function __invoke(Worksite $data): array
	{
		$eliminationPeriod = $data->getEliminationPeriods()->filter(fn(EliminationPeriod $p) => $p->getEndVisit() === null)->first();
		if ($eliminationPeriod instanceof EliminationPeriod) {
			$visits = $data->getVisits();
			$visitsSinceEliminationBegin = $visits->slice($visits->indexOf($eliminationPeriod->getStartVisit()));
			$visitsWithoutConsumption = [];
			$lastVisitWithConsumption = $eliminationPeriod->getStartVisit();
			$i = 0;
			while (count($visitsWithoutConsumption) < 2 && $i < count($visitsSinceEliminationBegin) - 1) {
				if ($this->hasConsumptionIncreased($visitsSinceEliminationBegin[$i], $visitsSinceEliminationBegin[$i + 1])) {
					$visitsWithoutConsumption[] = $visitsSinceEliminationBegin[$i + 1];
				} else {
					$visitsWithoutConsumption = [];
					$lastVisitWithConsumption = $visitsSinceEliminationBegin[$i + 1];
				}
				$i++;
			}
			return [
				"consumptionBegin" => $eliminationPeriod->getStartVisit()->getScheduledAt(),
				"consumptionEnd" => $lastVisitWithConsumption->getScheduledAt(),
				"inactivityBegin" => $visitsWithoutConsumption[0] ? $visitsWithoutConsumption[0]->getScheduledAt() : null,
				"inactivityEnd" => $visitsWithoutConsumption[1] ? $visitsWithoutConsumption[1]->getScheduledAt() : null
			];
		}
		return [];
	}
}

