<?php


namespace App\Controller;


use App\Entity\Station;
use App\Entity\StationStatus;
use App\Entity\Visit;
use App\Entity\Worksite;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ConsumptionHistoryController extends AbstractController
{
	public function __construct()
	{
	}

	public function __invoke(Worksite $data): array
	{
		$stations = $data->getStations();
		$visits = $data->getVisits();

		/**
		 * Each element of this array (corresponding to each station) contains the station number and consumption history.
		 * Consumption history is an array whose elements are station statuses for each visit during which the specific station was updated.
		 */
		$consumption = $stations->map(fn(Station $station) => [
			"number" => $station->getNumber(),
			"indexName" => $station->getIndexName(),
			"type" => $station->getType(),
			"installedDuringVisit" => $station->getInstalledDuringVisit(),
			"history" => $visits->map(fn(Visit $visit) => $visit->getStationStatuses()->filter(fn(StationStatus $status) => $status->getStation() === $station)->first()
			)
		]);

		return [
			"visits" => $visits->map(fn(Visit $visit) => [
				"id" => $visit->getId(),
				"scheduledAt" => $visit->getScheduledAt(),
				"startingEliminationPeriod" => $visit->getStartingEliminationPeriod(),
				"endingEliminationPeriod" => $visit->getEndingEliminationPeriod()
			]),
			"stations" => $consumption
		];
	}
}

