<?php


namespace App\Controller;

use App\Entity\Visit;
use App\Service\VisitReportStationService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupInterface;

class VisitReportController extends AbstractController
{
	public function __construct(private string $gotenbergHost, private string $gotenbergPort, private HttpClientInterface $httpClient, private EntrypointLookupInterface $entrypointLookup, private VisitReportStationService $reportStationService)
	{
	}

	/**
	 * @throws TransportExceptionInterface
	 * @throws Exception
	 */
	public function __invoke(Visit $data): array
	{
		/**
		 * Creating HTML from twig template
		 */
		$html = $this->renderView('visit_report.html.twig', [
			'pdf' => true,
			'worksite' => $data->getWorksite(),
			'diverseObservations' => $data->getDiverseObservation(),
			"visitDate" => $data->getScheduledAt()->format('d-m-Y'),
			"stationInfos" => $this->reportStationService->getStationInfos($data)
		]);

		/**
		 * Multipart form data fields for Gotenberg REST API
		 */
		$formFields = [
			'files' => [(new DataPart($html, "index.html", 'text/html'))],
			'printBackground' => 'true',
			'marginTop' => '0',
			'marginBottom' => '0',
			'marginLeft' => '0',
			'marginRight' => '0',
			'paperWidth' => "8.27",
			'paperHeight' => "11.7"
		];

		$this->entrypointLookup->reset();

		/**
		 * Adding built CSS and JS files
		 */
		foreach ([...$this->entrypointLookup->getCssFiles('app'), ...$this->entrypointLookup->getJavaScriptFiles('app')] as $file) {
			$formFields['files'][] = DataPart::fromPath($this->getParameter('kernel.project_dir') . '/public/' . $file, basename($file));
		}

		$this->entrypointLookup->reset();

		/**
		 * Making request to Gotenberg REST API
		 */
		$formData = new FormDataPart($formFields);
		/** @noinspection HttpUrlsUsage */
		$pdf = $this->httpClient->request('POST', "http://$this->gotenbergHost:$this->gotenbergPort/forms/chromium/convert/html", [
			'headers' => $formData->getPreparedHeaders()->toArray(),
			'body' => $formData->bodyToIterable(),
		]);

		try {
			return [
				"pdf" => base64_encode($pdf->getContent())
			];
		} catch (ClientExceptionInterface | RedirectionExceptionInterface | TransportExceptionInterface | ServerExceptionInterface) {
			throw new Exception('PDF conversion not available');
		}
	}
}
