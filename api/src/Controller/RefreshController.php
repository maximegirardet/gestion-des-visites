<?php


namespace App\Controller;


use Gesdinet\JWTRefreshTokenBundle\Service\RefreshToken;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class RefreshController extends AbstractController
{

	private RefreshToken $refreshToken;

	public function __construct(RefreshToken $refreshToken)
	{

		$this->refreshToken = $refreshToken;
	}

	public function refresh(Request $request)
	{
		if ($bearer = $request->cookies->get('REFRESH_TOKEN')) {
			$request->request->set('refresh_token', $bearer);
			$request->headers->set('Content-Type', '');
		}
		return $this->refreshToken->refresh($request);
	}

}