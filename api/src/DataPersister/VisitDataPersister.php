<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Station;
use App\Entity\StationStatus;
use App\Entity\User;
use App\Entity\Visit;
use App\Entity\VisitStepType;
use App\Entity\VisitStepTypeAdjustment;
use App\Entity\Worksite;
use App\Service\GoogleCalendarService;
use DateInterval;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Security;

class VisitDataPersister implements ContextAwareDataPersisterInterface
{

	public function __construct(private ContextAwareDataPersisterInterface $decorated, private Security $security, private EntityManagerInterface $entityManager, private GoogleCalendarService $calendarService)
	{
	}

	public function supports($data, array $context = []): bool
	{
		return $this->decorated->supports($data, $context);
	}

	/**
	 * Creates a station corresponding to a stationStatus add adds it to the stationStatus
	 * @param StationStatus $status Original stationStatus object
	 * @param Worksite $worksite Concerned worksite
	 * @param Visit $visit Concerned visit
	 * @return StationStatus
	 */
	private function createStation(StationStatus $status, Worksite $worksite, Visit $visit): StationStatus
	{
		$station = (new Station())
			->setWorksite($worksite)
			->setInstalledDuringVisit($visit)
			->setNumber($status->getStationNumber())
			->setIsSuspended($status->getIsSuspended())
			->setIsInstalled(true)
			->setMasterStation($status->getMasterStation())
			->setIndexName($status->getIndexName())
			->setType($status->getStationType());
		return $status->setStation($station);
	}

	/**
	 * Creates duration adjustments for a specific step type and worksite
	 * @param string $stepType Step type
	 * @param int $minutesDuration Adjustment duration
	 * @param Worksite $worksite Worksite
	 * @return VisitStepTypeAdjustment|null
	 */
	private function createDurationAdjustments(string $stepType, int $minutesDuration, Worksite $worksite): ?VisitStepTypeAdjustment
	{
		if ($minutesDuration !== 0) {
			$negative = $minutesDuration < 0;
			$type = $this->entityManager->getRepository(VisitStepType::class)->findOneBy(["short_name" => $stepType]);
			$user = $this->security->getUser();
			if ($user instanceof User && $type instanceof VisitStepType) {
				$minutesDuration = abs($minutesDuration);
				$interval = DateInterval::createFromDateString("$minutesDuration min");
				if ($negative) {
					$interval->invert = 1;
				}
				return (new VisitStepTypeAdjustment())
					->setWorksite($worksite)
					->setDuration($interval)
					->setCreatedAt(new DateTimeImmutable())
					->setCreatedBy($user)
					->setVisitStepType($type);
			}
		}
		return null;
	}


	/**
	 * Persists data in database and creates event in Google Calendar
	 * @throws Exception
	 */
	public function persist($data, array $context = [])
	{
		if ($data instanceof Visit) {
			array_map(fn($minutesDuration, $stepType) => $data->addVisitStepTypeAdjustment($this->createDurationAdjustments($stepType, $minutesDuration, $data->getWorksite())), $data->getDurationAdjustments(), array_keys($data->getDurationAdjustments()));
			$data->getStationStatuses()->filter(fn(StationStatus $s) => $s->getStation() === null)->map(fn(StationStatus $s) => $this->createStation($s, $data->getWorksite(), $data));
			$data->getStationStatuses()->map(function (StationStatus $s) {
				if (($station = $s->getStation()) instanceof Station) {
					$station->setIsSuspended($s->getIsSuspended());
					if ($s->getIsRemoved() && !$s->getIsSuspended()) {
						$station->setIsInstalled(false);
					}
				}
			});
			$nextVisit = $data->getNextVisit()->setWorksite($data->getWorksite())->setId(0);
			$data->getWorksite()->setNextVisitType($nextVisit->getType())
				->setNextVisitLimitDate($nextVisit->getScheduledAt());

			$persisted = $this->decorated->persist($data);
			$this->calendarService->createEvent($data->getNextVisit());
			return $persisted;
		}
		return $this->decorated->persist($data);
	}

	public function remove($data, array $context = [])
	{
		$this->decorated->remove($data, $context);
	}
}
