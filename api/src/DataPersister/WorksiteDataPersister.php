<?php


namespace App\DataPersister;


use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Worksite;
use App\Service\TypesenseService;
use Http\Client\Exception;
use Typesense\Exceptions\TypesenseClientError;

class WorksiteDataPersister implements ContextAwareDataPersisterInterface
{

	public function __construct(private ContextAwareDataPersisterInterface $decorated, private TypesenseService $typesenseService)
	{
	}

	public function supports($data, array $context = []): bool
	{
		return $this->decorated->supports($data, $context);
	}

	/**
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function persist($data, array $context = [])
	{
		$result = $this->decorated->persist($data, $context);
		if ($data instanceof Worksite &&
			(($context["collection_operation_name"] ?? null) === 'post' ||
				($context["item_operation_name"] ?? null) === 'put' ||
				($context["item_operation_name"] ?? null) === 'patch')) {
			$this->typesenseService->upsertWorksite($data);
		}
		return $result;
	}

	/**
	 * @throws Exception
	 * @throws TypesenseClientError
	 */
	public function remove($data, array $context = [])
	{
		$id = $data->getId();
		$result = $this->decorated->remove($data, $context);
		if ($data instanceof Worksite &&
			($context["item_operation_name"] ?? null) === 'delete') {
			$this->typesenseService->deleteWorksite($id);
		}
		return $result;
	}
}
