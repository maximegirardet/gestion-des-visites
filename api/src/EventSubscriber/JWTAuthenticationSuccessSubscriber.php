<?php

namespace App\EventSubscriber;

use DateInterval;
use DateTime;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;

class JWTAuthenticationSuccessSubscriber implements EventSubscriberInterface
{
	private int $ttl;
	private string $sameSite;

	public function __construct(string $ttl, string $sameSite)
	{
		$this->ttl = intval($ttl);
		$this->sameSite = $sameSite;
	}

	public static function getSubscribedEvents()
	{
		return [
			'lexik_jwt_authentication.on_authentication_success' => 'onLexikJwtAuthenticationOnAuthenticationSuccess',
		];
	}

	/**
	 * @throws Exception
	 */
	public function onLexikJwtAuthenticationOnAuthenticationSuccess(AuthenticationSuccessEvent $event)
	{
		$refreshToken = $event->getData()['refresh_token'];
		if ($refreshToken) {
			$response = $event->getResponse();
			$response->headers->setCookie(new Cookie('REFRESH_TOKEN', $refreshToken, (new DateTime())->add(new DateInterval('PT' . $this->ttl . 'S')), '/', null, true, true, false, $this->sameSite));
			$data = $event->getData();
			unset($data["refresh_token"]);
			$event->setData($data);
		}
	}
}

