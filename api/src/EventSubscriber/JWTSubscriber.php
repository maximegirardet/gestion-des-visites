<?php

namespace App\EventSubscriber;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class JWTSubscriber implements EventSubscriberInterface
{
	public function __construct(private EntityManagerInterface $entityManager)
	{
	}

	public static function getSubscribedEvents() : array
	{
		return [
			'lexik_authentication.on_jwt_created' => 'onLexikAuthenticationOnJwtCreated',
			'lexik_jwt_authentication.on_jwt_authenticated' => 'onLexikAuthenticationOnJwtAuthenticated',
		];
	}

	public function onLexikAuthenticationOnJwtCreated(JWTCreatedEvent $event)
	{
		$data = $event->getData();
		$user = $event->getUser();
		if ($user instanceof User) {
			$data["email"] = $user->getUserIdentifier();
			$data["calendar_id"] = $user->getCalendarId();
			$data["id"] = $user->getId();
		}
		$event->setData($data);
	}

	public function onLexikAuthenticationOnJwtAuthenticated(JWTAuthenticatedEvent $event)
	{
		$token = $event->getToken();
		$user = $token->getUser();
		if ($user instanceof User) {
			$this->entityManager->getUnitOfWork()->createEntity(User::class, [
				'id' => $user->getId(),
			]);
			$user = $this->entityManager->merge($user); //TODO: Replace
			$token->setUser($user);
		}
	}
}
