<?php


namespace App\Command;


use App\Service\TypesenseService;
use Http\Client\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Typesense\Exceptions\TypesenseClientError;

class TypesenseWorksitesImportCommand extends Command
{
	protected static $defaultName = 'typesense:import';

	public function __construct(private TypesenseService $typesenseService, string $name = null)
	{
		parent::__construct($name);
	}

	protected function configure(): void
	{
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		try {
			$this->typesenseService->createWorksiteCollection();
		} catch (Exception | TypesenseClientError) {
			$output->writeln("Collection worksites already exist");
		} finally {
			$this->typesenseService->importAllWorksites();
			return Command::SUCCESS;
		}
	}

}
