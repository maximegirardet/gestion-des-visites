<?php /** @noinspection PhpUnused */


namespace App\Command;


use App\Service\WorksiteImportService;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class ImportWorksitesCommand extends Command
{
	protected static $defaultName = 'nova:import';

	public function __construct(private WorksiteImportService $importService, string $name = null)
	{
		parent::__construct($name);
	}

	protected function configure(): void
	{
	}

	/**
	 * @throws Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$helper = $this->getHelper('question');
		$question = new Question('Code of the worksite : ');
		$worksite_code = $helper->ask($input, $output, $question);
		$this->importService->importWorksite($worksite_code);
		return Command::SUCCESS;
	}

}
