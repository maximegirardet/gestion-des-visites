<?php


namespace App\Command;


use App\Service\GoogleAbstractService;
use Exception;
use Google_Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class GoogleAPIAuthenticationCommand extends Command
{
	protected static $defaultName = 'googleapi:authenticate';

	public function __construct(
		private string $calendarClientID,
		private string $calendarClientSecret,
		private string $calendarTokenPath,
		private string $calendarScope,
		private string $driveClientID,
		private string $driveClientSecret,
		private string $driveTokenPath,
		private string $driveScope,
		string $name = null)
	{
		parent::__construct($name);
	}

	protected function configure(): void
	{
	}

	/**
	 * @throws Exception
	 */
	private function authenticate($type): int
	{
		if ($type === "calendar") {
			$client = GoogleAbstractService::configureClient(new Google_Client(), $this->calendarClientID, $this->calendarClientSecret, $this->calendarScope);
			$tokenPath = $this->calendarTokenPath;
		} else if ($type === 'drive') {
			$client = GoogleAbstractService::configureClient(new Google_Client(), $this->driveClientID, $this->driveClientSecret, $this->driveScope);
			$tokenPath = $this->driveTokenPath;
		} else {
			return Command::FAILURE;
		}
		$authUrl = $client->createAuthUrl();
		printf("Open the following link in your browser:\n%s\n", $authUrl);
		print 'Enter verification code: ';
		$authCode = trim(fgets(STDIN));

		// Exchange authorization code for an access token.
		$accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
		$client->setAccessToken($accessToken);

		// Check to see if there was an error.
		if (array_key_exists('error', $accessToken)) {
			throw new Exception(join(', ', $accessToken));
		}

		GoogleAbstractService::saveToken($tokenPath, $client);
		return Command::SUCCESS;
	}

	/**
	 * @throws Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$helper = $this->getHelper('question');
		$question = new Question('Name of Google API (calendar or drive) ? ');
		$apiType = $helper->ask($input, $output, $question);

		return $this->authenticate($apiType);
	}
}

