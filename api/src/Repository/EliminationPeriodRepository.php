<?php

namespace App\Repository;

use App\Entity\EliminationPeriod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EliminationPeriod|null find($id, $lockMode = null, $lockVersion = null)
 * @method EliminationPeriod|null findOneBy(array $criteria, array $orderBy = null)
 * @method EliminationPeriod[]    findAll()
 * @method EliminationPeriod[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EliminationPeriodRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EliminationPeriod::class);
    }

    // /**
    //  * @return EliminationPeriod[] Returns an array of EliminationPeriod objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EliminationPeriod
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
