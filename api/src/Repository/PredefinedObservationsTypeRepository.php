<?php

namespace App\Repository;

use App\Entity\PredefinedObservationsType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PredefinedObservationsType|null find($id, $lockMode = null, $lockVersion = null)
 * @method PredefinedObservationsType|null findOneBy(array $criteria, array $orderBy = null)
 * @method PredefinedObservationsType[]    findAll()
 * @method PredefinedObservationsType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PredefinedObservationsTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PredefinedObservationsType::class);
    }

    // /**
    //  * @return PredefinedObservationsType[] Returns an array of PredefinedObservationsType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PredefinedObservationsType
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
