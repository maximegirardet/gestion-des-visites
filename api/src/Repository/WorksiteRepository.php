<?php

namespace App\Repository;

use App\Entity\Worksite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Worksite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Worksite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Worksite[]    findAll()
 * @method Worksite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorksiteRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, Worksite::class);
	}

	/**
	 * @return ?Worksite Returns a Worksite object, hydrated with visits, stations and station statuses
	 * @throws NonUniqueResultException
	 */

	public function findByIdWithOrderedVisits($value): ?Worksite
	{
		return $this->createQueryBuilder('w')
			->addSelect('v')
			->addSelect('st')
			->addSelect("e1")
			->addSelect("e2")
			->innerJoin('w.visits', 'v', Join::WITH, 'v.worksite = w.id')
			->leftJoin('v.stationStatuses', 'st', Join::WITH, 'st.visit = v.id')
			->leftJoin('v.startingEliminationPeriod', 'e1', Join::WITH, 'e1.startVisit = v.id')
			->leftJoin('v.endingEliminationPeriod', 'e2', Join::WITH, 'e2.endVisit = v.id')
			->andWhere('w.id = :val')
			->setParameter('val', $value)
			->addOrderBy("v.scheduledAt", 'asc')
			->addOrderBy("v.id", 'asc')
			->getQuery()
			->getOneOrNullResult()
		;
	}

	/*
	public function findOneBySomeField($value): ?Worksite
	{
		return $this->createQueryBuilder('w')
			->andWhere('w.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
