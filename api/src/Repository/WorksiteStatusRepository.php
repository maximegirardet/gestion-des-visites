<?php

namespace App\Repository;

use App\Entity\WorksiteStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WorksiteStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorksiteStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorksiteStatus[]    findAll()
 * @method WorksiteStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorksiteStatusRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, WorksiteStatus::class);
	}

	// /**
	//  * @return WorksiteStatus[] Returns an array of WorksiteStatus objects
	//  */
	/*
	public function findByExampleField($value)
	{
		return $this->createQueryBuilder('w')
			->andWhere('w.exampleField = :val')
			->setParameter('val', $value)
			->orderBy('w.id', 'ASC')
			->setMaxResults(10)
			->getQuery()
			->getResult()
		;
	}
	*/

	/*
	public function findOneBySomeField($value): ?WorksiteStatus
	{
		return $this->createQueryBuilder('w')
			->andWhere('w.exampleField = :val')
			->setParameter('val', $value)
			->getQuery()
			->getOneOrNullResult()
		;
	}
	*/
}
