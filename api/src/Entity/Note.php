<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\NoteRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=NoteRepository::class)
 */
#[ApiResource(normalizationContext: ['groups' => ['worksite']])]
class Note
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="text")
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $content;

	/**
	 * @ORM\Column(type="datetime", nullable="true")
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $created_at;

	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $is_visible;

	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $is_punctual;

	/**
	 * @ORM\ManyToOne(targetEntity=Worksite::class, inversedBy="notes")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $worksite;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getContent(): ?string
	{
		return $this->content;
	}

	public function setContent(string $content): self
	{
		$this->content = $content;

		return $this;
	}

	public function getCreatedAt(): ?DateTimeInterface
	{
		return $this->created_at;
	}

	public function setCreatedAt(DateTimeInterface $created_at): self
	{
		$this->created_at = $created_at;

		return $this;
	}

	public function getIsVisible(): ?bool
	{
		return $this->is_visible;
	}

	public function setIsVisible(bool $is_visible): self
	{
		$this->is_visible = $is_visible;

		return $this;
	}

	public function getIsPunctual(): ?bool
	{
		return $this->is_punctual;
	}

	public function setIsPunctual(bool $is_punctual): self
	{
		$this->is_punctual = $is_punctual;

		return $this;
	}

	public function getWorksite(): ?Worksite
	{
		return $this->worksite;
	}

	public function setWorksite(?Worksite $worksite): self
	{
		$this->worksite = $worksite;

		return $this;
	}
}

