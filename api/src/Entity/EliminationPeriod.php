<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EliminationPeriodRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EliminationPeriodRepository::class)
 */
#[ApiResource]
class EliminationPeriod
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
	#[Groups('worksite_summary')]
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Visit::class, inversedBy="startingEliminationPeriod", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $startVisit;

    /**
     * @ORM\OneToOne(targetEntity=Visit::class, inversedBy="endingEliminationPeriod", cascade={"persist", "remove"})
     */
    private $endVisit;

    /**
     * @ORM\ManyToOne(targetEntity=Worksite::class, inversedBy="eliminationPeriods")
     * @ORM\JoinColumn(nullable=false)
     */
    private $worksite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartVisit(): ?Visit
    {
        return $this->startVisit;
    }

    public function setStartVisit(Visit $startVisit): self
    {
        $this->startVisit = $startVisit;

        return $this;
    }

    public function getEndVisit(): ?Visit
    {
        return $this->endVisit;
    }

    public function setEndVisit(?Visit $endVisit): self
    {
        $this->endVisit = $endVisit;

        return $this;
    }

    public function getWorksite(): ?Worksite
    {
        return $this->worksite;
    }

    public function setWorksite(?Worksite $worksite): self
    {
        $this->worksite = $worksite;

        return $this;
    }
}
