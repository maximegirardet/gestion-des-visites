<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\OpenAPI\SecuredAPIRessourceAttribute;
use App\Repository\PredefinedObservationsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PredefinedObservationsRepository::class)
 */
#[
ApiResource(
	collectionOperations: [
	'get' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]],
	'post' => [
	'openapi_context' => [
		'security' => [['bearerAuth' => []]]
	]]],
	itemOperations: [
	'get' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]],
	'put' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]],
	'delete' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]],
	'patch' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]]],
	normalizationContext: ['groups' => ['predefined_observations']],
	security: 'is_granted("ROLE_USER")'
)]
class PredefinedObservations
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	#[Groups('predefined_observations')]
	private $number;

	/**
	 * @ORM\Column(type="text")
	 */
	#[Groups('predefined_observations')]
	private $text;

	/**
	 * @ORM\ManyToOne(targetEntity=PredefinedObservationsType::class, inversedBy="predefinedObservations")
	 * @ORM\JoinColumn(nullable=false)
	 */
	#[Groups('predefined_observations')]
	private $type;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getNumber(): ?int
	{
		return $this->number;
	}

	public function setNumber(int $number): self
	{
		$this->number = $number;

		return $this;
	}

	public function getText(): ?string
	{
		return $this->text;
	}

	public function setText(string $text): self
	{
		$this->text = $text;

		return $this;
	}

	public function getType(): ?PredefinedObservationsType
	{
		return $this->type;
	}

	public function setType(?PredefinedObservationsType $type): self
	{
		$this->type = $type;

		return $this;
	}
}
