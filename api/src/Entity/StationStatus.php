<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StationStatusRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=StationStatusRepository::class)
 */
#[ApiResource(normalizationContext: ['groups' => ['worksite', 'worksite_summary', 'scheduled_visit']])]
class StationStatus
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isConnected;

	/**
	 * @ORM\Column(type="integer")
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $consumption;

	/**
	 * @ORM\ManyToOne(targetEntity=Visit::class, inversedBy="stationStatuses")
	 * @ORM\JoinColumn(nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $visit;

	/**
	 * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="stationStatuses", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=false)
	 */
	#[Groups(['write:visit'])]
	private $station;

	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isInActivity;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isRemoved;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isRecharged;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isReplaced;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isNew;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isNotAccessible;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isLidChanged;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isDuplicated;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isSuspended;

	/**
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	#[Groups(['worksite_summary', 'write:visit'])]
	private $isReinstalled;

	#[Groups(['write:visit'])]
	private ?int $stationNumber = null;

	#[Groups(['write:visit'])]
	private ?Station $masterStation = null;

	#[Groups(['write:visit'])]
	private ?string $indexName = null;

	#[Groups(['write:visit'])]
	private ?StationType $stationType = null;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getIsConnected(): ?bool
	{
		return $this->isConnected;
	}

	public function setIsConnected(bool $isConnected): self
	{
		$this->isConnected = $isConnected;

		return $this;
	}

	public function getConsumption(): ?int
	{
		return $this->consumption;
	}

	public function setConsumption(int $consumption): self
	{
		$this->consumption = $consumption;

		return $this;
	}

	public function getVisit(): ?Visit
	{
		return $this->visit;
	}

	public function setVisit(?Visit $visit): self
	{
		$this->visit = $visit;

		return $this;
	}

	public function getStation(): ?Station
	{
		return $this->station;
	}

	public function setStation(?Station $station): self
	{
		$this->station = $station;

		return $this;
	}

	public function getIsInActivity(): ?bool
	{
		return $this->isInActivity;
	}

	public function setIsInActivity(bool $isInActivity): self
	{
		$this->isInActivity = $isInActivity;

		return $this;
	}

	public function getIsRemoved(): ?bool
	{
		return $this->isRemoved;
	}

	public function setIsRemoved(?bool $isRemoved): self
	{
		$this->isRemoved = $isRemoved;

		return $this;
	}

	public function getIsRecharged(): ?bool
	{
		return $this->isRecharged;
	}

	public function setIsRecharged(?bool $isRecharged): self
	{
		$this->isRecharged = $isRecharged;

		return $this;
	}

	public function getIsReplaced(): ?bool
	{
		return $this->isReplaced;
	}

	public function setIsReplaced(?bool $isReplaced): self
	{
		$this->isReplaced = $isReplaced;

		return $this;
	}

	public function getIsNew(): ?bool
	{
		return $this->isNew;
	}

	public function setIsNew(?bool $isNew): self
	{
		$this->isNew = $isNew;

		return $this;
	}

	public function getIsNotAccessible(): ?bool
	{
		return $this->isNotAccessible;
	}

	public function setIsNotAccessible(?bool $isNotAccessible): self
	{
		$this->isNotAccessible = $isNotAccessible;

		return $this;
	}

	public function getIsLidChanged(): ?bool
	{
		return $this->isLidChanged;
	}

	public function setIsLidChanged(?bool $isLidChanged): self
	{
		$this->isLidChanged = $isLidChanged;

		return $this;
	}

	public function getIsDuplicated(): ?bool
	{
		return $this->isDuplicated;
	}

	public function setIsDuplicated(?bool $isDuplicated): self
	{
		$this->isDuplicated = $isDuplicated;

		return $this;
	}

	public function getIsSuspended(): ?bool
	{
		return $this->isSuspended;
	}

	public function setIsSuspended(?bool $isSuspended): self
	{
		$this->isSuspended = $isSuspended;

		return $this;
	}

	public function getStationNumber(): ?int
	{
		return $this->stationNumber;
	}

	public function setStationNumber(?int $stationNumber): self
	{
		$this->stationNumber = $stationNumber;

		return $this;
	}

	public function getMasterStation(): ?Station
	{
		return $this->masterStation;
	}

	public function setMasterStation(?Station $masterStation): self
	{
		$this->masterStation = $masterStation;

		return $this;
	}

	public function getIndexName(): ?string
	{
		return $this->indexName;
	}

	public function setIndexName(?string $indexName): self
	{
		$this->indexName = $indexName;

		return $this;
	}

	public function getStationType(): ?StationType
	{
		return $this->stationType;
	}

	public function setStationType(?StationType $stationType): self
	{
		$this->stationType = $stationType;

		return $this;
	}

	public function getIsReinstalled(): ?bool
	{
		return $this->isReinstalled;
	}

	public function setIsReinstalled(?bool $isReinstalled): self
	{
		$this->isReinstalled = $isReinstalled;

		return $this;
	}
}

