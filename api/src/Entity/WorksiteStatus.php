<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\WorksiteStatusRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=WorksiteStatusRepository::class)
 */
#[ApiResource(normalizationContext: ['groups' => ['worksite']])]
class WorksiteStatus
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $short_name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $long_name;

	/**
	 * @ORM\OneToMany(targetEntity=Worksite::class, mappedBy="status")
	 */
	private $worksites;

	/**
	 * @ORM\OneToMany(targetEntity=VisitType::class, mappedBy="worksite_status")
	 */
	private $visitTypes;

	public function __construct()
	{
		$this->worksites = new ArrayCollection();
		$this->visitTypes = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getShortName(): ?string
	{
		return $this->short_name;
	}

	public function setShortName(string $short_name): self
	{
		$this->short_name = $short_name;

		return $this;
	}

	public function getLongName(): ?string
	{
		return $this->long_name;
	}

	public function setLongName(string $long_name): self
	{
		$this->long_name = $long_name;

		return $this;
	}

	/**
	 * @return Collection|Worksite[]
	 */
	public function getWorksites(): Collection
	{
		return $this->worksites;
	}

	public function addWorksite(Worksite $worksite): self
	{
		if (!$this->worksites->contains($worksite)) {
			$this->worksites[] = $worksite;
			$worksite->setStatus($this);
		}

		return $this;
	}

	public function removeWorksite(Worksite $worksite): self
	{
		if ($this->worksites->removeElement($worksite)) {
			// set the owning side to null (unless already changed)
			if ($worksite->getStatus() === $this) {
				$worksite->setStatus(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|VisitType[]
	 */
	public function getVisitTypes(): Collection
	{
		return $this->visitTypes;
	}

	public function addVisitType(VisitType $visitType): self
	{
		if (!$this->visitTypes->contains($visitType)) {
			$this->visitTypes[] = $visitType;
			$visitType->setWorksiteStatus($this);
		}

		return $this;
	}

	public function removeVisitType(VisitType $visitType): self
	{
		if ($this->visitTypes->removeElement($visitType)) {
			// set the owning side to null (unless already changed)
			if ($visitType->getWorksiteStatus() === $this) {
				$visitType->setWorksiteStatus(null);
			}
		}

		return $this;
	}
}

