<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=StationRepository::class)
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"worksite_id", "number", "type_id", "index_name"})})
 */
#[ApiResource]
class Station
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	#[Groups('worksite_summary')]
	private $id;

	/**
	 * @ORM\Column(type="integer")
	 */
	#[Groups('worksite_summary')]
	private $number;

	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups('worksite_summary')]
	private $is_suspended;

	/**
	 * @ORM\ManyToOne(targetEntity=StationType::class)
	 * @ORM\JoinColumn(nullable=false)
	 */
	#[Groups('worksite_summary')]
	private $type;


	/**
	 * @ORM\OneToMany(targetEntity=StationStatus::class, mappedBy="station", orphanRemoval=true)
	 */
	#[Groups('worksite_summary')]
	private $stationStatuses;

	/**
	 * @ORM\ManyToOne(targetEntity=Worksite::class, inversedBy="stations")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $worksite;

	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups('worksite_summary')]
	private $is_installed;

	/**
	 * @ORM\ManyToOne(targetEntity=Visit::class, inversedBy="installedStations")
	 */
	private $installedDuringVisit;

	/**
	 * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="replicaStations")
	 */
	#[Groups('worksite_summary')]
	private $masterStation;

	/**
	 * @ORM\OneToMany(targetEntity=Station::class, mappedBy="masterStation")
	 */
	#[Groups('worksite_summary')]
	private $replicaStations;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	#[Groups('worksite_summary')]
	private $indexName;

	public function __construct()
	{
		$this->stationStatuses = new ArrayCollection();
		$this->replicaStations = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getNumber(): ?int
	{
		return $this->number;
	}

	public function setNumber(int $number): self
	{
		$this->number = $number;

		return $this;
	}

	public function getIsSuspended(): ?bool
	{
		return $this->is_suspended;
	}

	public function setIsSuspended(bool $is_suspended): self
	{
		$this->is_suspended = $is_suspended;

		return $this;
	}

	public function getType(): ?StationType
	{
		return $this->type;
	}

	public function setType(?StationType $type): self
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * @return Collection
	 */
	public function getStationStatuses(): Collection
	{
		return $this->stationStatuses;
	}

	public function addStationStatus(StationStatus $stationStatus): self
	{
		if (!$this->stationStatuses->contains($stationStatus)) {
			$this->stationStatuses[] = $stationStatus;
			$stationStatus->setStation($this);
		}

		return $this;
	}

	public function removeStationStatus(StationStatus $stationStatus): self
	{
		if ($this->stationStatuses->removeElement($stationStatus)) {
			// set the owning side to null (unless already changed)
			if ($stationStatus->getStation() === $this) {
				$stationStatus->setStation(null);
			}
		}

		return $this;
	}

	public function getWorksite(): ?Worksite
	{
		return $this->worksite;
	}

	public function setWorksite(?Worksite $worksite): self
	{
		$this->worksite = $worksite;

		return $this;
	}

	public function getIsInstalled(): ?bool
	{
		return $this->is_installed;
	}

	public function setIsInstalled(bool $is_installed): self
	{
		$this->is_installed = $is_installed;

		return $this;
	}

	public function getInstalledDuringVisit(): ?Visit
	{
		return $this->installedDuringVisit;
	}

	public function setInstalledDuringVisit(?Visit $installedDuringVisit): self
	{
		$this->installedDuringVisit = $installedDuringVisit;

		return $this;
	}

	public function getMasterStation(): ?self
	{
		return $this->masterStation;
	}

	public function setMasterStation(?self $masterStation): self
	{
		$this->masterStation = $masterStation;

		return $this;
	}

	/**
	 * @return Collection|self[]
	 */
	public function getReplicaStations(): Collection
	{
		return $this->replicaStations;
	}

	public function addReplicaStation(self $replicaStation): self
	{
		if (!$this->replicaStations->contains($replicaStation)) {
			$this->replicaStations[] = $replicaStation;
			$replicaStation->setMasterStation($this);
		}

		return $this;
	}

	public function removeReplicaStation(self $replicaStation): self
	{
		if ($this->replicaStations->removeElement($replicaStation)) {
			// set the owning side to null (unless already changed)
			if ($replicaStation->getMasterStation() === $this) {
				$replicaStation->setMasterStation(null);
			}
		}

		return $this;
	}

	public function getIndexName(): ?string
	{
		return $this->indexName;
	}

	public function setIndexName(?string $indexName): self
	{
		$this->indexName = $indexName;

		return $this;
	}
}

