<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 */
#[ApiResource]
class Contact
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
	#[Groups(['worksite', 'worksite_summary'])]
    private $name;

    /**
     * @ORM\Column(type="string", length=13, unique=true)
     */
	#[Groups(['worksite', 'worksite_summary'])]
    private $phone_number;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
	#[Groups(['worksite', 'worksite_summary'])]
    private $email_address;

    /**
     * @ORM\ManyToMany(targetEntity=Worksite::class, mappedBy="contacts")
     */
    private $worksites;

    public function __construct()
    {
        $this->worksites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getEmailAddress(): ?string
    {
        return $this->email_address;
    }

    public function setEmailAddress(string $email_address): self
    {
        $this->email_address = $email_address;

        return $this;
    }

    /**
     * @return Collection|Worksite[]
     */
    public function getWorksites(): Collection
    {
        return $this->worksites;
    }

    public function addWorksite(Worksite $worksite): self
    {
        if (!$this->worksites->contains($worksite)) {
            $this->worksites[] = $worksite;
            $worksite->addContact($this);
        }

        return $this;
    }

    public function removeWorksite(Worksite $worksite): self
    {
        if ($this->worksites->removeElement($worksite)) {
            $worksite->removeContact($this);
        }

        return $this;
    }
}
