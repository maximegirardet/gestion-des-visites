<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Filter\ScheduledVisitFilter;
use DateInterval;
use DateTimeInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
	collectionOperations: [
	'get' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		],
		'security' => 'is_granted("ROLE_USER")'
	]
],
	itemOperations: [
	'get' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		],
		'security' => 'is_granted("ROLE_USER")'
	]
],
	normalizationContext: ['groups' => ['scheduled_visit', 'worksite_summary']],
	security: 'is_granted("ROLE_USER")'
)]
#[ApiFilter(ScheduledVisitFilter::class)]
class ScheduledVisit
{

	#[Groups('scheduled_visit')]
	private DateTimeInterface $scheduled_at;

	#[Groups('scheduled_visit')]
	private DateInterval $scheduled_duration;

	#[Groups('scheduled_visit')]
	private ?Worksite $worksite;

	#[Groups('scheduled_visit')]
	private ?VisitType $type;

	#[ApiProperty(
		identifier: true
	)]
	#[Groups('scheduled_visit')]
	private string $event_id;

	#[Groups('scheduled_visit')]
	private ?Visit $visit;


	public function getScheduledAt(): ?DateTimeInterface
	{
		return $this->scheduled_at;
	}

	public function setScheduledAt(DateTimeInterface $scheduled_at): self
	{
		$this->scheduled_at = $scheduled_at;

		return $this;
	}


	public function getWorksite(): ?Worksite
	{
		return $this->worksite;
	}

	public function setWorksite(?Worksite $worksite): self
	{
		$this->worksite = $worksite;

		return $this;
	}

	public function getType(): ?VisitType
	{
		return $this->type;
	}

	public function setType(?VisitType $type): self
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * @return DateInterval
	 */
	public function getScheduledDuration(): DateInterval
	{
		return $this->scheduled_duration;
	}

	/**
	 * @param DateInterval $scheduled_duration
	 * @return ScheduledVisit
	 */
	public function setScheduledDuration(DateInterval $scheduled_duration): self
	{
		$this->scheduled_duration = $scheduled_duration;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getEventId(): string
	{
		return $this->event_id;
	}

	/**
	 * @param string $event_id
	 * @return ScheduledVisit
	 */
	public function setEventId(string $event_id): ScheduledVisit
	{
		$this->event_id = $event_id;
		return $this;
	}

	/**
	 * @return Visit|null
	 */
	public function getVisit(): ?Visit
	{
		return $this->visit;
	}

	/**
	 * @param Visit|null $visit
	 * @return ScheduledVisit
	 */
	public function setVisit(?Visit $visit): ScheduledVisit
	{
		$this->visit = $visit;
		return $this;
	}
}
