<?php


namespace App\Entity\Import;


use DateInterval;

class PDFVisitInfo
{

	private bool $isEliminationBegin = false;
	private bool $isEliminationEnd = false;
	private string $diverseObservations = "";
	private string $internObservations = "";
	private ?DateInterval $effectiveDuration;

	public function __construct()
	{
		$this->effectiveDuration = DateInterval::createFromDateString('0 min');
	}

	/**
	 * @return DateInterval|null
	 */
	public function getEffectiveDuration(): ?DateInterval
	{
		return $this->effectiveDuration;
	}

	/**
	 * @param DateInterval|null $effectiveDuration
	 * @return PDFVisitInfo
	 */
	public function setEffectiveDuration(?DateInterval $effectiveDuration): PDFVisitInfo
	{
		$this->effectiveDuration = $effectiveDuration;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isEliminationBegin(): bool
	{
		return $this->isEliminationBegin;
	}

	/**
	 * @param bool $isEliminationBegin
	 * @return PDFVisitInfo
	 */
	public function setIsEliminationBegin(bool $isEliminationBegin): PDFVisitInfo
	{
		$this->isEliminationBegin = $isEliminationBegin;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isEliminationEnd(): bool
	{
		return $this->isEliminationEnd;
	}

	/**
	 * @param bool $isEliminationEnd
	 * @return PDFVisitInfo
	 */
	public function setIsEliminationEnd(bool $isEliminationEnd): PDFVisitInfo
	{
		$this->isEliminationEnd = $isEliminationEnd;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDiverseObservations(): string
	{
		return $this->diverseObservations;
	}

	/**
	 * @param string $diverseObservations
	 * @return PDFVisitInfo
	 */
	public function setDiverseObservations(string $diverseObservations): PDFVisitInfo
	{
		$this->diverseObservations = $diverseObservations;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getInternObservations(): string
	{
		return $this->internObservations;
	}

	/**
	 * @param string $internObservations
	 * @return PDFVisitInfo
	 */
	public function setInternObservations(string $internObservations): PDFVisitInfo
	{
		$this->internObservations = $internObservations;
		return $this;
	}

}
