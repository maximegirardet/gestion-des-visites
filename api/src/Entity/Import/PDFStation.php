<?php


namespace App\Entity\Import;


use App\Entity\StationStatus;
use App\Entity\StationType;
use App\Entity\Visit;

class PDFStation
{

	private int $id;

	private int $number;

	private StationType $type;

	private StationStatus $stationStatus;

	private ?Visit $installedDuringVisit = null;

	/**
	 * @return Visit|null
	 */
	public function getInstalledDuringVisit(): ?Visit
	{
		return $this->installedDuringVisit;
	}

	/**
	 * @param Visit|null $installedDuringVisit
	 * @return PDFStation
	 */
	public function setInstalledDuringVisit(?Visit $installedDuringVisit): PDFStation
	{
		$this->installedDuringVisit = $installedDuringVisit;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return PDFStation
	 */
	public function setId(int $id): PDFStation
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getNumber(): int
	{
		return $this->number;
	}

	/**
	 * @param int $number
	 * @return PDFStation
	 */
	public function setNumber(int $number): PDFStation
	{
		$this->number = $number;
		return $this;
	}

	/**
	 * @return StationStatus
	 */
	public function getStationStatus(): StationStatus
	{
		return $this->stationStatus;
	}

	/**
	 * @param StationStatus $stationStatus
	 * @return PDFStation
	 */
	public function setStationStatus(StationStatus $stationStatus): PDFStation
	{
		$this->stationStatus = $stationStatus;
		return $this;
	}

	/**
	 * @return StationType
	 */
	public function getType(): StationType
	{
		return $this->type;
	}

	/**
	 * @param StationType $type
	 * @return PDFStation
	 */
	public function setType(StationType $type): self
	{
		$this->type = $type;
		return $this;
	}

}
