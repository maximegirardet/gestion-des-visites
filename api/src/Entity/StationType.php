<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StationTypeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=StationTypeRepository::class)
 */
#[ApiResource]
class StationType
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups('worksite_summary')]
	private $short_name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $long_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $control_duration;

	public function getId(): ?int
         	{
         		return $this->id;
         	}

	public function getShortName(): ?string
         	{
         		return $this->short_name;
         	}

	public function setShortName(string $short_name): self
         	{
         		$this->short_name = $short_name;
         
         		return $this;
         	}

	public function getLongName(): ?string
         	{
         		return $this->long_name;
         	}

	public function setLongName(string $long_name): self
         	{
         		$this->long_name = $long_name;
         
         		return $this;
         	}

    public function getControlDuration(): ?int
    {
        return $this->control_duration;
    }

    public function setControlDuration(int $control_duration): self
    {
        $this->control_duration = $control_duration;

        return $this;
    }
}
