<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PredefinedObservationsTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PredefinedObservationsTypeRepository::class)
 */
#[ApiResource(normalizationContext: ['groups' => ['predefined_observations']])]
class PredefinedObservationsType
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups('predefined_observations')]
	private $type;

	/**
	 * @ORM\OneToMany(targetEntity=PredefinedObservations::class, mappedBy="type")
	 */
	private $predefinedObservations;

	public function __construct()
	{
		$this->predefinedObservations = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getType(): ?string
	{
		return $this->type;
	}

	public function setType(string $type): self
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * @return Collection|PredefinedObservations[]
	 */
	public function getPredefinedObservations(): Collection
	{
		return $this->predefinedObservations;
	}

	public function addPredefinedObservation(PredefinedObservations $predefinedObservation): self
	{
		if (!$this->predefinedObservations->contains($predefinedObservation)) {
			$this->predefinedObservations[] = $predefinedObservation;
			$predefinedObservation->setType($this);
		}

		return $this;
	}

	public function removePredefinedObservation(PredefinedObservations $predefinedObservation): self
	{
		if ($this->predefinedObservations->removeElement($predefinedObservation)) {
			// set the owning side to null (unless already changed)
			if ($predefinedObservation->getType() === $this) {
				$predefinedObservation->setType(null);
			}
		}

		return $this;
	}
}
