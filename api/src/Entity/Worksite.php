<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use App\Controller\ConsumptionHistoryController;
use App\Controller\EliminationEndInfoController;
use App\Filter\WorksiteFilter;
use App\Repository\WorksiteRepository;
use DateInterval;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=WorksiteRepository::class)
 */
#[ApiResource(
	collectionOperations: [
	'get' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]],
	'post' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]]],
	itemOperations: [
	'get' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]],
	'put' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]],
	'delete' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]],
	'patch' => [
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]
		]],
	'get_consumption_history' => [
		'method' => 'GET',
		'path' => '/worksites/{id}/consumption_history',
		'controller' => ConsumptionHistoryController::class,
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]]
	],
	'get_elimination_end_info' => [
		'method' => 'GET',
		'path' => '/worksites/{id}/elimination_end_info',
		'controller' => EliminationEndInfoController::class,
		'openapi_context' => [
			'security' => [['bearerAuth' => []]]]
	]
],
	normalizationContext: ['groups' => ['worksite', 'worksite_summary', 'scheduled_visit']],
	security: 'is_granted("ROLE_USER")'
)]
#[ApiFilter(WorksiteFilter::class)]
#[ApiFilter(OrderFilter::class, properties: ['visits.scheduled_at'], arguments: ['orderParameterName' => 'order'])]
class Worksite
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $name;

	/**
	 * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="worksites", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=false)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $address;

	/**
	 * @ORM\OneToMany(targetEntity=Note::class, mappedBy="worksite", orphanRemoval=true, cascade={"persist"})
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $notes;

	/**
	 * @ORM\OneToMany(targetEntity=Visit::class, mappedBy="worksite", orphanRemoval=true, cascade={"persist"})
	 */
	#[Groups('worksite')]
	private $visits;

	/**
	 * @ORM\OneToMany(targetEntity=Station::class, mappedBy="worksite", orphanRemoval=true, cascade={"persist"})
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $stations;

	/**
	 * @ORM\Column(type="integer", unique=true)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $code;

	/**
	 * @ORM\ManyToOne(targetEntity=WorksiteStatus::class, inversedBy="worksites")
	 * @ORM\JoinColumn(nullable=false)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $status;

	/**
	 * @ORM\ManyToOne(targetEntity=VisitType::class, inversedBy="worksites")
	 */
	#[Groups('worksite')]
	private $next_visit_type;

	/**
	 * @ORM\Column(type="date")
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $installation_date;

	/**
	 * @ORM\Column(type="date")
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $next_visit_limit_date;

	/**
	 * @ORM\Column(type="integer")
	 */
	#[Groups('worksite')]
	private $protected_built_surface;

	/**
	 * @ORM\Column(type="integer")
	 */
	#[Groups('worksite')]
	private $protected_built_perimeter;

	/**
	 * @ORM\Column(type="integer")
	 */
	#[Groups('worksite')]
	private $protected_undeveloped_surface;

	/**
	 * @ORM\Column(type="integer")
	 */
	#[Groups('worksite')]
	private $protected_indeveloped_perimeter;

	/**
	 * @ORM\ManyToMany(targetEntity=Contact::class, inversedBy="worksites", cascade={"persist"})
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $contacts;

	/**
	 * @ORM\Column(type="dateinterval", nullable=true)
	 */
	private ?DateInterval $builtControlDuration;

	#[Groups(['worksite', 'worksite_summary'])]
	private array $nextScheduledVisits = [];

	/**
	 * @ORM\Column(type="integer", nullable=false)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $visit_per_year;

	/**
	 * @ORM\Column(type="date", nullable=false)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $annual_visit_date;

	/**
	 * @ORM\OneToMany(targetEntity=EliminationPeriod::class, mappedBy="worksite", orphanRemoval=true, cascade={"persist"})
	 */
	private $eliminationPeriods;

	/**
	 * @ORM\OneToMany(targetEntity=VisitStepTypeAdjustment::class, mappedBy="worksite", orphanRemoval=true)
	 */
	private $visitStepTypeAdjustments;

	#[Pure] public function __construct()
	{
		$this->stations = new ArrayCollection();
		$this->notes = new ArrayCollection();
		$this->visits = new ArrayCollection();
		$this->contacts = new ArrayCollection();
		$this->eliminationPeriods = new ArrayCollection();
		$this->visitStepTypeAdjustments = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 * @return Worksite
	 */
	public function setId(int $id): self
	{
		$this->id = $id;
		return $this;
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	public function getAddress(): ?Address
	{
		return $this->address;
	}

	public function setAddress(?Address $address): self
	{
		$this->address = $address;

		return $this;
	}

	/**
	 * @return Collection
	 */
	public function getNotes(): Collection
	{
		return $this->notes;
	}

	public function addNote(Note $note): self
	{
		if (!$this->notes->contains($note)) {
			$this->notes[] = $note;
			$note->setWorksite($this);
		}

		return $this;
	}

	public function removeNote(Note $note): self
	{
		if ($this->notes->removeElement($note)) {
			// set the owning side to null (unless already changed)
			if ($note->getWorksite() === $this) {
				$note->setWorksite(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection
	 */
	public function getVisits(): Collection
	{
		return $this->visits;
	}

	public function addVisit(Visit $visit): self
	{
		if (!$this->visits->contains($visit)) {
			$this->visits[] = $visit;
			$visit->setWorksite($this);
		}

		return $this;
	}

	public function removeVisit(Visit $visit): self
	{
		if ($this->visits->removeElement($visit)) {
			// set the owning side to null (unless already changed)
			if ($visit->getWorksite() === $this) {
				$visit->setWorksite(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection
	 */
	public function getStations(): Collection
	{
		return $this->stations;
	}

	public function addStation(Station $station): self
	{
		if (!$this->stations->contains($station)) {
			$this->stations[] = $station;
			$station->setWorksite($this);
		}

		return $this;
	}

	public function removeStation(Station $station): self
	{
		if ($this->stations->removeElement($station)) {
			// set the owning side to null (unless already changed)
			if ($station->getWorksite() === $this) {
				$station->setWorksite(null);
			}
		}

		return $this;
	}

	/**
	 * @return int
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	public function getSsolNumber(): int
	{
		return count($this->stations->filter(fn($s) => $s->getType()->getShortName() === 'ssol' && $s->getIsInstalled() && !$s->getIsSuspended()));
	}

	/**
	 * @return int
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	public function getSbNumber(): int
	{
		return count($this->stations->filter(fn($s) => $s->getType()->getShortName() === 'sb' && $s->getIsInstalled() && !$s->getIsSuspended()));
	}

	/**
	 * @return int
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	public function getVisitNumber(): int
	{
		return count($this->visits->filter(fn($v) => $v->getIsCompleted()));
	}

	/**
	 * @return int
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	public function getNotesNumber(): int
	{
		return count($this->notes);
	}


	public function getCode(): ?int
	{
		return $this->code;
	}

	public function setCode(int $code): self
	{
		$this->code = $code;

		return $this;
	}

	public function getStatus(): ?WorksiteStatus
	{
		return $this->status;
	}

	public function setStatus(?WorksiteStatus $status): self
	{
		$this->status = $status;

		return $this;
	}

	public function getNextVisitType(): ?VisitType
	{
		return $this->next_visit_type;
	}

	public function setNextVisitType(?VisitType $next_visit_type): self
	{
		$this->next_visit_type = $next_visit_type;

		return $this;
	}

	public function getInstallationDate(): ?DateTimeInterface
	{
		return $this->installation_date;
	}

	public function setInstallationDate(DateTimeInterface $installation_date): self
	{
		$this->installation_date = $installation_date;

		return $this;
	}

	public function getNextVisitLimitDate(): ?DateTimeInterface
	{
		return $this->next_visit_limit_date;
	}

	public function setNextVisitLimitDate(DateTimeInterface $next_visit_limit_date): self
	{
		$this->next_visit_limit_date = $next_visit_limit_date;

		return $this;
	}

	public function getProtectedBuiltSurface(): ?int
	{
		return $this->protected_built_surface;
	}

	public function setProtectedBuiltSurface(int $protected_built_surface): self
	{
		$this->protected_built_surface = $protected_built_surface;

		return $this;
	}

	public function getProtectedBuiltPerimeter(): ?int
	{
		return $this->protected_built_perimeter;
	}

	public function setProtectedBuiltPerimeter(int $protected_built_perimeter): self
	{
		$this->protected_built_perimeter = $protected_built_perimeter;

		return $this;
	}

	public function getProtectedUndevelopedSurface(): ?int
	{
		return $this->protected_undeveloped_surface;
	}

	public function setProtectedUndevelopedSurface(int $protected_undeveloped_surface): self
	{
		$this->protected_undeveloped_surface = $protected_undeveloped_surface;

		return $this;
	}

	public function getProtectedIndevelopedPerimeter(): ?int
	{
		return $this->protected_indeveloped_perimeter;
	}

	public function setProtectedIndevelopedPerimeter(int $protected_indeveloped_perimeter): self
	{
		$this->protected_indeveloped_perimeter = $protected_indeveloped_perimeter;

		return $this;
	}

	/**
	 * @return Collection
	 */
	public function getContacts(): Collection
	{
		return $this->contacts;
	}

	public function addContact(Contact $contact): self
	{
		if (!$this->contacts->contains($contact)) {
			$this->contacts[] = $contact;
		}

		return $this;
	}

	public function removeContact(Contact $contact): self
	{
		$this->contacts->removeElement($contact);

		return $this;
	}

	public function getBuiltControlDuration(): ?DateInterval
	{
		return $this->builtControlDuration;
	}

	public function setBuiltControlDuration(?DateInterval $builtControlDuration): self
	{
		$this->builtControlDuration = $builtControlDuration;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getNextScheduledVisits(): array
	{
		return $this->nextScheduledVisits;
	}

	/**
	 * @param array $nextScheduledVisits
	 * @return Worksite
	 */
	public function setNextScheduledVisits(array $nextScheduledVisits): Worksite
	{
		$this->nextScheduledVisits = $nextScheduledVisits;
		return $this;
	}

	public function getVisitPerYear(): ?int
	{
		return $this->visit_per_year;
	}

	public function setVisitPerYear(?int $visit_per_year): self
	{
		$this->visit_per_year = $visit_per_year;

		return $this;
	}

	public function getAnnualVisitDate(): ?DateTimeInterface
	{
		return $this->annual_visit_date;
	}

	public function setAnnualVisitDate(?DateTimeInterface $annual_visit_date): self
	{
		$this->annual_visit_date = $annual_visit_date;

		return $this;
	}

	/**
	 * @return Collection|EliminationPeriod[]
	 */
	public function getEliminationPeriods(): Collection
	{
		return $this->eliminationPeriods;
	}

	public function addEliminationPeriod(EliminationPeriod $eliminationPeriod): self
	{
		if (!$this->eliminationPeriods->contains($eliminationPeriod)) {
			$this->eliminationPeriods[] = $eliminationPeriod;
			$eliminationPeriod->setWorksite($this);
		}

		return $this;
	}

	public function removeEliminationPeriod(EliminationPeriod $eliminationPeriod): self
	{
		if ($this->eliminationPeriods->removeElement($eliminationPeriod)) {
			// set the owning side to null (unless already changed)
			if ($eliminationPeriod->getWorksite() === $this) {
				$eliminationPeriod->setWorksite(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|VisitStepTypeAdjustment[]
	 */
	public function getVisitStepTypeAdjustments(): Collection
	{
		return $this->visitStepTypeAdjustments;
	}

	public function addVisitStepTypeAdjustment(VisitStepTypeAdjustment $visitStepTypeAdjustment): self
	{
		if (!$this->visitStepTypeAdjustments->contains($visitStepTypeAdjustment)) {
			$this->visitStepTypeAdjustments[] = $visitStepTypeAdjustment;
			$visitStepTypeAdjustment->setWorksite($this);
		}

		return $this;
	}

	public function removeVisitStepTypeAdjustment(VisitStepTypeAdjustment $visitStepTypeAdjustment): self
	{
		if ($this->visitStepTypeAdjustments->removeElement($visitStepTypeAdjustment)) {
			// set the owning side to null (unless already changed)
			if ($visitStepTypeAdjustment->getWorksite() === $this) {
				$visitStepTypeAdjustment->setWorksite(null);
			}
		}

		return $this;
	}
}

