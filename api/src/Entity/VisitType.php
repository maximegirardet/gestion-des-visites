<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Filter\VisitTypeFilter;
use App\Repository\VisitTypeRepository;
use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=VisitTypeRepository::class)
 */
#[
	ApiResource(
		collectionOperations: [
		'get' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]],
		'post' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]]],
		itemOperations: [
		'get' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]],
		'put' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]],
		'delete' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]],
		'patch' => [
			'openapi_context' => [
				'security' => [['bearerAuth' => []]]
			]]],
		normalizationContext: ['groups' => ['worksite', 'visit_type']],
		security: 'is_granted("ROLE_USER")'
	)]
#[ApiFilter(SearchFilter::class, properties: ['worksite_status.short_name' => 'exact'])]
#[ApiFilter(BooleanFilter::class, properties: ['is_allowed_from_installation_only'])]
#[ApiFilter(VisitTypeFilter::class)]
class VisitType
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	private $id;

	/**
	 * @ORM\ManyToMany(targetEntity=VisitStepType::class)
	 */
	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	public $steps;

	/**
	 * @ORM\OneToMany(targetEntity=Worksite::class, mappedBy="next_visit_type")
	 */
	private $worksites;

	/**
	 * @ORM\OneToMany(targetEntity=Visit::class, mappedBy="type")
	 */
	private $visits;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	private $short_name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	private $long_name;

	/**
	 * @ORM\Column(type="dateinterval", nullable=true)
	 */
	#[Groups(['visit_type'])]
	private $delay_between_visits;

	/**
	 * @ORM\ManyToOne(targetEntity=WorksiteStatus::class, inversedBy="visitTypes")
	 * @ORM\JoinColumn(nullable=true)
	 */
	#[Groups(['visit_type'])]
	private $worksite_status;

	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups(['visit_type'])]
	private $is_allowed_from_installation_only;

	/**
	 * @ORM\Column(type="boolean")
	 */
	#[Groups(['visit_type'])]
	private $has_fixed_planification;

	private ?int $worksite_id;

	/**
	 * @ORM\Column(type="dateinterval")
	 */
	#[Groups(['visit_type'])]
	private $relational_time;

	public function __construct()
	{
		$this->steps = new ArrayCollection();
		$this->worksites = new ArrayCollection();
		$this->visits = new ArrayCollection();
	}

	#[Groups('visit_type')]
	public function getId(): ?int
	{
		return $this->id;
	}

	#[Groups(['worksite', 'worksite_summary', 'visit_type'])]
	public function getCode(): ?string
	{
		return implode(" + ", $this->steps->map(fn($s) => $s->getCode())->toArray());
	}

	/**
	 * @return Collection|VisitStepType[]
	 */
	public function getSteps(): Collection
	{
		return $this->steps;
	}

	public function addStep(VisitStepType $step): self
	{
		if (!$this->steps->contains($step)) {
			$this->steps[] = $step;
		}

		return $this;
	}

	public function removeStep(VisitStepType $step): self
	{
		$this->steps->removeElement($step);

		return $this;
	}

	/**
	 * @return Collection|Worksite[]
	 */
	public function getWorksites(): Collection
	{
		return $this->worksites;
	}

	public function addWorksite(Worksite $worksite): self
	{
		if (!$this->worksites->contains($worksite)) {
			$this->worksites[] = $worksite;
			$worksite->setNextVisitType($this);
		}

		return $this;
	}

	public function removeWorksite(Worksite $worksite): self
	{
		if ($this->worksites->removeElement($worksite)) {
			// set the owning side to null (unless already changed)
			if ($worksite->getNextVisitType() === $this) {
				$worksite->setNextVisitType(null);
			}
		}

		return $this;
	}

	/**
	 * @return Collection|Visit[]
	 */
	public function getVisits(): Collection
	{
		return $this->visits;
	}

	public function addVisit(Visit $visit): self
	{
		if (!$this->visits->contains($visit)) {
			$this->visits[] = $visit;
			$visit->setType($this);
		}

		return $this;
	}

	public function removeVisit(Visit $visit): self
	{
		if ($this->visits->removeElement($visit)) {
			// set the owning side to null (unless already changed)
			if ($visit->getType() === $this) {
				$visit->setType(null);
			}
		}

		return $this;
	}

	public function getShortName(): ?string
	{
		return $this->short_name;
	}

	public function setShortName(string $short_name): self
	{
		$this->short_name = $short_name;

		return $this;
	}

	public function getLongName(): ?string
	{
		return $this->long_name;
	}

	public function setLongName(string $long_name): self
	{
		$this->long_name = $long_name;

		return $this;
	}

	public function getDelayBetweenVisits(): ?DateInterval
	{
		return $this->delay_between_visits;
	}

	public function setDelayBetweenVisits(?DateInterval $delay_between_visits): self
	{
		$this->delay_between_visits = $delay_between_visits;

		return $this;
	}

	public function getWorksiteStatus(): ?WorksiteStatus
	{
		return $this->worksite_status;
	}

	public function setWorksiteStatus(?WorksiteStatus $worksite_status): self
	{
		$this->worksite_status = $worksite_status;

		return $this;
	}

	public function getIsAllowedFromInstallationOnly(): ?bool
	{
		return $this->is_allowed_from_installation_only;
	}

	public function setIsAllowedFromInstallationOnly(bool $is_allowed_from_installation_only): self
	{
		$this->is_allowed_from_installation_only = $is_allowed_from_installation_only;

		return $this;
	}

	public function getHasFixedPlanification(): ?bool
	{
		return $this->has_fixed_planification;
	}

	public function setHasFixedPlanification(bool $has_fixed_planification): self
	{
		$this->has_fixed_planification = $has_fixed_planification;

		return $this;
	}

	/**
	 * @return int|null
	 */
	public function getWorksiteId(): ?int
	{
		return $this->worksite_id;
	}

	/**
	 * @param int|null $worksite_id
	 * @return VisitType
	 */
	public function setWorksiteId(?int $worksite_id): VisitType
	{
		$this->worksite_id = $worksite_id;
		return $this;
	}

	public function getRelationalTime(): ?DateInterval
	{
		return $this->relational_time;
	}

	public function setRelationalTime(DateInterval $relational_time): self
	{
		$this->relational_time = $relational_time;

		return $this;
	}
}
