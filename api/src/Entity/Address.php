<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=AddressRepository::class)
 */
#[ApiResource(normalizationContext: ['groups' => ['worksite']])]
class Address
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $street;

	/**
	 * @ORM\Column(type="integer")
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $postal_code;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	#[Groups(['worksite', 'worksite_summary'])]
	private $city;

	/**
	 * @ORM\OneToMany(targetEntity=Worksite::class, mappedBy="address")
	 */
	private $worksites;

	public function __construct()
	{
		$this->worksites = new ArrayCollection();
	}

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getStreet(): ?string
	{
		return $this->street;
	}

	public function setStreet(string $street): self
	{
		$this->street = $street;

		return $this;
	}

	public function getPostalCode(): ?int
	{
		return $this->postal_code;
	}

	public function setPostalCode(int $postal_code): self
	{
		$this->postal_code = $postal_code;

		return $this;
	}

	public function getCity(): ?string
	{
		return $this->city;
	}

	public function setCity(string $city): self
	{
		$this->city = $city;

		return $this;
	}

	/**
	 * @return Collection|Worksite[]
	 */
	public function getWorksites(): Collection
	{
		return $this->worksites;
	}

	public function addWorksite(Worksite $worksite): self
	{
		if (!$this->worksites->contains($worksite)) {
			$this->worksites[] = $worksite;
			$worksite->setAddress($this);
		}

		return $this;
	}

	public function removeWorksite(Worksite $worksite): self
	{
		if ($this->worksites->removeElement($worksite)) {
			// set the owning side to null (unless already changed)
			if ($worksite->getAddress() === $this) {
				$worksite->setAddress(null);
			}
		}

		return $this;
	}
}
