<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210807152513 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE predefined_observations_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE predefined_observations_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE predefined_observations (id INT NOT NULL, type_id INT NOT NULL, number INT NOT NULL, text TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_836F3E58C54C8C93 ON predefined_observations (type_id)');
        $this->addSql('CREATE TABLE predefined_observations_type (id INT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE predefined_observations ADD CONSTRAINT FK_836F3E58C54C8C93 FOREIGN KEY (type_id) REFERENCES predefined_observations_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE predefined_observations DROP CONSTRAINT FK_836F3E58C54C8C93');
        $this->addSql('DROP SEQUENCE predefined_observations_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE predefined_observations_type_id_seq CASCADE');
        $this->addSql('DROP TABLE predefined_observations');
        $this->addSql('DROP TABLE predefined_observations_type');
    }
}
