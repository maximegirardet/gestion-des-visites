<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630121139 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE station_status_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE station_status (id INT NOT NULL, visit_id INT NOT NULL, station_id INT NOT NULL, is_connected BOOLEAN NOT NULL, consumption INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B4E276FA75FA0FF2 ON station_status (visit_id)');
        $this->addSql('CREATE INDEX IDX_B4E276FA21BDB235 ON station_status (station_id)');
        $this->addSql('ALTER TABLE station_status ADD CONSTRAINT FK_B4E276FA75FA0FF2 FOREIGN KEY (visit_id) REFERENCES visit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE station_status ADD CONSTRAINT FK_B4E276FA21BDB235 FOREIGN KEY (station_id) REFERENCES station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE station DROP CONSTRAINT fk_9f39f8b175fa0ff2');
        $this->addSql('DROP INDEX idx_9f39f8b175fa0ff2');
        $this->addSql('ALTER TABLE station ADD worksite_id INT NOT NULL');
        $this->addSql('ALTER TABLE station DROP visit_id');
        $this->addSql('ALTER TABLE station DROP is_connected');
        $this->addSql('ALTER TABLE station DROP consumption');
        $this->addSql('ALTER TABLE station ADD CONSTRAINT FK_9F39F8B1A47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9F39F8B1A47737E7 ON station (worksite_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE station_status_id_seq CASCADE');
        $this->addSql('DROP TABLE station_status');
        $this->addSql('ALTER TABLE station DROP CONSTRAINT FK_9F39F8B1A47737E7');
        $this->addSql('DROP INDEX IDX_9F39F8B1A47737E7');
        $this->addSql('ALTER TABLE station ADD is_connected BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE station ADD consumption INT NOT NULL');
        $this->addSql('ALTER TABLE station RENAME COLUMN worksite_id TO visit_id');
        $this->addSql('ALTER TABLE station ADD CONSTRAINT fk_9f39f8b175fa0ff2 FOREIGN KEY (visit_id) REFERENCES visit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_9f39f8b175fa0ff2 ON station (visit_id)');
    }
}
