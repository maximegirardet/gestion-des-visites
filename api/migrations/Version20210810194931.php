<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210810194931 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE station_status ADD is_removed BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE station_status ADD is_recharged BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE station_status ADD is_replaced BOOLEAN DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE station_status DROP is_removed');
        $this->addSql('ALTER TABLE station_status DROP is_recharged');
        $this->addSql('ALTER TABLE station_status DROP is_replaced');
    }
}
