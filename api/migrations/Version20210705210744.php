<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210705210744 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE worksite_contacts DROP CONSTRAINT fk_67249aea719fb48e');
        $this->addSql('DROP SEQUENCE contacts_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE contact_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE contact (id INT NOT NULL, name VARCHAR(255) NOT NULL, phone_number VARCHAR(10) NOT NULL, email_address VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4C62E6386B01BC5B ON contact (phone_number)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4C62E638B08E074E ON contact (email_address)');
        $this->addSql('CREATE TABLE worksite_contact (worksite_id INT NOT NULL, contact_id INT NOT NULL, PRIMARY KEY(worksite_id, contact_id))');
        $this->addSql('CREATE INDEX IDX_F6BA56CEA47737E7 ON worksite_contact (worksite_id)');
        $this->addSql('CREATE INDEX IDX_F6BA56CEE7A1254A ON worksite_contact (contact_id)');
        $this->addSql('ALTER TABLE worksite_contact ADD CONSTRAINT FK_F6BA56CEA47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE worksite_contact ADD CONSTRAINT FK_F6BA56CEE7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE worksite_contacts');
        $this->addSql('DROP TABLE contacts');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE worksite_contact DROP CONSTRAINT FK_F6BA56CEE7A1254A');
        $this->addSql('DROP SEQUENCE contact_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE contacts_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE worksite_contacts (worksite_id INT NOT NULL, contacts_id INT NOT NULL, PRIMARY KEY(worksite_id, contacts_id))');
        $this->addSql('CREATE INDEX idx_67249aeaa47737e7 ON worksite_contacts (worksite_id)');
        $this->addSql('CREATE INDEX idx_67249aea719fb48e ON worksite_contacts (contacts_id)');
        $this->addSql('CREATE TABLE contacts (id INT NOT NULL, name VARCHAR(255) NOT NULL, phone_number VARCHAR(10) NOT NULL, email_address VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_33401573b08e074e ON contacts (email_address)');
        $this->addSql('CREATE UNIQUE INDEX uniq_334015736b01bc5b ON contacts (phone_number)');
        $this->addSql('ALTER TABLE worksite_contacts ADD CONSTRAINT fk_67249aeaa47737e7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE worksite_contacts ADD CONSTRAINT fk_67249aea719fb48e FOREIGN KEY (contacts_id) REFERENCES contacts (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE worksite_contact');
    }
}
