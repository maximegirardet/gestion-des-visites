<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210923141833 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE visit_step_type_adjustment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE visit_step_type_adjustment (id INT NOT NULL, visit_step_type_id INT NOT NULL, worksite_id INT NOT NULL, created_by_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, duration VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4733306BA3E3733D ON visit_step_type_adjustment (visit_step_type_id)');
        $this->addSql('CREATE INDEX IDX_4733306BA47737E7 ON visit_step_type_adjustment (worksite_id)');
        $this->addSql('CREATE INDEX IDX_4733306BB03A8386 ON visit_step_type_adjustment (created_by_id)');
        $this->addSql('COMMENT ON COLUMN visit_step_type_adjustment.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN visit_step_type_adjustment.duration IS \'(DC2Type:dateinterval)\'');
        $this->addSql('ALTER TABLE visit_step_type_adjustment ADD CONSTRAINT FK_4733306BA3E3733D FOREIGN KEY (visit_step_type_id) REFERENCES visit_step_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE visit_step_type_adjustment ADD CONSTRAINT FK_4733306BA47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE visit_step_type_adjustment ADD CONSTRAINT FK_4733306BB03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE visit_step_type_adjustment_id_seq CASCADE');
        $this->addSql('DROP TABLE visit_step_type_adjustment');
    }
}
