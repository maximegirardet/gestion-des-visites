<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210813173355 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE elimination_period_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE elimination_period (id INT NOT NULL, start_visit_id INT NOT NULL, end_visit_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_39E1BFF76AD049C0 ON elimination_period (start_visit_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_39E1BFF7ACB99A7F ON elimination_period (end_visit_id)');
        $this->addSql('ALTER TABLE elimination_period ADD CONSTRAINT FK_39E1BFF76AD049C0 FOREIGN KEY (start_visit_id) REFERENCES visit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE elimination_period ADD CONSTRAINT FK_39E1BFF7ACB99A7F FOREIGN KEY (end_visit_id) REFERENCES visit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE elimination_period_id_seq CASCADE');
        $this->addSql('DROP TABLE elimination_period');
    }
}
