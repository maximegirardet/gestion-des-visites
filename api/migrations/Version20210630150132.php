<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630150132 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE visit_step_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE visit_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE visit_step_type (id INT NOT NULL, short_name VARCHAR(255) NOT NULL, long_name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE visit_type (id INT NOT NULL, short_name VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE visit_type_visit_step_type (visit_type_id INT NOT NULL, visit_step_type_id INT NOT NULL, PRIMARY KEY(visit_type_id, visit_step_type_id))');
        $this->addSql('CREATE INDEX IDX_6D9EF99B93C58854 ON visit_type_visit_step_type (visit_type_id)');
        $this->addSql('CREATE INDEX IDX_6D9EF99BA3E3733D ON visit_type_visit_step_type (visit_step_type_id)');
        $this->addSql('ALTER TABLE visit_type_visit_step_type ADD CONSTRAINT FK_6D9EF99B93C58854 FOREIGN KEY (visit_type_id) REFERENCES visit_type (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE visit_type_visit_step_type ADD CONSTRAINT FK_6D9EF99BA3E3733D FOREIGN KEY (visit_step_type_id) REFERENCES visit_step_type (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE visit ADD type_id INT NOT NULL');
        $this->addSql('ALTER TABLE visit ADD CONSTRAINT FK_437EE939C54C8C93 FOREIGN KEY (type_id) REFERENCES visit_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_437EE939C54C8C93 ON visit (type_id)');
        $this->addSql('ALTER TABLE worksite ADD next_visit_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE worksite ADD CONSTRAINT FK_5E464782D3CD8C32 FOREIGN KEY (next_visit_type_id) REFERENCES visit_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5E464782D3CD8C32 ON worksite (next_visit_type_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE visit_type_visit_step_type DROP CONSTRAINT FK_6D9EF99BA3E3733D');
        $this->addSql('ALTER TABLE visit DROP CONSTRAINT FK_437EE939C54C8C93');
        $this->addSql('ALTER TABLE visit_type_visit_step_type DROP CONSTRAINT FK_6D9EF99B93C58854');
        $this->addSql('ALTER TABLE worksite DROP CONSTRAINT FK_5E464782D3CD8C32');
        $this->addSql('DROP SEQUENCE visit_step_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE visit_type_id_seq CASCADE');
        $this->addSql('DROP TABLE visit_step_type');
        $this->addSql('DROP TABLE visit_type');
        $this->addSql('DROP TABLE visit_type_visit_step_type');
        $this->addSql('DROP INDEX IDX_437EE939C54C8C93');
        $this->addSql('ALTER TABLE visit DROP type_id');
        $this->addSql('DROP INDEX IDX_5E464782D3CD8C32');
        $this->addSql('ALTER TABLE worksite DROP next_visit_type_id');
    }
}
