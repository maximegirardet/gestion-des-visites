<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210813205128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE elimination_period ADD worksite_id INT NOT NULL');
        $this->addSql('ALTER TABLE elimination_period ADD CONSTRAINT FK_39E1BFF7A47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_39E1BFF7A47737E7 ON elimination_period (worksite_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE elimination_period DROP CONSTRAINT FK_39E1BFF7A47737E7');
        $this->addSql('DROP INDEX IDX_39E1BFF7A47737E7');
        $this->addSql('ALTER TABLE elimination_period DROP worksite_id');
    }
}
