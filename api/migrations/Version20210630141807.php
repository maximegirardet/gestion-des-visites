<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630141807 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE worksite_status_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE worksite_status (id INT NOT NULL, short_name VARCHAR(255) NOT NULL, long_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE worksite ADD status_id INT NOT NULL');
        $this->addSql('ALTER TABLE worksite ADD code INT NOT NULL');
        $this->addSql('ALTER TABLE worksite ADD CONSTRAINT FK_5E4647826BF700BD FOREIGN KEY (status_id) REFERENCES worksite_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5E4647826BF700BD ON worksite (status_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE worksite DROP CONSTRAINT FK_5E4647826BF700BD');
        $this->addSql('DROP SEQUENCE worksite_status_id_seq CASCADE');
        $this->addSql('DROP TABLE worksite_status');
        $this->addSql('DROP INDEX IDX_5E4647826BF700BD');
        $this->addSql('ALTER TABLE worksite DROP status_id');
        $this->addSql('ALTER TABLE worksite DROP code');
    }
}
