<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210810195214 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE station_status ALTER is_removed SET NOT NULL');
        $this->addSql('ALTER TABLE station_status ALTER is_recharged SET NOT NULL');
        $this->addSql('ALTER TABLE station_status ALTER is_replaced SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE station_status ALTER is_removed DROP NOT NULL');
        $this->addSql('ALTER TABLE station_status ALTER is_recharged DROP NOT NULL');
        $this->addSql('ALTER TABLE station_status ALTER is_replaced DROP NOT NULL');
    }
}
