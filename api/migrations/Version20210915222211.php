<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210915222211 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX uniq_9f39f8b1a47737e796901f54c54c8c93');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9F39F8B1A47737E796901F54C54C8C93F5A3A655 ON station (worksite_id, number, type_id, index_name)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_9F39F8B1A47737E796901F54C54C8C93F5A3A655');
        $this->addSql('CREATE UNIQUE INDEX uniq_9f39f8b1a47737e796901f54c54c8c93 ON station (worksite_id, number, type_id)');
    }
}
