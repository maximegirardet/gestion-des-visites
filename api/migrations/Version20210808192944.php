<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210808192944 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE visit_type ADD worksite_status_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE visit_type ADD delay_between_visits INT DEFAULT NULL');
        $this->addSql('ALTER TABLE visit_type ADD CONSTRAINT FK_5386AAEBE2704FD2 FOREIGN KEY (worksite_status_id) REFERENCES worksite_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5386AAEBE2704FD2 ON visit_type (worksite_status_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE visit_type DROP CONSTRAINT FK_5386AAEBE2704FD2');
        $this->addSql('DROP INDEX IDX_5386AAEBE2704FD2');
        $this->addSql('ALTER TABLE visit_type DROP worksite_status_id');
        $this->addSql('ALTER TABLE visit_type DROP delay_between_visits');
    }
}
