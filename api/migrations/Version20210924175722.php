<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210924175722 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE visit_step_type_adjustment ADD added_during_visit_id INT NOT NULL');
        $this->addSql('ALTER TABLE visit_step_type_adjustment ADD CONSTRAINT FK_4733306B5EE24A7B FOREIGN KEY (added_during_visit_id) REFERENCES visit (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_4733306B5EE24A7B ON visit_step_type_adjustment (added_during_visit_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE visit_step_type_adjustment DROP CONSTRAINT FK_4733306B5EE24A7B');
        $this->addSql('DROP INDEX IDX_4733306B5EE24A7B');
        $this->addSql('ALTER TABLE visit_step_type_adjustment DROP added_during_visit_id');
    }
}
