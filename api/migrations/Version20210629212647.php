<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210629212647 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE address_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE station_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE station_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE worksite_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE address (id INT NOT NULL, street VARCHAR(255) NOT NULL, postal_code INT NOT NULL, city VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE station (id INT NOT NULL, type_id INT NOT NULL, worksite_id INT NOT NULL, number INT NOT NULL, is_connected BOOLEAN NOT NULL, consumption INT NOT NULL, is_suspended BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9F39F8B1C54C8C93 ON station (type_id)');
        $this->addSql('CREATE INDEX IDX_9F39F8B1A47737E7 ON station (worksite_id)');
        $this->addSql('CREATE TABLE station_type (id INT NOT NULL, short_name VARCHAR(255) NOT NULL, long_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE worksite (id INT NOT NULL, address_id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5E464782F5B7AF75 ON worksite (address_id)');
        $this->addSql('ALTER TABLE station ADD CONSTRAINT FK_9F39F8B1C54C8C93 FOREIGN KEY (type_id) REFERENCES station_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE station ADD CONSTRAINT FK_9F39F8B1A47737E7 FOREIGN KEY (worksite_id) REFERENCES worksite (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE worksite ADD CONSTRAINT FK_5E464782F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE worksite DROP CONSTRAINT FK_5E464782F5B7AF75');
        $this->addSql('ALTER TABLE station DROP CONSTRAINT FK_9F39F8B1C54C8C93');
        $this->addSql('ALTER TABLE station DROP CONSTRAINT FK_9F39F8B1A47737E7');
        $this->addSql('DROP SEQUENCE address_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE station_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE station_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE worksite_id_seq CASCADE');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE station');
        $this->addSql('DROP TABLE station_type');
        $this->addSql('DROP TABLE worksite');
    }
}
