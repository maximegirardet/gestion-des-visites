<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210808194217 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE visit_type ADD is_allowed_from_installation_only BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE visit_type ADD has_fixed_planification BOOLEAN DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE visit_type DROP is_allowed_from_installation_only');
        $this->addSql('ALTER TABLE visit_type DROP has_fixed_planification');
        $this->addSql('ALTER TABLE visit_type ALTER worksite_status_id DROP NOT NULL');
    }
}
