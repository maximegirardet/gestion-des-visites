<?php

namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;

class AddressTest extends ApiTestCase
{
    public function testCreateAddress()
    {
        static::createClient()->request('POST', '/addresses', ['json' => [
            'street' => '800 Avenue du Parc des Expositions',
	        'postalCode' => 33260,
	        "city" => "LA TESTE DE BUCH"
        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertJsonContains([
            '@context' => '/contexts/Address',
            '@type' => 'Address',
	        'street' => '800 Avenue du Parc des Expositions',
	        'postal_code' => 33260,
	        "city" => "LA TESTE DE BUCH"
        ]);
    }
}
