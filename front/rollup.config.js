import noderesolve from '@rollup/plugin-node-resolve';
import replace from "@rollup/plugin-replace";

export default {
	input: process.env.NODE_ENV === 'production'? 'sw.js' : 'front/sw.js',
	output: {
		file: process.env.NODE_ENV === 'production'? 'sw.build.js' : 'front/sw.build.js',
		format: 'iife'
	},
	plugins: [
		noderesolve(), // prise en charge des modules depuis node_modules
		replace({
			'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
		})
	]

};