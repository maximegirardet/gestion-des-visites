import {findHours, findMinutes, findMonths, invert} from "pomeranian-durations"

/**
 * Formats a date into french format.
 * @param date {string} The date to format (yyyy-mm-dd)
 * @returns {string} The formatted date (dd/mm/yyyy)
 */
export function getFormattedDate(date) {
	return new Date(date).toLocaleDateString('fr-FR')
}

/**
 * Formats a datetime into french format for date and time .
 * @param date {string} The date to format (yyyy-mm-dd) or an ISO DateTime string
 * @returns {string} The formatted date (dd/mm/yyyy – ..h..)
 */
export function getFormattedDateAndTime(date) {
	const dateObject = new Date(date)
	return `${dateObject.toLocaleDateString('fr-FR')} – ${dateObject.getHours()}h${formatMinutes(dateObject.getMinutes())}`
}

/**
 * Formats a phone number into french format (06 xx xx xx xx)
 * @param phone {string} The original phone number
 * @returns {string}  The formatted phone number
 */
export function getFormattedPhoneNumber(phone) {
	return phone.match(/.{1,2}/g).join(" ")
}

/**
 * Formats a date into API format (yyyy-mm-dd)
 * @param date {Date} A Date object
 * @returns {string} API-formatted date
 */
export function getAPIFormattedDate(date) {
	return new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000).toISOString().split('T')[0]
}

/**
 * Gets ISO8601 duration from a signed duration
 * @param duration ISO8601 signed duration
 * @returns {string|*}
 * @constructor
 */
export function ISO8601NegativeNormalize(duration) {
	if (duration.charAt(0) === "-") {
		return invert(duration)
	}
	return duration
}

/**
 * Parses an ISO 8601 Duration into months, hours, minutes
 * @param duration
 * @returns {{hours: number, months: number, minutes: number}}
 * @constructor
 */
export function ISO8601Parse(duration) {
	duration = ISO8601NegativeNormalize(duration)
	const months = findMonths(duration) ?? 0
	const hours = findHours(duration) ?? 0
	const minutes = findMinutes(duration) ?? 0
	return {months, hours, minutes}
}

/**
 * Formats an ISO 8601 interval string into a french .h..min format.
 * @param duration {string} An ISO 8601 interval string
 * @returns {string} A well formatted duration
 */
export function getFormattedDuration(duration) {
	const {hours, minutes} = ISO8601Parse(duration)
	if (hours > 0) {
		return `${hours}h${formatMinutes(minutes)}`
	}
	return `${minutes} min`
}

/**
 * Formats minutes into a french format (ex : 05 min instead of 5 min)
 * @param minutes The minutes to format
 * @returns {string}
 */
function formatMinutes(minutes) {
	return `${minutes < 10 ? '0' : ''}${minutes}`
}

/**
 * Determines if two values are equal (deep comparison)
 * @param a
 * @param b
 * @returns {boolean}
 */
export function equals(a, b) {
	if (a === b) return true
	if (a instanceof Date && b instanceof Date)
		return a.getTime() === b.getTime()
	if (!a || !b || (typeof a !== 'object' && typeof b !== 'object'))
		return a === b
	if (a.prototype !== b.prototype) return false
	let keys = Object.keys(a)
	if (keys.length !== Object.keys(b).length) return false
	return keys.every(k => equals(a[k], b[k]))
}

/**
 * Returns the last element of an array if it exists, null if not
 * @param array
 * @returns {null|*}
 */
export function getLastElementArray(array) {
	if (array.length > 0) {
		return array[array.length - 1]
	}
	return null
}

/**
 * Creates className string from an array of classes
 * @param classes
 * @returns {string}
 */
export default function classNames(...classes) {
	return classes.filter(Boolean).join(' ')
}

/**
 * Returns the real result of n modulo m (positive number)
 * @param n
 * @param m
 * @returns {number}
 */
export function mod(n, m) {
	return ((n % m) + m) % m;
}
