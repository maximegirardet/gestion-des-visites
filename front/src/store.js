import {combineReducers, configureStore} from "@reduxjs/toolkit";
import stationsReducer from "./components/Visit/Stations/stationsSlice";
import storage from 'redux-persist/lib/storage'
import {FLUSH, PAUSE, PERSIST, persistReducer, PURGE, REGISTER, REHYDRATE} from "redux-persist";
import observationsReducer from "./components/Visit/Observations/ObservationsSlice";
import eliminationsReducer from "./components/Visit/Eliminations/EliminationsSlice";
import pagesReducer from "./components/Visit/Pages/PagesSlice";
import stationsReplicaReducer from "./components/Visit/Stations/stationsReplicaSlice";
import nextVisitReducer from "./components/Visit/NextVisit/nextVisitSlice"

const reducers = combineReducers({
	stations: stationsReducer,
	stationsReplica: stationsReplicaReducer,
	observations: observationsReducer,
	eliminations: eliminationsReducer,
	nextVisit: nextVisitReducer,
	pages: pagesReducer
});

const persistConfig = {
	key: 'root',
	storage
};

const persistedReducer = persistReducer(persistConfig, reducers);


const store = configureStore({
	reducer: persistedReducer,
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: {
				ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
			},
		}),
});

export default store
