import React, {useEffect, useRef, useState} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import Nav from './components/Nav'

import DashboardPage from './views/DashboardPage'
import NotFound from './views/NotFound'
import Login from "./views/Login"
import {getUser} from "./services/authentication"
import Loading from "./views/Loading"
import VisitPage from "./views/VisitPage"

const App = () => {
	const token = useRef()
	const [user, setUser] = useState(null)
	const [loading, setLoading] = useState(true)
	const [logged, setLogged] = useState(false)

	useEffect(() => {
		if (!user) {
			getUser(token, setUser, setLoading)
		}
	}, [logged])

	if (loading) {
		return <Loading/>
	}

	if (!user) {
		return <Login ref={token} state={{setLogged, setLoading}}/>
	}

	return (
		<Router>
			<Nav/>
			<Switch>
				<Route exact path="/">
					<DashboardPage token={token} setLogged={setLogged}/>
				</Route>
				<Route path="/dashboard">
					<DashboardPage token={token} setLogged={setLogged}/>
				</Route>
				<Route path="/report/:event_id">
					<VisitPage token={token}/>
				</Route>
				<Route path="/login">
					<Login/>
				</Route>
				<Route>
					<NotFound/>
				</Route>
			</Switch>
		</Router>
	)
}

export default App
