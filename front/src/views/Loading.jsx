import React from 'react';
import Loader from "../components/Loader";

const Loading = () => {

	return (
		<React.Fragment>
			<main>
				<div className={"flex justify-center align-middle h-screen items-center loading"}>
					<Loader/>
				</div>
			</main>
		</React.Fragment>
	);
};

export default Loading;
