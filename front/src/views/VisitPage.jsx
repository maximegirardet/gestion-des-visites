import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom"
import Header from '../components/Header';
import DashboardItem from "../components/DashboardItem";
import {searchWorksites} from "../services/visit";
import Visit from "../Icons/Visit";
import IconButton from "../components/IconButton";
import Page from "../components/Visit/Pages/Page";
import {useFetchAPI} from "../services/authentication"


export default function VisitPage({token}) {
	const [visit, setVisit] = useState(null)
	const [showBar, setShowBar] = useState(false)

	const {event_id} = useParams()

	const API = useFetchAPI()

	useEffect(() => {
		searchWorksites(API, token, event_id).then(setVisit)
	}, [])

	return (
		<React.Fragment>
			<main
				className={"bg-gray-100 dark:bg-gray-800 flex flex-col lg:grid w-full visit-page-grid fixed overflow-y-scroll"}>
				<div className={"sidebar hidden lg:flex"}>
					{visit && <DashboardItem worksite={visit} isVisit={true}/>}
				</div>
				<div className={"ml-4 lg:ml-0 my-2 pr-4 w-full"}>
					<Header icon={<IconButton icon={<Visit/>} onClick={() => setShowBar(!showBar)}/>}>
						Compte rendu de visite
					</Header>
					{visit && !showBar && <Page visit={visit} event_id={event_id} token={token} setVisit={setVisit}/>}
				</div>
				<div className={"lg:hidden mt-4"}>
					{visit && showBar && <DashboardItem worksite={visit} isVisit={true}/>}
				</div>
			</main>
		</React.Fragment>
	)
}
