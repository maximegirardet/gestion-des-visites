import React, {forwardRef} from 'react';
import LoginForm from "../components/LoginForm";

const Login = (props, ref) => {

	return (
		<React.Fragment>
			<main>
				<LoginForm state={props.state} ref={ref}/>
			</main>
		</React.Fragment>
	);
};

export default forwardRef(Login);
