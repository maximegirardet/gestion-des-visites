import React, {useCallback, useEffect, useState} from 'react';
import Header from '../components/Header';
import DashboardItem from "../components/DashboardItem";
import SearchBar from "../components/SearchBar";
import {getScheduledVisits, searchWorksites} from "../services/dashboard";
import {getAPIFormattedDate} from "../utils/functions";
import DashBoardTitleDate from "../components/Dashboard/DashBoardTitleDate";
import Loading from "../components/Dashboard/Loading";
import {useDebouncedCallback} from "use-debounce";
import DashBoardTitleSearch from "../components/Dashboard/DashBoardTitleSearch";
import useAbortableEffect from "@closeio/use-abortable-effect";
import DatePicker from "../components/DatePicker/DatePicker";
import {useFetchAPI} from "../services/authentication"

const DashboardPage = ({token}) => {

	const [date, setDate] = useState(new Date())
	const [searchedDate, setSearchedDate] = useState(date)
	const [worksites, setWorksites] = useState({loading: false, value: []})
	const [search, setSearch] = useState("")

	const API = useFetchAPI()

	/**
	 *
	 * @type {function(string): void}
	 */
	const debouncedSearch = useDebouncedCallback(value => setSearch(value), 400)
	/**
	 *
	 * @type {function(string): void}
	 */
	const debouncedDate = useDebouncedCallback(value => setSearchedDate(value), 300)

	const changeDateDebounced = useCallback((date) => {
		setDate(date)
		debouncedDate(date)
	}, [])

	const changeDate = useCallback((date) => {
		setDate(date)
		setSearchedDate(date)
	}, [])

	/**
	 * Effect used to load worksites from a specific date
	 */
	useEffect(() => {
		if (search === "") {
			const fetchWorksites = async () => {
				setWorksites({loading: true, value: []})
				const data = await getScheduledVisits(API, token, getAPIFormattedDate(searchedDate));
				if (data instanceof Array) {
					setWorksites({loading: false, value: data})
				}
			}
			fetchWorksites().catch(console.error);
		}
	}, [API, searchedDate, search]);

	/**
	 * Effect used to search for specific worksites
	 */
	useAbortableEffect((abortSignal) => {
		if (search !== "") {
			setWorksites({loading: true, value: []})
			searchWorksites(API, token, search, abortSignal).then(data => {
				if (data instanceof Array) {
					setWorksites({loading: false, value: data})
				}
			})
		}
		return (controller) => {
			controller.abort()
		}
	}, [API, search])

	return (
		<React.Fragment>
			<Header title="Tableau de bord"/>
			<main className={"bg-gray-100 dark:bg-gray-800"}>
				<div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
					<SearchBar placeholder={"Rechercher un chantier..."} onChange={e => debouncedSearch(e.target.value)}
					           isSearched={search !== ""} onClickCancel={() => setSearch("")}/>
				</div>
				<div className="max-w-7xl mx-auto py-2 sm:px-6 lg:px-8">
					{search === "" ? <DashBoardTitleDate date={date} setDate={changeDateDebounced}
					                                     datepicker={<DatePicker selected={date}
					                                                             onChange={changeDate}/>}/> :
						<DashBoardTitleSearch search={search}/>}
				</div>
				<div className="max-w-7xl mx-auto py-2 sm:px-6 lg:px-8">
					{worksites.loading && <Loading/>}
				</div>
				<div className="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
					{!worksites.loading && worksites.value && worksites.value.map(
						worksite => <DashboardItem worksite={worksite}
						                           key={worksite?.worksite?.code.toString() || worksite.code.toString()}/>)}
				</div>
			</main>
		</React.Fragment>
	);
};

export default DashboardPage;
