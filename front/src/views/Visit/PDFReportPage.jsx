import React from 'react';
import PDFReport from "../../components/Visit/PDFReport/PDFReport";

function PDFReportPage({visit, token, setIsButtonAvailable}) {
	return (
		<div className={"my-2"}>
			<PDFReport token={token} visit={visit} setIsButtonAvailable={setIsButtonAvailable}/>
		</div>
	)
}

export default PDFReportPage;