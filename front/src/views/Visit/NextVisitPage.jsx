import React from 'react';
import NextVisitChoice from "../../components/Visit/NextVisit/NextVisitChoice";

export default function NextVisitPage({token, visit, event_id, setIsButtonAvailable}) {
	return (
		<div className={"flex flex-col items-center justify-center"}>
			{visit && <NextVisitChoice token={token} worksite={visit.worksite} event_id={event_id} setIsButtonAvailable={setIsButtonAvailable}/>}
		</div>
	);
}
