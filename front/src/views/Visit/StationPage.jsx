import React from 'react';
import Stations from "../../components/Visit/Stations/Stations";

export default function StationPage({visit, token, event_id}) {
	return (
		<div className={"flex flex-col 2xl:flex-row items-center"}>
			{visit.worksite.ssolNumber > 0 &&
			<Stations token={token} worksite={visit.worksite} event_id={event_id} type={"ssol"} label={"Sentri Sol"}/>}
			{visit.worksite.sbNumber > 0 &&
			<Stations token={token} worksite={visit.worksite} event_id={event_id} type={"sb"} label={"Sentri Box"}/>}
		</div>
	)
}
