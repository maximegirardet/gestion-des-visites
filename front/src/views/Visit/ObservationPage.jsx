import React, {useEffect, useState} from 'react'
import Observations from "../../components/Visit/Observations/Observations"
import {getPredefinedObservations} from "../../services/observations"
import {useFetchAPI} from "../../services/authentication"

export default function ObservationPage({token, event_id}) {

	/**
	 * State used to store predefined observations
	 */
	const [predefinedObservations, setPredefinedObservations] = useState(null)

	/**
	 * API function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	/**
	 * Effect used to fetch predefined observations from the API and to add default option
	 */
	useEffect(() => {
		getPredefinedObservations(API, token, "client").then(v => setPredefinedObservations([
			{
				id: 0,
				value: "Choix d'une observation prédéfinie..."
			},
			...v
		]))
	}, [])

	return (
		<div className={"flex flex-col items-center justify-center"}>
			<Observations event_id={event_id} type={"client"}
			              predefinedObservations={predefinedObservations}
			              label={"Observations diverses"}/>
			<Observations event_id={event_id} type={"intern"}
			              predefinedObservations={predefinedObservations}
			              label={"Observations internes"}/>
		</div>
	)
}
