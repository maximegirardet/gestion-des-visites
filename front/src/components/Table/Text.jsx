import React from "react";

export default function Text({children}) {
	return (
		<p className="text-gray-900 whitespace-no-wrap">
			{children}
		</p>
	)
}
