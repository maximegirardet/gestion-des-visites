import React from "react";

export default function Th({children}) {
	return (
		<th scope="col"
		    className="px-3 py-3 bg-white border-b border-gray-200 text-gray-800  text-center text-sm uppercase font-normal">
			{children}
		</th>
	)
}
