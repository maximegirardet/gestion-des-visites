import React, {useEffect, useRef} from "react"
import './Table.css'
import Th from "./Th";

export default function Table({ths, trs, className}) {

	/**
	 * Reference to an anchor div
	 * @type {React.MutableRefObject<HTMLDivElement>}
	 */
	const divAnchor = useRef()

	/**
	 * Effect used to scroll automatically into the end of the table
	 */
	useEffect(() => {
		if (divAnchor.current instanceof HTMLDivElement){
			divAnchor.current.scrollIntoView({block: 'nearest', inline: 'start' })
		}
	}, [])

	return (
		<table className={`min-w-full leading-normal table-auto table-responsive border-2 ${className}`}>
			<thead>
			<tr>
				{ths.map((label, index) => label && <Th key={index}>{label}</Th>)}
				<div ref={divAnchor}/>
			</tr>
			</thead>
			<tbody>
			{trs}
			</tbody>
		</table>
	)
}
