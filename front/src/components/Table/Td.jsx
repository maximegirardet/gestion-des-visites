import React from "react";

export default function Td({children, label, colspan, defaultLabel, className}) {
	return (
		<td className={`px-3 py-5 border-b border-gray-200 bg-white text-sm text-center ${className}`} data-label={!label && defaultLabel ? defaultLabel : label}
		    colSpan={colspan}>
			{children}
		</td>
	)
}
