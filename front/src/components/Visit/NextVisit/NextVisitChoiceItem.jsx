import React from "react";

export default function NextVisitChoiceItem({visitType, date, duration}) {
	return (
		<>
			<div className="flex items-center">
				<div className="text-sm">
					<p className="font-medium text-gray-800">
						{visitType}
					</p>
					<div className="text-gray-500">
						<p className="sm:inline">Date limite : {date}</p>
					</div>
				</div>
			</div>
			<div className="mt-2 flex justify-center items-center text-sm sm:mt-0 sm:ml-4 sm:text-right">
				<div className="font-medium text-gray-500">{duration}</div>
			</div>
		</>
	)
}
