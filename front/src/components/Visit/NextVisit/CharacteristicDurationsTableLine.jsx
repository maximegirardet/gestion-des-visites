import React from 'react'
import Td from "../../Table/Td"
import {getFormattedDuration} from "../../../utils/functions"

function CharacteristicDurationsTableLine({getFunction, label, colored = false}) {
	return (
		<tr>
			<Td label={"Type"}>
				<div className={"text-base"}>
					{label}
				</div>
			</Td>
			<Td label={"SSOL"}>
				{getFormattedDuration(getFunction("ssol_all"))}
			</Td>
			<Td label={"SB"}>
				{getFormattedDuration(getFunction("sb"))}
			</Td>
			<Td label={"CB"}>
				{getFormattedDuration(getFunction("cb"))}
			</Td>
		</tr>
	)
}

export const CharacteristicDurationsTableLineMemo = React.memo(CharacteristicDurationsTableLine)