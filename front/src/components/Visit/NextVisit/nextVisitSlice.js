import {createSlice} from "@reduxjs/toolkit"
import {getVisitTypeDuration} from "../../../services/nextVisit"


export const nextVisitSlice = createSlice({
	name: "nextVisit",
	initialState: {},
	reducers: {
		initializeNextVisit: (state, {payload: {event_id, type}}) => {
			if (!state?.[event_id]){
				return {
					...state,
					[event_id] : {
						...state?.[event_id],
						adjustments: {
							...state?.[event_id]?.adjustments,
							ssol_all: 0,
							sb: 0,
							cb: 0
						},
						choice: null
					}
				}
			}
		},
		updateAdjustments: (state, {payload: {event_id, type, value}}) => {
			state[event_id].adjustments[type] = value
		},
		updateChoice: (state, {payload: {event_id, value}}) => {
			state[event_id].choice = value
		},
		updateDuration: (state, {payload: {event_id}}) => {
			state[event_id].choice = {
				...state[event_id].choice,
				duration: getVisitTypeDuration(state[event_id].choice, state[event_id].adjustments)
			}
		},
		removeNextVisitData: (state, action) => {
			delete state[action.payload.event_id]
		},
	}
})

export const {
	initializeNextVisit,
	updateAdjustments,
	updateChoice,
	updateDuration,
	removeNextVisitData
} = nextVisitSlice.actions

export default nextVisitSlice.reducer
