import {createSlice} from "@reduxjs/toolkit";

export const pagesSlice = createSlice({
	name: "progressBar",
	initialState: {},
	reducers: {
		initializePages: (state, action) => {
			if (!state[action.payload.event_id]?.steps) {
				return {
					...state,
					[action.payload.event_id]: {
						...state[action.payload.event_id],
						steps: action.payload.value
					}
				}
			}
		},
		addStep: (state, action) => {
			return {
				...state,
				[action.payload.event_id]: {
					...state[action.payload.event_id],
					steps: [
						...state[action.payload.event_id].steps,
						action.payload.value
					]
				}
			}
		},
		modifyStep: (state, action) => {
			state[action.payload.event_id].steps[action.payload.index] = action.payload.value
		},
		deleteStep: (state, action) => {
			state[action.payload.event_id].steps = state[action.payload.event_id].steps.filter((s, index) => index !== action.payload.index)
		},
		setInProgressStep: (state, action) => {
			state[action.payload.event_id].steps = state[action.payload.event_id].steps.map((s, index) => {
				if (s.status === "progress") {
					s.status = "next"
				}
				if (action.payload.index === index) {
					s.status = "progress"
				}
				return s
			})
		},
		removePagesData: (state, action) => {
			delete state[action.payload.event_id]
		}
	}
})

export const {initializePages, addStep, modifyStep, deleteStep, setInProgressStep, removePagesData} = pagesSlice.actions

export default pagesSlice.reducer
