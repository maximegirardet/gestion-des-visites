import React, {useCallback, useEffect, useState} from 'react'
import ProgressStepBarItem from "./ProgressStepBarItem"
import IconButton from "../../../IconButton"
import Carousel from 'react-elastic-carousel'
import LeftChevron from "../../../../Icons/LeftChevron"
import RightChevron from "../../../../Icons/RightChevron"
import ProgressStepBarContextMenu from "./ProgressStepBarContextMenu"
import {findIndex} from "../../../../services/pages"
import './ProgressBar.css'

const ProgressStepBar = React.forwardRef(({
	                                          steps,
	                                          lastStep,
	                                          addStep,
	                                          deleteStep,
	                                          currentSelected,
	                                          setCurrentSelected,
	                                          className,
	                                          event_id,
	                                          worksite,
	                                          token
                                          }, carouselRef) => {

	/**
	 * Local state which stores indexes of elimination steps
	 */
	const [eliminationIndexes, setEliminationIndexes] = useState({
		begin: null,
		preEnd: null,
		end: null
	})

	/**
	 * Effect used to set elimination indexes when steps change
	 */
	useEffect(() => {
		setEliminationIndexes({
			begin: findIndex(steps, "isEliminationBegin"),
			preEnd: findIndex(steps, "isPreEliminationEnd"),
			end: findIndex(steps, "isEliminationEnd")
		})
	}, [steps])

	/**
	 * Function used to generate items from steps array
	 * @type {Function}
	 */
	const displaySteps = useCallback(() => steps.map((s, index) => {
		return <ProgressStepBarItem key={s.title} number={s.number} title={s.title}
		                            description={s.description}
		                            progressionState={s.status}
		                            isSelected={currentSelected === index}
		                            onClick={() => {
			                            setCurrentSelected(index)
		                            }}/>
	}), [steps, currentSelected, setCurrentSelected])

	return (
		<div className={`bg-white mt-2 rounded shadow ${className}`}>
			<div
				className="lg:border-t lg:border-b lg:border-gray-200 lg:flex lg:flex-row lg:justify-between items-center">
				<nav className="mx-auto flex-grow" aria-label="Progress">
					<ol className="rounded-md lg:flex lg:border-l lg:border-r lg:border-gray-200 lg:rounded-none hidden">
						<Carousel isRTL={false} itemsToShow={2} ref={carouselRef}
						          breakPoints={
							          [
								          {
									          width: 0,
									          itemsToShow: 2,
									          verticalMode: true
								          },
								          {
									          width: 550,
									          itemsToShow: 2,
								          },
								          {
									          width: 800,
									          itemsToShow: 3
								          }
							          ]
						          }
						          pagination={false}
						          renderArrow={({type, onClick, isEdge}) => {
							          return <div
								          className={`flex flex-grow items-center px-2 ${isEdge && type === "NEXT" ? "justify-center" : type === "NEXT" ? 'justify-end' : ''}`}>
								          {isEdge && type === "NEXT" ?
									          <ProgressStepBarContextMenu event_id={event_id} worksite={worksite}
									                                      token={token}
									                                      addStep={addStep} deleteStep={index => {
										          deleteStep(index)
										          if (currentSelected === index) {
											          setCurrentSelected(index - 1)
										          }
									          }} lastNumber={lastStep} eliminationBeginIndex={eliminationIndexes.begin}
									                                      preEliminationEndIndex={eliminationIndexes.preEnd}
									                                      eliminationEndIndex={eliminationIndexes.end}/> :
									          <IconButton onClick={onClick} disabled={isEdge}
									                      icon={type === "PREV" ? <LeftChevron width={15}/> :
										                      <RightChevron width={15}/>}/>}
							          </div>
						          }}>
							{displaySteps()}
						</Carousel>
					</ol>
					<ol className="rounded-md block lg:border-l lg:border-r lg:border-gray-200 lg:rounded-none lg:hidden">
						{displaySteps()}
					</ol>
				</nav>
			</div>
		</div>
	)
})

export default ProgressStepBar
