import React, {useCallback, useEffect, useMemo, useRef, useState} from "react"
import ProgressStepBar from "./ProgressStepBar/ProgressStepBar"
import {useDispatch, useSelector} from "react-redux"
import StationPage from "../../../views/Visit/StationPage"
import ObservationPage from "../../../views/Visit/ObservationPage"
import NextVisitPage from "../../../views/Visit/NextVisitPage"
import {addStep, deleteStep, initializePages, modifyStep, setInProgressStep} from "./PagesSlice"
import EliminationBegin from "../Eliminations/EliminationBegin"
import EliminationEnd from "../Eliminations/EliminationEnd"
import Button from "../../Button"
import {findLastNumber, getInitialSteps} from "../../../services/pages"
import {postReport, removeVisitData} from "../../../services/validation"
import PDFReportPage from "../../../views/Visit/PDFReportPage"
import {useFetchAPI} from "../../../services/authentication"
import {useHistory} from "react-router-dom"
import PreEliminationEnd from "../Eliminations/PreEliminationEnd"

/**
 * Correspondence between components name and functions
 * @type {Object}
 */
const correspondence = {
	StationPage,
	ObservationPage,
	NextVisitPage,
	EliminationBegin,
	EliminationEnd,
	PreEliminationEnd,
	PDFReportPage
}

export default function Page({visit, event_id, token, setVisit}) {

	/**
	 * Visit report steps
	 * @type {Array}
	 */
	const steps = useSelector(
		/**
		 * @param state {{pages: Array}}
		 */
		state => state.pages[event_id] ? state.pages[event_id].steps : [])

	/**
	 * Last number of steps
	 */
	const lastStep = useMemo(() => findLastNumber(steps), [steps])

	/**
	 * Local state which stores current selected step
	 */
	const [currentSelected, setCurrentSelected] = useState(0)

	/**
	 * Initial visit report step
	 * @type {Array}
	 */
	const initialSteps = useMemo(getInitialSteps, [])

	/**
	 * Local state which stores the components to display
	 */
	const [components, setComponents] = useState(null)

	/**
	 * Redux dispatch function
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Callback used to add a step to the list of steps
	 * @type {Function}
	 */
	const addStepCallback = useCallback(value => {
		dispatch(addStep({
			event_id: event_id,
			value: value
		}))
	}, [dispatch, event_id])

	/**
	 * Callback used to delete a step from the list of steps
	 * @type {Function}
	 */
	const deleteStepCallback = useCallback(index => {
		dispatch(deleteStep({
			event_id: event_id,
			index: index
		}))
	}, [dispatch, event_id])

	/**
	 * Callback used to modify a step in the list of steps
	 * @type {Function}
	 */
	const modifyStepCallback = useCallback((index, value) => {
		dispatch(modifyStep({
			event_id: event_id,
			index: index,
			value: value
		}))
	}, [dispatch, event_id])

	/**
	 * Effect used to initialize pages
	 */
	useEffect(() => {
		dispatch(initializePages({
			event_id: event_id,
			value: initialSteps
		}))
	}, [])

	/**
	 *
	 * Reference to carousel element, used to call goTo method
	 */
	const carousel = useRef()

	/**
	 * Callback used when a click is made on next step button
	 * @type {Function}
	 */
	const onClickNextCallback = useCallback(() => {
		setCurrentSelected(c => {
			// noinspection JSUnresolvedFunction
			carousel.current.goTo(c + 1)
			modifyStepCallback(c, {
				...steps[c],
				status: "completed"
			})
			dispatch(setInProgressStep({
				event_id,
				index: c + 1
			}))
			return c + 1
		})
	}, [setCurrentSelected, steps, carousel])

	const PDFReportStepTitle = "Rapport PDF"

	/**
	 * Callback used to add report preview step
	 * @type {Function}
	 */
	const addReportPreviewStepCallback = useCallback(id => {
		setVisit({
			...visit,
			id: id
		})
		addStepCallback({
			number: lastStep + 1,
			title: PDFReportStepTitle,
			description: "Prévisualisation du rapport",
			status: "next",
			isEliminationBegin: false,
			isEliminationEnd: false,
			components: ["PDFReportPage"]
		})
	}, [lastStep, event_id])

	/**
	 * Effect used to set components when current selected step changes
	 */
	useEffect(() => {
		setComponents(steps[currentSelected] ? steps[currentSelected].components : [])
	}, [currentSelected, steps])

	/**
	 * History object used to change location
	 * @type {History<LocationState>}
	 */
	const history = useHistory()


	/**
	 * Callback used to redirect user to the dashboard page
	 * @type {Function}
	 */
	const routeChange = useCallback(() => {
		history.push("/dashboard")
	}, [history])

	/**
	 * Local state to tell if validate button is available (not disabled)
	 */
	const [isButtonAvailable, setIsButtonAvailable] = useState(true)

	/**
	 * API function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	return (
		<div>
			<ProgressStepBar setComponents={setComponents} steps={steps} lastStep={lastStep} addStep={addStepCallback}
			                 className={"lg:sticky lg:top-0 lg:z-50 mb-4"}
			                 deleteStep={deleteStepCallback} ref={carousel} currentSelected={currentSelected}
			                 setCurrentSelected={setCurrentSelected} event_id={event_id} worksite={visit.worksite}
			                 token={token}/>
			{components && components.map(c => React.createElement(correspondence[c], {
				visit: visit,
				event_id: event_id,
				token: token,
				setIsButtonAvailable: setIsButtonAvailable,
				key: c
			}))}
			<div className={"w-full flex justify-end mt-4"}>
				{currentSelected < steps.length - 1 && steps[currentSelected].title !== PDFReportStepTitle ?
					<Button type={"primary"} onClick={onClickNextCallback}>
						Etape suivante...
					</Button> :
					steps[currentSelected]?.title !== PDFReportStepTitle ?
						<Button type={"primary"}
						        onClick={() => postReport(API, token, event_id, visit, addReportPreviewStepCallback, dispatch)}
						        disabled={!steps.every(s => s.status === "completed") && !isButtonAvailable}
						>
							Valider
						</Button> :
						<Button type={"primary"}
						        onClick={() => {
							        removeVisitData(event_id, dispatch)
							        routeChange()
						        }}
						        disabled={!isButtonAvailable}>
							Quitter le formulaire
						</Button>
				}
			</div>
		</div>
	)
}
