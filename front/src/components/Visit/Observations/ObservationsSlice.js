import {createSlice} from "@reduxjs/toolkit"
import {addToObservations} from "../../../services/observations"


export const observationsSlice = createSlice({
	name: "observations",
	initialState: {},
	reducers: {
		modifyObservation: (state, action) => {
			return {
				...state,
				[action.payload.event_id]: {
					...state[action.payload.event_id],
					[action.payload.type]: action.payload.value
				}
			}
		},
		addToObservation: (state, action) => {
			const currentValue = state[action.payload.event_id]?.[action.payload.type]
			if (!currentValue || currentValue && !currentValue.includes(action.payload.value)) {
				return {
					...state,
					[action.payload.event_id]: {
						...state[action.payload.event_id],
						[action.payload.type]: addToObservations(currentValue ?? "", action.payload.value)
					}
				}
			}
		},
		removeObservationData: (state, action) => {
			delete state[action.payload.event_id]
		}
	}
})

export const {modifyObservation, removeObservationData, addToObservation} = observationsSlice.actions

export default observationsSlice.reducer
