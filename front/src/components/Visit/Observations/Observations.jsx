import React, {useCallback, useEffect, useState} from "react"
import CustomSelect from "../../CustomSelect/CustomSelect";
import {useDispatch, useSelector} from "react-redux";
import {modifyObservation} from "./ObservationsSlice";
import {useDebouncedCallback} from "use-debounce";
import IconButton from "../../IconButton";
import Bin from "../../../Icons/Bin";
import {addToObservations} from "../../../services/observations"

export default function Observations({event_id, type, label, predefinedObservations}) {

	/**
	 * Redux dispatch function
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Current observation
	 * @type {String}
	 */
	const observations = useSelector(/**
	 * @param state {{observations: Array}}
	 */
	state => state.observations[event_id] ? state.observations[event_id][type] : "")

	/**
	 * Local state used to store textarea content
	 */
	const [localObservations, setLocalObservations] = useState(observations)

	/**
	 * Callback used to dispatch update action
	 * @type {function(string): void}
	 */
	const inputCallback = useCallback(v => {
		dispatch(modifyObservation({
			event_id: event_id,
			type: type,
			value: v
		}))
	}, [])

	/**
	 * Debounced callback used to dispatch update action
	 * @type {function(string): void}
	 */
	const debouncedInputCallback = useDebouncedCallback(inputCallback, 400)

	/**
	 * Effect used to update local state when redux state has changed
	 */
	useEffect(() => {
		if (localObservations !== observations){
			setLocalObservations((observations))
		}
	}, [observations])


	return (
		<div className={"flex flex-col w-full items-center justify-center mb-4"}>
			<div className={"container pb-4 flex flex-row justify-between"}>
				{predefinedObservations &&
				<CustomSelect options={predefinedObservations} className={"w-full"} label={label}
				              reset={localObservations === ""}
				              onChange={v => {
					              const value = addToObservations(localObservations, v.value)
					              setLocalObservations(value)
					              inputCallback(value)
				              }}
				              rightIcon={
					              <div>
						              <IconButton
							              className={"ml-2 p-2 bg-indigo-500 rounded shadow-md text-white focus:ring-offset-gray-100 focus:ring-indigo-500"}
							              icon={<Bin width={17}/>} onClick={() => {
							              setLocalObservations("")
							              inputCallback("")
						              }}
						              />
					              </div>
				              }
				/>}
			</div>
			<div className={"container"}>
				<label className="text-gray-700" htmlFor="name">
					<textarea
						className="flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 rounded-lg text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
						id="comment" placeholder={`${label}...`} name="comment" rows="5" cols="40"
						value={localObservations}
						onChange={v => {
							setLocalObservations(v.target.value)
							debouncedInputCallback(v.target.value)
						}}>
                    </textarea>
				</label>
			</div>
		</div>
	)
}
