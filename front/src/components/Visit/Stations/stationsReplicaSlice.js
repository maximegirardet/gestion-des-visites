import {createSlice} from "@reduxjs/toolkit"
import {initializeReplicaStations} from "../../../services/stations"


export const stationsReplicaSlice = createSlice({
	name: "stationsReplica",
	initialState: {},
	reducers: {
		initializeReplicaTable: (state, {payload: {event_id, type, stations}}) => {
			if (!state[event_id]?.[type]) {
				return initializeReplicaStations(stations, type, event_id, state)
			}
		},
		addReplica: (state, {payload: {event_id, type, value, indexName}}) => {
			state[event_id][type] = {
				...state[event_id][type],
				[value.station]: [
					...(state[event_id][type][value.station] ?? []),
					indexName
				]
			}
		},
		removeReplica: (state, {payload: {event_id, type, station, indexName}}) => {
			const replicas = state[event_id][type][station]
			state[event_id][type][station] = replicas.filter(v => v !== indexName)
		},
		resetReplicaTable: (state, {payload: {event_id, type, stations}}) => {
			return initializeReplicaStations(stations, type, event_id, state)
		},
		removeStationsReplicaData: (state, action) => {
			delete state[action.payload.event_id]
		},
	}
})

export const {
	addReplica,
	removeReplica,
	initializeReplicaTable,
	resetReplicaTable,
	removeStationsReplicaData
} = stationsReplicaSlice.actions

export default stationsReplicaSlice.reducer
