import React from "react";
import ContextMenu from "../../../ContextMenu/ContextMenu";
import Burger from "../../../../Icons/Burger";
import ContextMenuItemWithIcon from "../../../ContextMenu/ContextMenuItemWithIcon";
import Previous from "../../../../Icons/Previous";
import {EyeIcon, EyeOffIcon} from "@heroicons/react/solid";

export default function StationInputBurger({setIsHistoryVisible, seeAll, setSeeAll}) {
	return (
		<div className={"flex justify-center align-middle"}>
			<ContextMenu openingButton={<div className={"text-white p-2 bg-gray-500 rounded shadow-md"}>
				<Burger width={15}/>
			</div>}
			             buttonClassName={"focus:ring-gray-500"}>
				<ContextMenuItemWithIcon icon={<Previous width={20}/>} onClick={() => setIsHistoryVisible(false)}
				                         className={"text-gray-600"}>
					<div className={"font-semibold text-sm normal-case"}>Revenir à la saisie</div>
				</ContextMenuItemWithIcon>
				{!seeAll ? <ContextMenuItemWithIcon icon={<EyeIcon width={20}/>} onClick={() => setSeeAll(true)}
				                                    className={"text-blue-600"}>
						<div className={"font-semibold text-sm normal-case"}>Voir tout...</div>
					</ContextMenuItemWithIcon> :
					<ContextMenuItemWithIcon icon={<EyeOffIcon width={20}/>} onClick={() => setSeeAll(false)}
					                         className={"text-blue-600"}>
						<div className={"font-semibold text-sm normal-case"}>Restreindre la vue...</div>
					</ContextMenuItemWithIcon>
				}
			</ContextMenu>
		</div>
	)
}
