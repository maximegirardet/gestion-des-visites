import React from "react";
import Th from "../../../Table/Th";
import Td from "../../../Table/Td";

function ConsumptionHistoryLine({number, history, getStatus}) {

	return (
		<tr>
			<Th>
				<div className={"normal-case"}>
					{number}
				</div>
			</Th>
			{history.map((status, index) => status ? <Td key={index}>
				<div className={"flex flex-col justify-center items-center"}>
					<div className={"mb-2"}>
						{getStatus(status)}
					</div>
					<div className={"text-gray-800"}>{status.consumption} %</div>
				</div>
			</Td> : <Td key={index}>-</Td>)}
			<Td/>
		</tr>
	)
}

export const ConsumptionHistoryLineMemo = React.memo(ConsumptionHistoryLine)
