import React, {useEffect, useMemo, useState} from "react";
import StationInputBurger from "./ConsumptionHistoryBurger";
import Table from "../../../Table/Table";

import './ConsumptionHistory.css'
import {ConsumptionHistoryLineMemo} from "./ConsumptionHistoryLine";
import {getConsumptionHistory} from "../../../../services/stations";
import ConsumptionHistoryLabelItem from "./ConsumptionHistoryLabelItem";
import {useFetchAPI} from "../../../../services/authentication"


export default function ConsumptionHistory({token, worksite, type, setIsHistoryVisible, getStatus}) {

	const [visits, setVisits] = useState([])
	const [consumption, setConsumption] = useState([])
	const [isLoading, setIsLoading] = useState(false)
	const [seeAll, setSeeAll] = useState(false)

	/**
	 * API function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	/**
	 * Effect used to load consumption history from the API
	 */
	useEffect(() => {
		setIsLoading(true)
		getConsumptionHistory(API, token, worksite.id).then(/** @param response {{visits: Array, stations: Array}}*/response => {
			setVisits(response.visits)
			setConsumption(response.stations)
			setIsLoading(false)
		})
	}, [API])

	/**
	 * Labels of the table
	 * @type {Array}
	 */
	const visitsLabel = useMemo(() => [
		"N°",
		...visits.map(v => <ConsumptionHistoryLabelItem visit={v}/>),
		<StationInputBurger setIsHistoryVisible={setIsHistoryVisible} setSeeAll={setSeeAll} seeAll={seeAll}/>
	], [visits, setIsHistoryVisible, setSeeAll, seeAll])


	/**
	 * Lines of the table
	 * @type {Array}
	 */
	const lines = consumption.map(c => {
		const invisible = c.history.every(status => status === false)
		const number = `${c.number}${c.indexName ? ` ${c.indexName}` : ''}`
		return (!invisible || seeAll) && type === c.type.short_name &&
			<ConsumptionHistoryLineMemo key={number} number={number} history={c.history} getStatus={getStatus}/>
	})

	return (
		!isLoading && <Table className={"rounded mb-4"}
		                     ths={visitsLabel}
		                     trs={lines}
		/>
	)
}

