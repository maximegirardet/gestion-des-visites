import React from "react";
import ArrayTextBadge from "../../../ArrayTextBadge";
import {getFormattedDate} from "../../../../utils/functions";

export default function ConsumptionHistoryLabelItem({visit}) {
	return (
		<div className={"normal-case flex flex-col"}>
			{visit.startingEliminationPeriod &&
			<div className={"mb-2"}>
				<ArrayTextBadge color={"indigo"}>Début d'élimination</ArrayTextBadge>
			</div>}
			{visit.endingEliminationPeriod &&
			<div className={"mb-2"}>
				<ArrayTextBadge color={"indigo"}>Fin d'élimination</ArrayTextBadge>
			</div>}
			<div>
				{getFormattedDate(visit.scheduledAt)}
			</div>
		</div>
	)
}
