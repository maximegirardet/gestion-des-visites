import React, {useCallback, useMemo} from "react"
import ContextMenuItemWithIcon from "../../../ContextMenu/ContextMenuItemWithIcon"
import {getRestrictedAttributes, getStationValue} from "../../../../services/stations"

export default function StationLineContextMenuItem({
	                                                   icon,
	                                                   event_id,
	                                                   type,
	                                                   index,
	                                                   className,
	                                                   station,
	                                                   attributes,
	                                                   attribute,
	                                                   optionalAttribute,
	                                                   excludedAttributes,
	                                                   label,
	                                                   cancelLabel,
	                                                   unitAction,
	                                                   updateStationLine,
	                                                   disabledCondition,
	                                                   setIsPopupVisible
                                                   }) {

	/**
	 * Station status attributes which are not equal to this line's attribute
	 */
	const restrictedAttributes = useMemo(() => {
		return getRestrictedAttributes(attributes, attribute, excludedAttributes)
	}, [attributes, attribute, excludedAttributes])

	/**
	 * Function that gets the condition which disables the context menu item.
	 * Returns true (=== disabled the item) if at least one of restrictedAttributes are true for the specific station AND optional attribute does not exist or is false for the station
	 * @type {Function}
	 */
	const getCondition = useCallback(station => {
		return restrictedAttributes.reduce((prev, curr) => {
			return prev || station[curr]
		}, false) && (!optionalAttribute || !station[optionalAttribute])
	}, [restrictedAttributes])


	return (
		<ContextMenuItemWithIcon icon={icon}
		                         disabled={disabledCondition ?? getCondition(station)}
		                         onClick={() => {
			                         if (setIsPopupVisible && !station[attribute]) {
				                         setIsPopupVisible(true)
			                         } else {
				                         updateStationLine({
					                         event_id: event_id,
					                         type: type,
					                         index: index,
					                         value: getStationValue(restrictedAttributes, station, [attribute], cancelLabel === null),
					                         unitAction: unitAction
				                         })
			                         }
		                         }}
		                         className={className}>
			<div className={"font-semibold"}>{station[attribute] ? cancelLabel ?? label : label}</div>
		</ContextMenuItemWithIcon>
	)
}
