import React, {useCallback} from "react"
import ContextMenu from "../../../ContextMenu/ContextMenu"
import Burger from "../../../../Icons/Burger"
import ContextMenuItemWithIcon from "../../../ContextMenu/ContextMenuItemWithIcon"
import Plus from "../../../../Icons/Plus"
import Previous from "../../../../Icons/Previous"
import {ArchiveIcon} from "@heroicons/react/solid"
import {addStationLine} from "../stationsSlice"
import {getStationID} from "../../../../services/stations"
import {useDispatch} from "react-redux"

export default function StationInputBurger({
	                                           event_id,
	                                           type,
	                                           worksite,
	                                           addNewStation,
	                                           suspendedStations,
	                                           resetTable,
	                                           setIsHistoryVisible,
	                                           setPopupsOnClick,
	                                           setIsPopupVisible
                                           }) {

	/**
	 * Dispatch function (redux)
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Callback used to process validation on station add popup
	 * @type {Function}
	 */
	const onClickValidationStationAddPopup = useCallback((radioChoice, chosenStation) => {
		if (radioChoice === "install") {
			addNewStation()
		} else if (radioChoice === "reinstall") {
			dispatch(addStationLine({
				event_id: event_id,
				type: type,
				number: parseInt(chosenStation),
				initialValues: {
					station: `/stations/${getStationID(worksite, type, chosenStation)}`,
					isReinstalled: true
				}
			}))
		}
	}, [addNewStation, event_id, type, worksite])

	/**
	 * Callback used when a click is made on add station button (in burger)
	 * @type {Function}
	 */
	const addNewStationCallback = useCallback(() => {
		if (suspendedStations.length > 0) {
			setPopupsOnClick(value => ({
				...value,
				addStation: [onClickValidationStationAddPopup]
			}))
			setIsPopupVisible(value => ({
				...value,
				addStation: true
			}))
		} else {
			addNewStation()
		}
	}, [suspendedStations, setPopupsOnClick, onClickValidationStationAddPopup, setIsPopupVisible])

	return (
		<div className={"flex justify-center align-middle"}>
			<ContextMenu openingButton={<div className={"text-white p-2 bg-gray-500 rounded shadow-md"}>
				<Burger width={15}/>
			</div>}
			             buttonClassName={"focus:ring-gray-500"}>
				<ContextMenuItemWithIcon icon={<Plus width={20}/>} onClick={addNewStationCallback}
				                         className={"text-green-600"}>
					<div className={"font-semibold text-sm normal-case"}>Installer une station</div>
				</ContextMenuItemWithIcon>
				<ContextMenuItemWithIcon icon={<ArchiveIcon width={20}/>} onClick={() => setIsHistoryVisible(true)}
				                         className={"text-blue-600"}>
					<div className={"font-semibold text-sm normal-case"}>Historique des consommations</div>
				</ContextMenuItemWithIcon>
				<ContextMenuItemWithIcon icon={<Previous width={20}/>} onClick={resetTable} className={"text-gray-600"}>
					<div className={"font-semibold text-sm normal-case"}>Réinitialiser la saisie</div>
				</ContextMenuItemWithIcon>
			</ContextMenu>
		</div>
	)

}
