import React, {useCallback} from 'react'
import Bin from "../../../../../Icons/Bin"
import StationLineContextMenuItem from "../StationLineContextMenuItem"
import {getRestrictedAttributes, getStationValue} from "../../../../../services/stations"

export default function RemoveStationContextMenuItem({
	                                                     station,
	                                                     index,
	                                                     type,
	                                                     event_id,
	                                                     allAttributes,
	                                                     excludedAttributes,
	                                                     updateStationLine,
	                                                     setIsPopupVisible,
	                                                     setPopupsOnClick
                                                     }) {

	/**
	 * Callback used when a click is made on temporary station removal
	 * @type {Function}
	 */
	const onClickTemporary = useCallback(() => {
		updateStationLine({
			event_id: event_id,
			type: type,
			index: index,
			value: getStationValue(getRestrictedAttributes(allAttributes, "isSuspended", excludedAttributes), station, ["isSuspended", "isRemoved"])
		})
	}, [updateStationLine, event_id, type, index, allAttributes, excludedAttributes, station])

	/**
	 * Callback used when a click is made on definitive station removal
	 * @type {Function}
	 */
	const onClickDefinitive = useCallback(() => {
		updateStationLine({
			event_id: event_id,
			type: type,
			index: index,
			value: getStationValue(getRestrictedAttributes(allAttributes, "isSuspended", excludedAttributes), station, ["isRemoved"])
		})
	}, [updateStationLine, event_id, type, index, allAttributes, excludedAttributes, station])


	return (
		<StationLineContextMenuItem station={station} index={index} className={"text-red-600"} type={type}
		                            event_id={event_id} icon={<Bin width={20}/>} attribute={"isRemoved"}
		                            label={"Démonter"} cancelLabel={'Annuler le démontage'}
		                            attributes={allAttributes}
		                            updateStationLine={updateStationLine}
		                            setIsPopupVisible={value => {
			                            setPopupsOnClick(v => ({
				                            ...v,
				                            removeStation: [onClickTemporary, onClickDefinitive]
			                            }))
			                            setIsPopupVisible(v => ({...v, removeStation: value}))
		                            }}
		                            optionalAttribute={"isSuspended"}/>
	)
}
