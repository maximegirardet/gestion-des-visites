import React, {useEffect, useState} from 'react'
import StationPopup from "./StationPopup"
import Select from "../../../../Select"

export default function AddStationPopup({
	                                   isPopupVisible,
	                                   setIsPopupVisible,
	                                   popupsOnClick,
	                                   suspendedStations
                                   }) {


	/**
	 * Local state which stores the chosen station number
	 */
	const [chosenStation, setChosenStation] = useState(null)

	/**
	 * Local state which stores the choice between the two options
	 */
	const [radioChoice, setRadioChoice] = useState("")

	/**
	 * Effect used to set the chosen station to the first suspended station, when suspendedStations changes
	 */
	useEffect(() => {
		setChosenStation(suspendedStations?.[0]?.number ?? null)
	}, [suspendedStations])

	const name = "addStation"

	return (
		<StationPopup name={name} popupOpen={isPopupVisible} setPopupOpen={setIsPopupVisible}
		              title={"Type d'installation"} buttons={[
			{
				label: "Valider",
				className: "bg-green-600 hover:bg-green-500 focus:ring-green-500",
				onClick: () => popupsOnClick[name][0](radioChoice, chosenStation)
			}]}>
			<div className="mt-4">
				<div>
					<label className="inline-flex items-center">
						<input type="radio" className="form-radio" name="radio"
						       value="reinstall" onChange={v => setRadioChoice(v.target.value)}
						       checked={radioChoice === "reinstall"}/>
						<span className="ml-2">Réinstaller une station démontée temporairement</span>
					</label>
					{radioChoice === "reinstall" &&
					<Select className={"ml-4 my-4"} onChange={e => setChosenStation(e.target.value)}
					        options={suspendedStations.map(s => (
						        {
							        value: s.number,
							        text: s.number
						        }))}/>}
				</div>
				<div>
					<label className="inline-flex items-center">
						<input type="radio" className="form-radio" name="radio"
						       value="install" onChange={v => setRadioChoice(v.target.value)}
						       checked={radioChoice === "install"}/>
						<span className="ml-2">Installer une nouvelle station</span>
					</label>
				</div>
			</div>
		</StationPopup>
	)
}
