import React from 'react'
import StationPopup from "./StationPopup"

export default function RemoveStationPopup({
	                                           isPopupVisible,
	                                           setIsPopupVisible,
	                                           popupsOnClick
                                           }) {
	const name = "removeStation"

	return (
		<StationPopup name={name} popupOpen={isPopupVisible} setPopupOpen={setIsPopupVisible}
		              title={"Type de démontage"} buttons={[
			{
				label: "Temporaire",
				className: "bg-yellow-600 hover:bg-yellow-500 focus:ring-yellow-500",
				onClick: popupsOnClick[name][0]
			},
			{
				label: "Définitif",
				className: "bg-red-600 hover:bg-red-500 focus:ring-red-500",
				onClick: popupsOnClick[name][1]
			}
		]}>
			<p className="text-base text-gray-500">
				Merci de choisir le type de démontage de cette station
			</p>
		</StationPopup>
	)
}
