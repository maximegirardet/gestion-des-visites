import React from "react";
import Td from "../../../Table/Td";
import IconButton from "../../../IconButton";
import Plus from "../../../../Icons/Plus";

export default function StationLineAddButton({addLine, disabled}) {
	return (
		<tr key={"plus"}>
			<Td colspan={"7"}>
				<IconButton icon={<Plus/>} onClick={addLine} disabled={disabled}
				            className={"text-white p-2 bg-blue-500 rounded shadow-md focus:ring-offset-gray-100 focus:ring-blue-500"}/>
			</Td>
		</tr>
	)
}
