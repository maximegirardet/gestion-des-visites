import React, {useCallback, useState} from "react"
import StationsInput from "./StationInput/StationsInput"
import ConsumptionHistory from "./ConsumptionHistory/ConsumptionHistory"
import ArrayTextBadge from "../../ArrayTextBadge"
import {ClockIcon} from "@heroicons/react/outline"

export default function Stations({token, worksite, event_id, type, label}) {

	const attributes = [
		"isRemoved", "isRecharged", "isReplaced", "isNew", "isNotAccessible", "isLidChanged", "isReinstalled"
	]

	/**
	 * Function which returns the status of the station
	 * @type {Function}
	 */
	const getStatus = useCallback(station => {
			if (attributes.every(a => !station[a])) {
				if (station.isConnected) {
					return <ArrayTextBadge color={"gray"}>Connectée</ArrayTextBadge>
				}
				return <span>-</span>
			}
			return <div className={"flex flex-col justify-center gap-y-2"}>
				{station.isRemoved ? <ArrayTextBadge color={station.isSuspended ? "yellow" : "red"} icon={station.isSuspended ?
					<ClockIcon width={25}/> : null}>Démontée</ArrayTextBadge> : null}
				{station.isRecharged ? <ArrayTextBadge color={"green"}>Rechargée</ArrayTextBadge> : null}
				{station.isReinstalled ? <ArrayTextBadge color={"blue"}>Réinstallée</ArrayTextBadge> : null}
				{station.isReplaced ? <ArrayTextBadge color={"blue"}>Remplacée</ArrayTextBadge> : null}
				{station.isNew ? <ArrayTextBadge color={"blue"}>Installée</ArrayTextBadge> : null}
				{station.isNotAccessible ? <ArrayTextBadge color={"yellow"}>Non accessible</ArrayTextBadge> : null}
				{station.isLidChanged ? <ArrayTextBadge color={"purple"}>Couvercle changé</ArrayTextBadge> : null}
			</div>
		}
		, [])

	const [isHistoryVisible, setIsHistoryVisible] = useState(false)

	return (
		<div className="container sm:px-8">
			<h1 className={"flex text-xl mb-2 text-indigo-600 font-semibold tracking-wide uppercase justify-center"}>{label}</h1>
			<div className={`-mx-4 sm:-mx-8 px-0 mb-2 ${!isHistoryVisible ? 'overflow-x-visible' : 'overflow-x-auto'}`}>
				<div className={`inline-block min-w-full rounded-lg`}>
					{isHistoryVisible ? <ConsumptionHistory token={token} worksite={worksite}
					                                        setIsHistoryVisible={setIsHistoryVisible}
					                                        getStatus={getStatus}
					                                        type={type}/> :
						<StationsInput worksite={worksite} event_id={event_id} type={type}
						               setIsHistoryVisible={setIsHistoryVisible} getStatus={getStatus}/>}
				</div>
			</div>
		</div>
	)
}
