import {createSlice} from "@reduxjs/toolkit"


export const eliminationsSlice = createSlice({
	name: "eliminations",
	initialState: {},
	reducers: {
		addElimination: (state, action) => {
			const old = state[action.payload.event_id]
			state[action.payload.event_id] = {
				...old,
				[action.payload.type]: action.payload.value
			}
		},
		removeEliminationData: (state, action) => {
			delete state[action.payload.event_id]
		},
		removeEliminationDataType: (state, action) => {
			if (state[action.payload.event_id][action.payload.type]) {
				delete state[action.payload.event_id][action.payload.type]
			}
		}
	}
})

export const {addElimination, removeEliminationData, removeEliminationDataType} = eliminationsSlice.actions

export default eliminationsSlice.reducer
