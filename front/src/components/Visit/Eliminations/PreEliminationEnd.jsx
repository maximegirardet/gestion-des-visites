import React, {useCallback, useEffect, useMemo} from "react"
import {useDispatch, useSelector} from "react-redux"
import SpecialStepPage from "../Pages/SpecialStepPage"
import SB from "../../../Icons/SB"
import EliminationEndTable from "./EliminationEndTable"
import {addElimination} from "./EliminationsSlice"
import {getRemovedStations} from "../../../services/stations"


export default function PreEliminationEnd({event_id, visit}) {

	const type = "preEliminationEnd"

	/**
	 * preEliminationEnd object
	 */
	const preEliminationEnd = useSelector(/**
	 * @param state {{eliminations: Array}}
	 */
	state => state.eliminations[event_id]?.[type] ? state.eliminations[event_id][type] : null)

	/**
	 * Dispatch function (redux)
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * Array of stations statuses
	 * @type {Object}
	 */
	const stations = useSelector(/**
	 * @param state {{stations: Array}}
	 */
	state => state.stations[event_id] ?? {})

	/**
	 * Callback used to initialize tha table with data (from API or local storage)
	 * @type {Function}
	 */
	const initializeTable = useCallback((stationsOnly = false) => {
		const stationNumber = {
			computedSSOL: visit.worksite.ssolNumber,
			computedSB: stations?.["sb"] ? visit.worksite.sbNumber - getRemovedStations(stations["sb"]) : 0
		}
		if (stationsOnly) {
			dispatch(addElimination({
				event_id: event_id,
				type: type,
				value: {
					...preEliminationEnd,
					...stationNumber
				}
			}))
		} else {
			dispatch(addElimination({
				event_id: event_id,
				type: type,
				value: {
					stateChoice: null,
					...stationNumber
				}
			}))
		}
	}, [visit, event_id, type, stations])

	/**
	 * Callback used to change a value from the elimination table
	 * @type {Function}
	 */
	const onChangeCallback = useCallback((attribute, value) => {
		dispatch(addElimination({
			event_id: event_id,
			type: type,
			value: {
				...preEliminationEnd,
				[attribute]: value
			}
		}))
	}, [preEliminationEnd])

	const preEliminationItems = useMemo(() => {
		if (preEliminationEnd) {
			return [
				{
					name: 'Statut des stations',
					description: <div>
						<div>
							<label className="inline-flex items-center">
								<input type="radio" className="form-radio" name="radio"
								       value="sb_ssol" onChange={v => onChangeCallback("stateChoice", v.target.value)}
								       checked={preEliminationEnd.stateChoice === "sb_ssol"}/>
								<span className="ml-2">Plus de consommation dans les stations SB et SSOL</span>
							</label>
						</div>
						<div>
							<label className="inline-flex items-center">
								<input type="radio" className="form-radio" name="radio"
								       value="sb" onChange={v => onChangeCallback("stateChoice", v.target.value)}
								       checked={preEliminationEnd.stateChoice === "sb"}/>
								<span className="ml-2">Plus de consommation dans les stations SB mais toujours dans une ou plusieurs SSOL</span>
							</label>
						</div>
					</div>,
					icon: SB,
				},
			]
		}
		return []

	}, [preEliminationEnd, visit])

	useEffect(() => {
		if (!preEliminationEnd) {
			initializeTable()
		}
	}, [])

	/**
	 * Effect used to update station number in table when stations changes
	 */
	useEffect(() => {
		initializeTable(true)
	}, [stations])

	return (
		preEliminationEnd &&
		<SpecialStepPage items={preEliminationItems} title={"pré-élimination"} onReset={() => initializeTable(false)}>
			<EliminationEndTable initialSSOL={visit.worksite.ssolNumber}
			                     initialSB={visit.worksite.sbNumber}
			                     computedSB={preEliminationEnd.computedSB}
			                     computedSSOL={preEliminationEnd.computedSSOL}
			                     onChange={onChangeCallback}/>
		</SpecialStepPage>
	)
}
