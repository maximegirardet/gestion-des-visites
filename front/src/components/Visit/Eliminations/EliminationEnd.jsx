import React, {useCallback, useEffect, useMemo} from "react"
import DatePicker from "../../DatePicker/DatePicker"
import SB from "../../../Icons/SB"
import {BanIcon, ClipboardCheckIcon} from "@heroicons/react/outline"
import EliminationEndTable from "./EliminationEndTable"
import {useDispatch, useSelector} from "react-redux"
import {addElimination} from "./EliminationsSlice"
import {getEliminationEnd} from "../../../services/eliminations"
import {useFetchAPI} from "../../../services/authentication"
import SpecialStepPage from "../Pages/SpecialStepPage"
import {getRemovedStations} from "../../../services/stations"


export default function EliminationEnd({token, event_id, visit}) {

	const type = "eliminationEnd"

	/**
	 * @param eliminationEnd {{
	 *     consumptionBegin: string,
	 *     consumptionEnd: string,
	 *     inactivityBegin: string,
	 *     inactivityEnd: string
	 * }}
	 */
	const eliminationEnd = useSelector(/**
	 * @param state {{eliminations: Array}}
	 */
	state => state.eliminations[event_id]?.[type] ? state.eliminations[event_id][type] : null)

	/**
	 * Array of stations statuses
	 * @type {Object}
	 */
	const stations = useSelector(/**
	 * @param state {{stations: Array}}
	 */
	state => state.stations[event_id] ?? {})

	/**
	 * Redux dispatch function
	 * @type {Dispatch<any>}
	 */
	const dispatch = useDispatch()

	/**
	 * API function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	/**
	 * Callback used to initialize the table with data (from API or local storage)
	 * @type {Function}
	 */
	const initializeTable = useCallback((stationsOnly = false) => {
		const stationNumber = {
			computedSSOL: visit.worksite.ssolNumber,
			computedSB: stations?.["sb"] ? visit.worksite.sbNumber - getRemovedStations(stations["sb"]) : 0
		}
		if (stationsOnly) {
			dispatch(addElimination({
				event_id: event_id,
				type: type,
				value: {
					...eliminationEnd,
					...stationNumber
				}
			}))
		} else {
			getEliminationEnd(API, token, visit.worksite.id).then(r => {
				dispatch(addElimination({
					event_id: event_id,
					type: type,
					value: {
						inactivityBegin: null,
						consumptionBegin: null,
						consumptionEnd: null,
						...r,
						installationDate: new Date(visit.worksite.installation_date).toISOString(),
						inactivityEnd: new Date().toISOString(),
						date: new Date().toISOString(),
						...stationNumber
					}
				}))
			})
		}
	}, [API, token, visit, event_id, type, stations])

	useEffect(() => {
		if (!eliminationEnd) {
			initializeTable()
		}
	}, [])

	/**
	 * Effect used to update station number in table when stations changes
	 */
	useEffect(() => {
		if (eliminationEnd){
			initializeTable(true)
		}
	}, [stations])

	/**
	 * Callback used to change a value from the elimination table
	 * @type {Function}
	 */
	const onChangeCallback = useCallback((attribute, value) => {
		dispatch(addElimination({
			event_id: event_id,
			type: type,
			value: {
				...eliminationEnd,
				[attribute]: value
			}
		}))
	}, [eliminationEnd])

	const eliminationItems = useMemo(() => {
		if (eliminationEnd) {
			return [
				{
					name: 'Consommation',
					description: <div className={""}>
						Consommation de Recrute HD ou Recrute PRO observée du <DatePicker
						selected={eliminationEnd.consumptionBegin ? new Date(eliminationEnd.consumptionBegin) : ''}
						onChange={v => onChangeCallback("consumptionBegin", v.toISOString())}
						inputClassName={"basic-calendar-input"}/> au <DatePicker
						selected={eliminationEnd.consumptionEnd ? new Date(eliminationEnd.consumptionEnd) : ''}
						onChange={v => onChangeCallback("consumptionEnd", v.toISOString())}
						inputClassName={"basic-calendar-input"}/>.
					</div>,
					icon: SB,
				},
				{
					name: 'Inactivité',
					description: <div>
						Inactivité observée du <DatePicker
						selected={eliminationEnd.inactivityBegin ? new Date(eliminationEnd.inactivityBegin) : ''}
						onChange={v => onChangeCallback("inactivityBegin", v.toISOString())}
						inputClassName={"basic-calendar-input"}/> au <DatePicker
						selected={new Date(eliminationEnd.inactivityEnd)}
						onChange={v => onChangeCallback("inactivityEnd", v.toISOString())}
						inputClassName={"basic-calendar-input"}/>.
					</div>,
					icon: BanIcon,
				},
				{
					name: 'Inspection',
					description: <div>
						Pas d'activité dans la zone à protéger lors de l'inspection de constat d'élimination
						effectuée le <DatePicker selected={new Date(eliminationEnd.date)}
						                         onChange={v => onChangeCallback("date", v.toISOString())}
						                         inputClassName={"basic-calendar-input"}/> dans les mêmes
						conditions que l'inspection de début de chantier effectuée le <DatePicker
						selected={new Date(eliminationEnd.installationDate)}
						onChange={v => onChangeCallback("installationDate", v.toISOString())}
						inputClassName={"basic-calendar-input"}/>.
					</div>,
					icon: ClipboardCheckIcon,
					className: "col-span-2"
				}
			]
		}
		return []
	}, [eliminationEnd, visit])

	return (
		<SpecialStepPage title={"élimination"} items={eliminationItems} onReset={() => initializeTable(false)}>
			{eliminationEnd && <EliminationEndTable initialSSOL={visit.worksite.ssolNumber}
			                                        initialSB={visit.worksite.sbNumber}
			                                        computedSB={eliminationEnd.computedSB}
			                                        computedSSOL={eliminationEnd.computedSSOL}
			                                        onChange={onChangeCallback}/>}
		</SpecialStepPage>
	)
}
