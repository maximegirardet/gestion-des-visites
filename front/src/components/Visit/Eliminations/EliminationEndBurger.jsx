import React from "react";
import ContextMenu from "../../ContextMenu/ContextMenu";
import Burger from "../../../Icons/Burger";
import ContextMenuItemWithIcon from "../../ContextMenu/ContextMenuItemWithIcon";
import Previous from "../../../Icons/Previous";

export default function EliminationEndBurger({resetTable}) {
	return (
		<div className={"flex justify-center align-middle"}>
			<ContextMenu openingButton={<div className={"text-white p-2 bg-gray-500 rounded shadow-md"}>
				<Burger width={15}/>
			</div>}
			             buttonClassName={"focus:ring-gray-500"}>
				<ContextMenuItemWithIcon icon={<Previous width={20}/>} onClick={resetTable} className={"text-gray-600"}>
					<div className={"font-semibold text-sm normal-case"}>Réinitialiser la saisie</div>
				</ContextMenuItemWithIcon>
			</ContextMenu>
		</div>
	)
}
