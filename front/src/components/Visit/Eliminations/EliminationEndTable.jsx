import React from "react";
import Td from "../../Table/Td";
import Table from "../../Table/Table";

export default function EliminationEndTable({initialSSOL, computedSSOL, initialSB, computedSB, onChange}) {
	return (
		<Table className={"text-center text-gray-700"} trs={[<tr key={"ext"}>
			<Td label={"Emplacement"}>
				<div className={"text-base"}>
					Extérieur
				</div>
			</Td>
			<Td label={"Lors de la visite"}>
				{initialSSOL}
			</Td>
			<Td label={"Après le constat"} className={"flex justify-center"}>
				<input
					type="text"
					className="focus:ring-indigo-500 focus:border-indigo-500 block sm:text-sm border-gray-300 rounded-md w-14 text-center"
					value={computedSSOL}
					onChange={v => onChange("computedSSOL", v.target.value)}
				/>
			</Td>
		</tr>, <tr key={"int"}>
			<Td label={"Emplacement"}>
				<div className={"text-base"}>
					Intérieur
				</div>
			</Td>
			<Td label={"Lors de la visite"}>{initialSB}</Td>
			<Td label={"Après le constat"} className={"flex justify-center"}>
				<input
					type="text"
					className="focus:ring-indigo-500 focus:border-indigo-500 block sm:text-sm border-gray-300 rounded-md w-14 text-center"
					value={computedSB}
					onChange={v => onChange("computedSB", v.target.value)}
				/>
			</Td>
		</tr>]} ths={[" ", "Lors de la visite", "Après le constat"]}/>
	)
}
