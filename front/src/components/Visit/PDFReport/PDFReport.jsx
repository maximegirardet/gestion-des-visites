import React, {useEffect, useState} from 'react';
import {base64ToBlob, getPDFReport} from "../../../services/validation";
import Spinner from "../../Spinner";
import {useFetchAPI} from "../../../services/authentication"

export default function PDFReport({visit, token, setIsButtonAvailable}) {

	/**
	 * Local state which stores pdfURL
	 */
	const [pdfURL, setPDFURL] = useState(null)

	/**
	 * API function
	 * @type {Function}
	 */
	const API = useFetchAPI()

	/**
	 * Effect used to load new PDF when the component mounts
	 */
	useEffect(async () => {
		setIsButtonAvailable(false)
		const {pdf: base64} = await getPDFReport(API, token, visit.id)
		const blob = base64ToBlob(base64, 'application/pdf')
		setPDFURL(URL.createObjectURL(blob))
		setIsButtonAvailable(true)
	}, [API])

	return (
		pdfURL ? <iframe width="100%" src={pdfURL} className={"pdf-report"}/> :
			<div className={"flex justify-center"}>
				<Spinner className={"h-16 w-16 text-red-600"}/>
			</div>

	)
}
