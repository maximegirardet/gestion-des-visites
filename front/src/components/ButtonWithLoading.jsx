import React from "react";
import Spinner from "./Spinner";

const ButtonWithLoading = (props) => {
	return (
		<button type={props.type} className="w-full block bg-indigo-500 hover:bg-indigo-400 focus:bg-indigo-400 text-white font-semibold rounded-lg
              px-4 py-3 mt-6 inline-flex justify-center items-center" disabled={props.loading}>
			{props.loading && <Spinner className={"h-5 w-5"}/>}
			{props.children}
		</button>
	)
}

export default ButtonWithLoading
