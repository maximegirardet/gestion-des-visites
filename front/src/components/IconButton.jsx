import React from "react";

export default function IconButton({onClick, className, icon, disabled}) {
	return (
		<button
			className={`text-right flex justify-end h-full items-center focus:outline-none ${className} disabled:opacity-50 disabled:cursor-not-allowed focus:ring-2 focus:ring-offset-2`}
			onClick={onClick} disabled={disabled}>
			{icon}
		</button>
	)
}
