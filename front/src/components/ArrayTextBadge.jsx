import React from "react"

function getClasses(color) {
	switch (color) {
		case "green":
			return {
				text: "text-green-900",
				bg: "bg-green-200"
			}
		case "yellow":
			return {
				text: "text-yellow-900",
				bg: "bg-yellow-200"
			}
		case "red":
			return {
				text: "text-red-900",
				bg: "bg-red-200"
			}
		case "gray":
			return {
				text: "text-gray-700",
				bg: "bg-gray-300"
			}
		case "indigo":
			return {
				text: "text-indigo-900",
				bg: "bg-indigo-200"
			}
		case "purple":
			return {
				text: "text-purple-900",
				bg: "bg-purple-200"
			}
		default:
			return {
				text: "text-blue-900",
				bg: "bg-blue-200"
			}
	}
}

export default function ArrayTextBadge({color, children, icon}) {
	const {text, bg} = getClasses(color)
	return <span className={`relative inline-block px-3 py-1 font-semibold leading-tight max-w-max mx-auto ${text}`}>
						<span aria-hidden="true"
						      className={`absolute inset-0 bg-green-200 opacity-50 rounded-full ${bg}`}>
						</span>
						<div className="relative flex justify-between gap-x-2 items-center">
							{children}
							{icon}
						</div>
					</span>
}
