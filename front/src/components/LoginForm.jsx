import React, {forwardRef, useState} from 'react';
import {login} from "../services/authentication";
import {useForm} from "react-hook-form";
import Alert from "./Alert";
import ButtonWithLoading from "./ButtonWithLoading";


/**
 * Login Form component
 *
 * @component
 */
const LoginForm = (props, ref) => {
	const {register, handleSubmit} = useForm();

	const [error, setError] = useState(false)
	const [loading, setLoading] = useState(false)

	return (
		<section className="flex flex-col md:flex-row h-screen items-center">

			<div className="bg-indigo-600 hidden lg:block w-full md:w-1/2 xl:w-2/3 h-screen">
				<img src="../../img/bureau.png" alt="" className="w-full h-full object-cover"/>
			</div>

			<div className="bg-white w-full md:max-w-md lg:max-w-full md:mx-auto md:mx-0 md:w-1/2 xl:w-1/3 h-screen px-6 lg:px-16 xl:px-12
        flex items-center justify-center">

				<div className="w-full h-100">
					<div className="flex justify-center align-middle">
						<img src="../../img/logo.svg" alt="" className="w-50 object-cover rounded-3xl shadow-lg"/>
					</div>

					{error && <Alert type={"danger"}>{error}</Alert>}

					<h1 className="text-xl md:text-2xl font-bold leading-tight mt-12">Merci de vous connecter à votre
						compte</h1>

					<form className="mt-6" onSubmit={handleSubmit(data => login(data, ref, props.state, setError, setLoading))}>
						<div>
							<label className="block text-gray-700">Adresse email</label>
							<input type="email" name="" id="" placeholder="Entrez votre adresse email"
							       className="w-full px-4 py-3 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500 focus:bg-white focus:outline-none"
							       autoFocus autoComplete required {...register("email")}/>
						</div>

						<div className="mt-4">
							<label className="block text-gray-700">Mot de passe</label>
							<input type="password" name="" id="" placeholder="Entrez votre mot de passe" minLength="4"
							       className="w-full px-4 py-3 rounded-lg bg-gray-200 mt-2 border focus:border-blue-500
                focus:bg-white focus:outline-none" required {...register("password")}/>
						</div>

						<div className="text-right mt-2">
							<a href="#"
							   className="text-sm font-semibold text-gray-700 hover:text-blue-700 focus:text-blue-700">Mot passe oublié ?</a>
						</div>

						<ButtonWithLoading type={"submit"} loading={loading}>S'identifier</ButtonWithLoading>
					</form>

					<hr className="my-6 border-gray-300 w-full"/>
				</div>
			</div>

		</section>
	);
};

export default forwardRef(LoginForm)
