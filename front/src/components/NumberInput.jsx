import React from "react";

export default function NumberInput({min, max, step, className, value, onChange}) {
	return (
		<input type={"number"} min={min} max={max} step={step} value={value} onChange={onChange}
		       className={`inline mr-1 focus:ring-indigo-500 focus:border-indigo-500 block ${className} shadow-sm sm:text-sm border-gray-300 rounded-md`}/>
	)
}
