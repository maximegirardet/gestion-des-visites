import React from 'react';

/**
 * Test header component
 *
 * @component
 */
const Header = ({children, icon}) => {
	return (
		<header className="bg-white shadow">
			<div className="max-w-7xl mx-auto py-2 px-4 sm:px-6 lg:px-8 flex flex-row items-center justify-between lg:justify-center">
				<div className={"mr-4 lg:hidden"}>
					{icon}
				</div>
				<h1 className="text-3xl font-bold leading-tight text-gray-900 text-center">
					{children}
				</h1>
				<div/>
			</div>
		</header>
	);
};

export default Header;
