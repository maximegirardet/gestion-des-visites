import React from "react";

export default function CardContainer({children, isVisit, hideCondition, className}) {
	return (
		<div
			className={`flex-1 text-center items-center lg:mr-4 lg:ml-4 content-center w-full ${!isVisit ? 'lg:w-auto lg:mb-0 pl-1 mb-4' : 'lg:ml-4 lg:mr-4 mb-6 lg:max-w-xs'} ${hideCondition ? ' lg:hidden' : ''} ${className ? className : ''}`}>
			{children}
		</div>
	)
}
