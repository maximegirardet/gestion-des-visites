import React from "react";

export default function LoadingItem() {
	return (
		<div className="border border-blue-300 shadow rounded-md p-4 mt-6 w-full mx-auto h-52">
			<div className="animate-pulse flex space-x-4 w-full">
				<div className="rounded-full bg-blue-400 h-12 w-12"/>
				<div className="flex-1 space-y-4 py-1">
					<div className="h-4 bg-blue-400 rounded w-3/4"/>
					<div className="space-y-2">
						<div className="h-4 bg-blue-400 rounded"/>
						<div className="h-4 bg-blue-400 rounded w-5/6"/>
					</div>
					<div className="space-y-2">
						<div className="h-4 bg-blue-400 rounded"/>
						<div className="h-4 bg-blue-400 rounded w-5/6"/>
					</div>
					<div className="space-y-2">
						<div className="h-4 bg-blue-400 rounded w-1/3"/>
					</div>
				</div>
			</div>
		</div>
	)
}
