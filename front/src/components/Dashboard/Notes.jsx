import React from "react";
import NotesItem from "./NotesItem";
import IconButton from "../IconButton";
import LeftChevron from "../../Icons/LeftChevron";

export default function Notes({setNotesOpen, worksite}) {
	return (
		<div className="bg-white dark:bg-gray-800 shadow overflow-hidden sm:rounded-md">
			<ul className="divide-y divide-gray-200">
				<li>
					<div className={"p-4 flex space-between justify-between"}>
						<IconButton icon={<LeftChevron width={20} height={20}/>} onClick={() => setNotesOpen(false)}/>
						<div className={"text-gray-700 text-xl"}>
							Notes du chantier
						</div>
						<div/>
					</div>
				</li>
				{worksite.notes.map((note, index) =>
					note.is_visible &&
					<li key={`note-${index}`}>
						<NotesItem date={note.created_at}>{note.content}</NotesItem>
					</li>
				)}
			</ul>
		</div>
	)
}
