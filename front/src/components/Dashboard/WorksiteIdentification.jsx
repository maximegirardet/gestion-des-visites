import React from 'react'
import TextBadge from "../TextBadge";
import {getFormattedPhoneNumber} from "../../utils/functions";

const statusCorrespondance = {
	"curatif": ["red", "Curatif"],
	"curatif_exterieur_uniquement": ["yellow", "Curatif extérieur"],
	"surveillance": ["green", "Surveillance"]
}

export default function WorksiteIdentification({worksite}) {
	const [color, label] = statusCorrespondance[worksite.status.short_name]
	return (
		<div className="shadow-lg rounded-xl w-full p-8 bg-indigo-500 dark:bg-gray-800 relative overflow-hidden flex-col">
			<TextBadge color={color}>{label}</TextBadge>
			<div className="text-white text-lg mt-4 mb-2">
				{worksite.name}
			</div>
			<div className="text-white text-sm">
				{worksite.address.street}
			</div>
			<div className="text-white text-sm">
				{worksite.address.postal_code + " " + worksite.address.city}
			</div>
			{worksite.contacts.map((contact, index) =>
				<div className="font-medium text-white mt-4 flex justify-center" key={index.toString()}>
					<a className={"mr-1"}
					   href={`tel:${contact.phone_number}`}>{getFormattedPhoneNumber(contact.phone_number)}</a>
					{contact.name && `(${contact.name})`}
				</div>
			)}
		</div>
	)
}
