import React from "react";
import TextBadge from "../TextBadge";
import {getFormattedDateAndTime, getFormattedDuration} from "../../utils/functions";

export default function NextVisitScheduling({nextVisit}) {
	return (
		nextVisit ?
			<div className={"flex w-full justify-between items-baseline"}>
				<div className={"text-gray-700 mb-2"}>
					<TextBadge>
						{getFormattedDateAndTime(nextVisit.scheduled_at)}
					</TextBadge>
				</div>
				<div className={"text-gray-700 text-xs mb-2"}>
					<TextBadge color={"blue"} small={true}>
						{getFormattedDuration(nextVisit.scheduled_duration)}
					</TextBadge>
				</div>
			</div> :
			<div className={"flex"}>
				<TextBadge color={"red"}>Aucun RDV planifié et assigné</TextBadge>
			</div>
	)
}
