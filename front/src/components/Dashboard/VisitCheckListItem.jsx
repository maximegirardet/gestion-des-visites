import React from "react";
import RoundTick from "../../Icons/RoundTick";
import {getFormattedDuration} from "../../utils/functions";

export default function VisitCheckListItem({step_name, step_duration, step_done}) {
	return (
		<li className="flex items-center text-gray-600 dark:text-gray-200 justify-between py-3 border-b-2 border-gray-100 dark:border-gray-800" key={step_name}>
			<div className={"flex items-center justify-start text-sm " + (step_done && "line-through text-gray-400")}>
				<span className={"text-left"}>
					{step_name}
				</span>
				<span
					className={"mx-4 flex items-center text-gray-400 dark:text-gray-300 min-w-max " + (step_done && "line-through")}>
					{step_duration ? getFormattedDuration(step_duration) : "0 min"}
				</span>
			</div>
			<div>
				<RoundTick checked={step_done}/>
			</div>
		</li>
	)
}
