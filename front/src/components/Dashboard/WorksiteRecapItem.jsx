import React from "react";

export default function WorksiteRecapItem({label, value, children}) {
	return (
		<div className="flex items-center mb-4 text-blue-500 rounded justify-between">
        <span className="rounded-lg p-2 bg-white">
	        {children}
        </span>
			<div className="flex flex-col w-full ml-2 items-start justify-evenly">
				<p className="text-white text-lg">
					{value}
				</p>
				<p className="text-blue-200 text-sm">
					{label}
				</p>
			</div>
		</div>
	)
}
