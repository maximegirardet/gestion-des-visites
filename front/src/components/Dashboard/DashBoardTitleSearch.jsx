import React from "react";

export default function DashBoardTitleSearch({search}) {
	return (
		<div className={"flex w-full justify-between ml-2"}>
			<h1 className={"text-2xl text-gray-600"}>Chantiers correspondant à la recherche "{search}" :</h1>
		</div>
	)
}
