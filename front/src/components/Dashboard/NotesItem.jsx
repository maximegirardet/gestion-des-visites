import React, {useState} from "react";
import {getFormattedDate} from "../../utils/functions";

export default function NotesItem({children, isPonctual, date}) {
	const [isDeployed, setDeployed] = useState(false)
	const type = isPonctual ? "Ponctuel" : "Récurrent"
	return (
		<a className="block hover:bg-gray-50 dark:hover:bg-gray-900 cursor-pointer" onClick={() => setDeployed(!isDeployed)}>
			<div className="px-4 py-4 sm:px-6">
				<div className="flex items-center justify-between">
					<p className={`text-md text-gray-700 dark:text-white ${!isDeployed ?  'md:truncate' : ''}`}>
						{children}
					</p>
					<div className="ml-2 flex-shrink-0 flex">
						<p className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
							{type}
						</p>
					</div>
				</div>
				<div className="mt-2 sm:flex sm:justify-between">
					<div className="sm:flex">
						<p className="flex items-center text-md font-light text-gray-500 dark:text-gray-300">
							{date && getFormattedDate(date)}
						</p>
					</div>
				</div>
			</div>
		</a>
	)
}
