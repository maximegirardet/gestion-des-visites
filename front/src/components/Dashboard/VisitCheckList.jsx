import React from "react";
import VisitCheckListItem from "./VisitCheckListItem";
import NextVisitScheduling from "./NextVisitScheduling";

export default function VisitCheckList({worksite}) {
	const nextVisit = worksite.scheduled_at ? worksite : worksite.nextScheduledVisits[0]
	const type = nextVisit ? nextVisit.type : worksite.next_visit_type
	return (
		<div className={"flex flex-col mt-2"}>
			<NextVisitScheduling nextVisit={nextVisit}/>
			<div
				className="px-4 py-1 mt-5 text-base rounded-full text-indigo-500 border border-indigo-500 undefined text-center max-w-max">
				{type.long_name}
			</div>
			<ul className={"mt-4"}>
				{type.steps?.map(step =>
					<VisitCheckListItem step_name={step.long_name} step_duration={step.duration} step_done={step.done}
					                    key={step.short_name}/>
				)}
			</ul>
		</div>

	)
}

