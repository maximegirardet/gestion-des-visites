import React from 'react'
import Link from "../../Icons/Link";
import Visit from "../../Icons/Visit";
import SSOL from "../../Icons/SSOL";
import SB from "../../Icons/SB";
import WorksiteRecapItem from "./WorksiteRecapItem";
import {getFormattedDate} from "../../utils/functions";

export default function WorksiteRecap({worksite, setNotesOpen}) {
	worksite = worksite.worksite ? worksite.worksite : worksite
	return (
		<div
			className="shadow-lg rounded-xl bg-blue-500 w-full p-6 bg-white dark:bg-gray-800 relative overflow-hidden flex flex-col max-h-52">
			<div className={"flex w-full mb-4 justify-between"}>
				<p className="text-white text-xl">
					Récapitulatif
				</p>
				{worksite.notesNumber > 0 &&
				<div className={"flex flex-row"}>
					<button type="button" className="w-8 h-8 text-base  rounded-full text-white bg-red-400 ml-2" onClick={() => setNotesOpen(true)}>
						<span className="p-1">{worksite.notesNumber}</span>
					</button>
				</div>
				}
			</div>
			<div className={"flex flex-col flex-wrap w-full max-h-44"}>
				<WorksiteRecapItem value={getFormattedDate(worksite.installation_date)}
				                   label={"Installation"}><Link/></WorksiteRecapItem>
				<WorksiteRecapItem value={worksite.visitNumber} label={"Visites"}><Visit/></WorksiteRecapItem>
				<WorksiteRecapItem value={worksite.ssolNumber} label={"SSOL"}><SSOL/></WorksiteRecapItem>
				<WorksiteRecapItem value={worksite.sbNumber} label={"SB"}><SB/></WorksiteRecapItem>
			</div>
		</div>
	)
}
