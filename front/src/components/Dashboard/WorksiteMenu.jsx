import React from "react";
import Button from "../Button";
import {Link} from "react-router-dom";

export function WorksiteMenu({worksite}) {
	const eventId = worksite.event_id
	return (
		<div className={"flex content-center justify-center flex-col items-center"}>
			<Link to={`/report/${eventId}`}>
				<Button type={"primary"}>Démarrer la visite</Button>
			</Link>

			<Button type={"secondary"}>Visite Hors-Cadre</Button>
		</div>
	)
}
