import React from 'react';
import {BadgeCheckIcon, ClockIcon} from "@heroicons/react/solid";

export default function DashboardItemIcon({type}) {
	return (
		<div className={"absolute -top-7 -left-7 z-10"}>
			{type === "done" ?
				<BadgeCheckIcon width={50} className={"m-2 text-green-500"}/> :
				type === "pending" ?
				<ClockIcon width={50} className={"m-2 text-yellow-500"}/> : null
			}
		</div>
	)
}