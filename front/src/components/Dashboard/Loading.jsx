import React, {Fragment} from "react";
import LoadingItem from "./LoadingItem";

export default function Loading() {

	return (
		<Fragment>
			<LoadingItem/>
			<LoadingItem/>
			<LoadingItem/>
			<LoadingItem/>
		</Fragment>
	)
}
