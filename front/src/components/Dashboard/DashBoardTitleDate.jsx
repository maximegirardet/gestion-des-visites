import React from "react";
import IconButton from "../IconButton";
import LeftChevron from "../../Icons/LeftChevron";
import RightChevron from "../../Icons/RightChevron";


function processDate(date, operation) {
	const dateObject = new Date(date)
	dateObject.setDate(dateObject.getDate() + operation)
	return dateObject
}

export default function DashBoardTitleDate({date, setDate, datepicker}) {
	return (
		<div className={"flex w-full justify-between"}>
			<IconButton icon={<LeftChevron width={30} height={30}/>} onClick={() => {
				setDate(processDate(date, -1))
			}
			}/>
			<h1 className={"text-2xl text-gray-600"}>Chantiers du {datepicker}</h1>
			<IconButton icon={<RightChevron width={30} height={30}/>} onClick={() => {
				setDate(processDate(date, 1))
			}
			}/>
		</div>
	)
}
