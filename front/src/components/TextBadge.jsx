import React from "react";

export default function TextBadge({color, children, small = false}) {
		switch(color){
			case "green":
				return <span className={`px-4 py-2 ${small ? 'text-sm' : 'text-base'} rounded-full text-green-800 bg-green-300`}>{children}</span>
			case "yellow":
				return <span className={`px-4 py-2 ${small ? 'text-sm' : 'text-base'} rounded-full text-yellow-800 bg-yellow-200`}>{children}</span>
			case "red":
				return <span className={`px-4 py-2 ${small ? 'text-sm' : 'text-base'} rounded-full text-red-800 bg-red-300`}>{children}</span>
			default:
				return <span className={`px-4 py-2 ${small ? 'text-sm' : 'text-base'} rounded-full text-blue-600 bg-blue-200`}>{children}</span>
		}
}
