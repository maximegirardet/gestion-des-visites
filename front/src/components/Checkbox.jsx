import React from "react";

export default function Checkbox({onChange, value, disabled}) {
	return (
		<input
			type="checkbox"
			className="h-6 w-6 rounded border-gray-300 focus:border-indigo-300 focus:ring-2 focus:ring-indigo-200 focus:ring-opacity-50 text-indigo-500 disabled:opacity-50"
			onChange={onChange}
			checked={value}
			disabled={disabled}
		/>
	)
}
