import React from "react";
import {CheckIcon} from "@heroicons/react/outline";
import {Listbox} from "@headlessui/react";
import classNames from "../../utils/functions";

export default function CustomSelectOption({option}) {
	return (
		<Listbox.Option
			key={option.id}
			className={({active}) =>
				classNames(
					active ? 'text-white bg-indigo-600' : 'text-gray-800',
					'cursor-pointer select-none relative py-2 pl-3 pr-9'
				)
			}
			value={option}>
			{({selected, active}) => (
				<>
					<div className="flex items-center">
						<div className="px-2 w-6 flex justify-center bg-gray-500 rounded shadow text-white font-semibold">{option.id}</div>
						<span className={classNames(selected ? 'font-semibold' : 'font-normal', 'ml-3 block truncate')}>
							{option.value}
						</span>
					</div>

					{selected ? (
						<span
							className={classNames(
								active ? 'text-white' : 'text-indigo-600',
								'absolute inset-y-0 right-0 flex items-center pr-4'
							)}>
							<CheckIcon className="h-5 w-5" aria-hidden="true"/>
						</span>
					) : null}
				</>
			)}
		</Listbox.Option>

	)
}
