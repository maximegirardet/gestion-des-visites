import React from "react";

const Loader = () => {
	return (
		<div>
			<img src="../../img/logo_entreprise.svg" alt="" className="h-56 object-cover rounded-3xl mb-32"/>
			<img src="../../img/logo.svg" alt="" className="w-50 object-cover rounded-3xl shadow-lg animate-bounce h-64"/>
		</div>
	)
}

export default Loader
