import React from "react";

export const buttonColorCorrespondances = {
	"primary": ["bg-indigo-600", "hover:bg-indigo-700", "focus:ring-indigo-500", "focus:ring-offset-indigo-200"],
	"secondary": ["bg-gray-600", "hover:bg-gray-700", "focus:ring-gray-500", "focus:ring-offset-gray-200"]
}


export default function Button({children, type, onClick, disabled}){
	return (
		<button type="button" onClick={onClick} disabled={disabled}
		        className={`py-2 px-4 ${buttonColorCorrespondances[type].join(' ')} text-white transition ease-in duration-200 text-center text- font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2 rounded-lg m-2 max-w-max disabled:opacity-50 disabled:cursor-not-allowed`}>
			{children}
		</button>
	)
}
