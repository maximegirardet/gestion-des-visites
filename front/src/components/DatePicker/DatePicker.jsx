import React from "react";
import ReactDatePicker, {registerLocale} from "react-datepicker";
import fr from 'date-fns/locale/fr';
import "react-datepicker/dist/react-datepicker.css";
import "./DatePicker.css"
import {getDay} from "date-fns";

registerLocale('fr', fr)

export default function DatePicker({selected, onChange, inputClassName}) {
	const isWeekday = (date) => {
		const day = getDay(date);
		return day !== 0 && day !== 6;
	};
	return (
		<ReactDatePicker selected={selected} onChange={onChange} locale={fr} dateFormat={'dd/MM/yyyy'}
		                 calendarClassName={"calendar"} className={inputClassName ?? "calendar-input"}
		                 wrapperClassName={"calendar-wrapper"}
		                 dayClassName={() => "calendar-day"}
		                 weekDayClassName={() => "calendar-weekday"}
		                 calendarStartDay={1}
		                 filterDate={isWeekday}
		                 disabledKeyboardNavigation={true}
		/>
	)
}
