import React from "react";
import ContextMenuItem from "./ContextMenuItem";

export default function ContextMenuItemWithIcon({onClick, icon, className, children, disabled}) {
	return (
		<ContextMenuItem onClick={onClick} disabled={disabled}>
			<div className={`flex flex-row justify-between ${className}`}>
				{children}
				{icon}
			</div>
		</ContextMenuItem>
	)
}
