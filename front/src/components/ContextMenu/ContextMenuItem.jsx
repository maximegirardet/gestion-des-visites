import React from "react";
import {Menu} from "@headlessui/react";

export default function ContextMenuItem({children, onClick, disabled}) {
	return (
		<Menu.Item disabled={disabled}>
			{({active}) => (
				<a
					onClick={onClick}
					className={`cursor-pointer block px-4 py-2 text-sm disabled:opacity-50' ${active ? 'bg-gray-100 text-gray-900' : 'text-gray-700'} ${disabled ? 'cursor-not-allowed opacity-50' : ''}`}
				>
					{children}
				</a>
			)}
		</Menu.Item>
	)
}
