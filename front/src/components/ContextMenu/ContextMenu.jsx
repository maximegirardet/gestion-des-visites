import React, {Fragment} from "react";
import {Menu, Transition} from "@headlessui/react";

export default function ContextMenu({openingButton, buttonClassName, children}) {
	return (
		<Menu as="div" className="relative inline-block text-left">
			{({open}) => (
				<>
					<div className={"flex justify-center items-center"}>
						<Menu.Button
							className={`inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 ${buttonClassName}`}>
							{openingButton}
						</Menu.Button>
					</div>

					<Transition
						show={open}
						as={Fragment}
						enter="transition ease-out duration-100"
						enterFrom="transform opacity-0 scale-95"
						enterTo="transform opacity-100 scale-100"
						leave="transition ease-in duration-75"
						leaveFrom="transform opacity-100 scale-100"
						leaveTo="transform opacity-0 scale-95"
					>
						<Menu.Items
							static
							className="origin-top-right absolute right-0 mt-2 w-52 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none z-50"
						>
							<div className="py-1">
								{children}
							</div>
						</Menu.Items>
					</Transition>
				</>
			)}
		</Menu>
	)
}
