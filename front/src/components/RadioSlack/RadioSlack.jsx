import React, {useRef, useState} from "react"
import RadioSlackItem from "./RadioSlackItem"


export default function RadioSlack({options, select, selected}) {
	const [isSelected, setIsSelected] = useState(selected)

	/**
	 * Reference which points to the ul (used to focus li children)
	 * @type {React.MutableRefObject<HTMLUListElement>}
	 */
	const ul = useRef()
	return (
		<>
			<div className="pt-4 flex justify-center w-full">
				<div className="w-full max-w-3xl mx-auto">
					<fieldset>
						<ul className="space-y-4" ref={ul}>
							{options.map((element, index) =>
								<RadioSlackItem index={index} key={element.id}
								                active={element.id === isSelected}
								                borderColor={element.borderColor}
								                onClick={() => {
									                setIsSelected(element.id)
									                select(element.id)
									                ul.current.children[index].focus()
								                }}>
									{element.value}
								</RadioSlackItem>)}
						</ul>
					</fieldset>
				</div>
			</div>
		</>
	)
}
