import React from "react";

/**
 * Gets border color of RadioSlackItem
 * @param color
 * @returns {string}
 */
function getBorder(color) {
	switch (color) {
		case "green":
			return "border-green-300 hover:border-green-400"
		case "yellow":
			return "border-yellow-300 hover:border-yellow-400"
		case "red":
			return "border-red-300 hover:border-red-400"
		default:
			return "border-gray-300 hover:border-gray-400"
	}
}

export default function RadioSlackItem({index, active, onClick, borderColor, children}) {
	return (
		<li id="radiogroup-option-0"
		    className="group relative bg-white rounded-lg shadow-sm cursor-pointer focus:outline-none focus:ring-1 focus:ring-offset-2 focus:ring-indigo-500"
		    role={"radio"} aria-checked={active} onClick={onClick} tabIndex={index}>
			<div
				className={`rounded-lg border bg-white px-6 py-4 sm:flex sm:justify-between ${getBorder(borderColor)}`}>
				{children}
			</div>
			<div
				className={`${active ? 'border-indigo-500' : 'border-transparent'} absolute inset-0 rounded-lg border-2 pointer-events-none`}/>
		</li>
	)
}
