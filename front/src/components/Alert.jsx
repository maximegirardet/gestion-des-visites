import React from 'react';

const Alert = (props) => {

	let className = ""

	switch (props.type) {
		case "success":
			className = "bg-green-100"
			break
		case "info":
			className = "bg-blue-100"
			break
		case "warning":
			className = "bg-orange-100"
			break
		case "danger":
			className = "bg-red-100"
			break
		default:
			className = "bg-orange-100"
	}

	return (
		<div className={className + " border-l-4 border-orange-500 text-orange-700 p-4 mt-12"} role="alert">
			<p>{props.children}</p>
		</div>
	)
}

export default Alert
