import React from "react";

const Select = React.forwardRef(({value, options, onChange, disabled, className}, ref) => (
	<p className="text-gray-900 whitespace-no-wrap">
		<select value={value}
		        className={`lock w-24 text-gray-700 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-primary-500 focus:border-primary-500 ${className}`}
		        onChange={onChange}
		        disabled={disabled}
		        ref={ref}>
			{options.map(option => <option key={option.value} name={option.value}
			                               value={option.value}>{option.text}</option>)}
		</select>

	</p>
));

export default Select
