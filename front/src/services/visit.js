import {baseURL} from "./authentication"

/**
 * Makes a call to the API to get scheduled visit from eventID
 * @param API
 * @param token JWT
 * @param eventId Event ID
 * @returns {Promise<ApiResponse<*, *>>}
 */
export async function searchWorksites(API, token, eventId) {
	return await API(baseURL, "/scheduled_visits/{event_id}", {
		method: "get",
		params: {
			event_id: eventId
		},
	}, token)
}
