import {baseURL} from "./authentication"

/**
 * Makes a call to the API to get scheduled visits during a specific day.
 * @param API
 * @param token JWT
 * @param date Date during which we want to get scheduled visits
 * @returns {Promise<ApiResponse<*, *>>}
 */
export async function getScheduledVisits(API, token, date) {
	return await API(baseURL, "/scheduled_visits", {
		method: "get",
		query: {
			date: date
		},
	}, token)
}

/**
 * Makes a call to the API to search for worksites which satisfy the query
 * @param API
 * @param token JWT
 * @param query The query
 * @param signal {AbortSignal} AbortSignal used to abort a fetch request
 * @returns {Promise<ApiResponse<*, *>>}
 */
export async function searchWorksites(API, token, query, signal) {
	return await API(baseURL, "/worksites", {
		method: "get",
		query: {
			q: query
		},
		signal: signal
	}, token)
}
