import {baseURL} from "./authentication"

/**
 * Gets EliminationEnd info from the API
 * @param API
 * @param token
 * @param id
 * @returns {Promise<ApiResponse<*, *>>}
 */
export async function getEliminationEnd(API, token, id) {
	return await API(baseURL, "/worksites/{id}/elimination_end_info", {
		params: {
			id: id
		},
		method: "get"
	}, token)
}
