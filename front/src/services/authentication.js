import {fetchAPI} from "../utils/api"

import configData from '../../config.json'
import {useHistory} from "react-router-dom"
import {useCallback} from "react"

const hostname = location.hostname

// noinspection JSUnresolvedVariable
/**
 * API Base URL
 * @type {string}
 */
export const baseURL = configData[hostname].API_BASE_URL

/**
 * Logs the user and sets the received token
 * @param data {Object} User data
 * @param token {Object} JWT token
 * @param state {Object} LoginForm states
 * @param setError {Function} Sets logging error
 * @param setLoading {Function} Sets login loading state
 */
export function login(data, token, state, setError, setLoading) {
	setLoading(true)
	fetchAPI(baseURL, "/login", {
		json: {
			username: data.email,
			password: data.password
		},
		method: "post",
		credentials: "include"
	}).then(
		/** @param r {{token: String} }*/
		r => {
			setLoading(false)
			state.setLoading(true)
			token.current = r.token
			state.setLogged(true)
			return true
		}).catch(r => {
		if (r.status === 401) {
			setLoading(false)
			setError("Identifiant ou mot de passe incorrect !")
		}
		return false
	})
}

/**
 * Gets current user and tries to refresh token if an error occurs during /me call
 * @param token {Object} JWT token
 * @param setUser {Function} Sets user info
 * @param setLoading {Function} Sets page loading state
 * @param refreshAttempt {Boolean} Tells if a refresh attempt has been done
 */
export function getUser(token, setUser, setLoading, refreshAttempt = false) {
	fetchAPI(baseURL, "/me", {
		method: "get",
	}, token.current).then(r => {
		setUser(r)
		setLoading(false)
	}).catch(() => {
		if (!refreshAttempt) {
			refresh(token).then(refreshed => {
				if (refreshed) {
					return getUser(token, setUser, setLoading, true)
				}
				setLoading(false)
			})
		}
	})
}

/**
 * Refreshes the token using the refresh token (stored in HTTPOnly cookies)
 * @param token {Object} JWT token
 * @returns {Promise<boolean>}
 */
export async function refresh(token) {
	try {
		const response = await fetchAPI(baseURL, "/token/refresh", {
			method: "get",
			credentials: "include"
		})
		token.current = response.token
		return true
	} catch (r) {
		return false
	}
}

/**
 * Custom fetch function adapted to API and JWT refresh tokens
 * If a call fails, refreshes automatically the token
 * @param baseUrl Base URL of the API
 * @param endpoint Route
 * @param options FetchOptions (method, ...)
 * @param authToken JWT
 * @param history History entity
 * @param refreshAttempt Indicates if a refresh attempt has been made
 * @returns {Promise<ApiResponse<*, *>>}
 */
function internFetchAPI(baseUrl, endpoint, options, authToken, history, refreshAttempt = false) {
	return new Promise(async resolve => {
		try {
			resolve(await fetchAPI(baseUrl, endpoint, options, authToken.current))
		} catch (r) {
			if (r.name !== 'AbortError' && !refreshAttempt) {
				refresh(authToken).then(async refreshed => {
					if (refreshed) {
						resolve(await internFetchAPI(baseUrl, endpoint, options, authToken, history, true))
					} else {
						history.push("/login")
					}
				})
			}
		}
	})
}

/**
 * Custom hook used to get API function containing history entity
 * @returns {Function}
 */
export function useFetchAPI() {
	const history = useHistory()
	return useCallback((baseUrl, endpoint, options, authToken) => internFetchAPI(baseUrl, endpoint, options, authToken, history), [history])
}
