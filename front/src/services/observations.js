import {baseURL} from "./authentication"

/**
 * Gets predefined observations from the API (of a specific type)
 * @param API
 * @param token JWT token
 * @param type Type of observation
 * @returns {Promise<*>}
 */
export async function getPredefinedObservations(API, token, type) {
	const response = await API(baseURL, "/predefined_observations", {
		method: "get",

	}, token)
	return response.filter(v => v.type.type === type).map(v => ({
		id: v.number,
		value: v.text
	}))
}

/**
 * Adds the specified text to observations and automatically inserts a line break if necessary
 * @param localObservations Previous observations
 * @param toAdd Text to add
 * @returns {*}
 */
export function addToObservations(localObservations, toAdd) {
	const separator = localObservations.length === 0 || localObservations.charAt(localObservations.length - 1) === '\n' ? '' : '\n'
	return localObservations + separator + toAdd
}
