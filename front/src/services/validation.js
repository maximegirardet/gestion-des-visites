import store from "../store"
import {baseURL} from "./authentication"
import {removeStationsData} from "../components/Visit/Stations/stationsSlice"
import {removeObservationData} from "../components/Visit/Observations/ObservationsSlice"
import {removeEliminationData} from "../components/Visit/Eliminations/EliminationsSlice"
import {removePagesData} from "../components/Visit/Pages/PagesSlice"
import {removeStationsReplicaData} from "../components/Visit/Stations/stationsReplicaSlice"
import {removeNextVisitData} from "../components/Visit/NextVisit/nextVisitSlice"

/**
 * POSTs visit report to the API
 * @param API
 * @param token JWT Token
 * @param event_id Google Calendar event_id
 * @param visit Visit
 * @param addReportPreviewStepCallback Callback used to add PDF preview step
 * @param dispatch
 * @returns {Promise<ApiResponse<*, *>>}
 */
export async function postReport(API, token, event_id, visit, addReportPreviewStepCallback, dispatch) {
	/**
	 *
	 * @type {{}}
	 */
	const reportStore = store.getState()
	const data = await API(baseURL, "/visits", {
		method: "post",
		json: {
			worksite: `/worksites/${visit.worksite.id}`,
			type: `/visit_types/${visit.type.id}`,
			diverseObservation: reportStore.observations[event_id]?.client ?? "",
			internObservation: reportStore.observations[event_id]?.intern ?? "",
			scheduledAt: visit.scheduled_at,
			createdAt: new Date().toISOString(),
			updatedAt: new Date().toISOString(),
			estimatedDuration: visit.scheduled_duration,
			effectiveDuration: "P00Y00M00DT01H00M00S",
			isPending: false,
			isCompleted: true,
			eventId: event_id,
			stationStatuses: [
				...(reportStore.stations[event_id].ssol ?? []), ...(reportStore.stations[event_id].sb ?? [])
			],
			durationAdjustments: reportStore.nextVisit[event_id].adjustments,
			nextVisit: {
				estimatedDuration: reportStore.nextVisit[event_id].choice.duration,
				type: `/visit_types/${reportStore.nextVisit[event_id].choice.id}`,
				scheduledAt: reportStore.nextVisit[event_id].choice.date
			}
		},
	}, token)
	if (data) {
		addReportPreviewStepCallback(data.id)
	}
	return data
}

/**
 * Gets PDF Report from the API
 * @param API
 * @param token JWT
 * @param id Id of the visit
 * @returns {Promise<ApiResponse<*, *>>}
 */
export async function getPDFReport(API, token, id) {
	return await API(baseURL, "/visits/{id}/report", {
		method: 'get',
		params: {
			id: id
		}
	}, token)
}

/**
 * Converts a base64 string to a Blob
 * @param base64 Base64 string
 * @param mimeType Mime Type of the file
 * @returns {Blob}
 */
export function base64ToBlob(base64, mimeType) {
	const byteString = window.atob(base64)
	const arrayBuffer = new ArrayBuffer(byteString.length)
	const int8Array = new Uint8Array(arrayBuffer)
	for (let i = 0; i < byteString.length; i++) {
		int8Array[i] = byteString.charCodeAt(i)
	}
	return new Blob([int8Array], {type: mimeType})
}

/**
 * Removes all data of a specific event
 * @param event_id Id of the event
 * @param dispatch Dispatch function
 */
export function removeVisitData(event_id, dispatch) {
	dispatch(removeStationsData({event_id}))
	dispatch(removeObservationData({event_id}))
	dispatch(removeEliminationData({event_id}))
	dispatch(removeStationsReplicaData({event_id}))
	dispatch(removeNextVisitData({event_id}))
	dispatch(removePagesData({event_id}))
}
