const PREFIX = "V1"

import { pageCache } from 'workbox-recipes';
import { imageCache } from 'workbox-recipes';
import { staticResourceCache } from 'workbox-recipes';

self.addEventListener("install", e => {
	e.waitUntil(self.skipWaiting())
})

self.addEventListener("activate", e => {
	e.waitUntil(self.clients.claim())
})

pageCache();
imageCache();
staticResourceCache();


