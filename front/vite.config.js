import {defineConfig} from 'vite'
import reactRefresh from '@vitejs/plugin-react-refresh'
import execute from "rollup-plugin-execute";

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [reactRefresh()],
	build: {
		rollupOptions: {
			plugins: [
				execute('npx rollup -c && mv sw.build.js dist/'),
				execute('cp -r img dist/')
			]
		},
		assetsInlineLimit: 0
	}
})
